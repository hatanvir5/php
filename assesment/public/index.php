<?php
include_once("../vendor/autoload.php");

use Tanvir\Utility;
use Tanvir\Debugger;
use Tanvir\Sanitizer;
use Tanvir\Validator;

$music = simplexml_load_file('music.xml');
// echo"<pre>";
// print_r($music);
// echo"</pre>";
// die();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Product List</title>
</head>
<body class="bg-info">
    <h2 class="p-5  text-light text-center">Music</h2><br>

<?php 

if(count($music)<=0){

?>
    <tr><td colspan='8' style="text-align:center;"> No Records Found</td></tr>
<?php
}
else{
?>
          <?php
         foreach($music as $artist):
            foreach($artist as $album):
        
?>
<table class="table">
    <tr>

      <td>
          <dl>
            <dt>Artist</dt>
            <dd><?= $artist['name'];?></dd>
          </dl>       
      </td>

      <td>
          <dl >
            <dt>Album Title</dt>
            <dd>  <?= $album['title'];?></dd>
          </dl>        
      </td>
      <td>
      <dt>description</dt>
                 <dd><li><?= $album->description;?></li></dd>
               <dt>link</dt>
                 <dd><li><?= $album->description['link'];?></li></dd>
        </td>
            
        <?php foreach($album as $song):?>
            <td>
             <dl>
               <dt>song</dt>
                 <dd><li><?= $song['title'];?></li></dd>
               <dt>Length</dt>

                 <dd><li><?= $song['length'];?></li></dd>
            

        <?php  endforeach;?>
          
            </dl>
          </td>
           
    </tr>
</table>
            <?php   endforeach;?>
            <?php   endforeach;?>
      
<?php
}
?>
        </table>
        <a href="create.php" class="ml-4 btn btn-light">Add</a>      
</body>
</html>