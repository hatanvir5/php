<?php

include_once("../vendor/autoload.php");

use Tanvir\Utility;
use Tanvir\Debugger;
use Tanvir\Sanitizer;
use Tanvir\Validator;

$sanitizedData=Sanitizer::sanitize($_POST);
$validatedData=Validator::validate($sanitizedData);

$catalog = simplexml_load_file('products.xml');

// $product = $catalog->addChild('product');
// $product->addAttribute('description', $validatedData['productDescription']);
$catalog_item = $catalog->product->addChild('catalog_item');

$catalog_item->addAttribute('gender', $validatedData['gender']);
$catalog_item->addChild('item_number', $validatedData['itemNumber']);
$catalog_item->addChild('price', $validatedData['price']);
$size = $catalog_item->addChild('size');

$size->addAttribute('description', $validatedData['description']);

$size->addChild('color_swatch', $validatedData['colorSwatch']);
$size->color_swatch->addAttribute('image', $validatedData['colorSwatchImage']);

file_put_contents('products.xml', $catalog->asXML());
header('location:index.php');


?>

