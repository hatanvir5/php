<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Creative Corners</title>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Alata&display=swap" rel="stylesheet">
   
    <link rel="icon" type="image/png" href="images/logo1.png">

</head>

<body id="body">
    
 
   

      <header>
        
      
            <nav class="navbar navbar-expand-lg ">
                <a class="navbar-brand" href="#">Creative Corners</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"><i class="fas fa-sliders-h"></i></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                  <ul class="navbar-nav  ml-auto">
                    <li class="nav-item active">
                      <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="portfolio.html">Portfolio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="project.html">My Projects</a>
                    </li>

        
                    <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Others
                      </a>
                      <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Coming </a>
                        <a class="dropdown-item" href="#">Coming</a>
                        <a class="dropdown-item" href="#">Coming</a>
                      </div>
                    </li>
                  </ul>
                </div>
              </nav>
        
      </header>
      
 <section id="slogan">
    <div class="container">
   

          <h1><strong>SHIFT</strong> the <strong>CTRL</strong> to me<br>
        
            I will <strong>ALT</strong> the world.
            </h1>
  
     
            </div>

 </section>


    <footer class="pt-4 pb-4 fixed-bottom">
        <div class="container">
        <div class="row">
            <div class="col-8 col-xs-8">
                <p class="pt-2">&copy; Copyright Tanvir Ahmad</p>
            </div>
            <div class="col-4 col-xs-4 text-right">
                <ol class="m-0 p-0" >
                    <li class="d-inline-block"><a href="www.facebook.com"><i class="fab fa-facebook-square fa-2x"></i></a></li>
                    <li class="d-inline-block"><a href="www.youtube.com"><i class="fab fa-youtube fa-2x "></i></a></li>
                    <li class="d-inline-block"><a href="www.googleplus.com"><i class="fab fa-google-plus-square fa-2x "></i></a></li>
                </ol>
            </div>
        </div>
        </div>
    </footer>






 


  


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js" crossorigin="anonymous"></script>




</body>

</html>