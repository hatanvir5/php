<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Javascript</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" type="image/png" href="images/pin-icon.png">
</head>

<body>
      

<header>
    <nav class="navbar navbar-expand-lg ">
        <a class="navbar-brand" href="index.html"><h2>Creative Corners</h2></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"><i class="fas fa-sliders-h"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav  ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                    <a class="nav-link" href="portfolio.html">portfolio</a>
                </li>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="project.html">My Projects</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Others
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Coming Soon</a>
                <a class="dropdown-item" href="#">Coming Soon</a>
                <a class="dropdown-item" href="#">Coming Soon</a>
              </div>
            </li>
          </ul>
        </div>
      </nav>
</header>
<body>
    <li><a href="project/Javascript/weather/weather.html">Weather API</a></li>
    <li> <a href="project/Javascript/superhero/superHero.html" target="blank">See Super Hero Squad</a></li>
    <li> <a href="project/Javascript/photos/photos.html"  target="blank">See photos</a></li>
    <li> <a href="project/Javascript/comment/comment.html"  target="blank"> See Comment Style </a></li>
    <li> <a href="project/Javascript/Todo/todo.html"  target="blank">See Todo List</a></li>
    <li> <a href="project/Javascript/Users/users.html"  target="blank">See Users Data</a></li>
   
</body>
</html>