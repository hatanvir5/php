<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Creative Corners</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" type="image/png" href="images/pin-icon.png">
</head>

<body>
      

<header>
    <nav class="navbar navbar-expand-lg ">
        <a class="navbar-brand" href="#"><h2>Creative Corners</h2></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"><i class="fas fa-sliders-h"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav  ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                    <a class="nav-link" href="portfolio.html">portfolio</a>
                </li>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="project.html">My Projects</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Others
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Coming Soon</a>
                <a class="dropdown-item" href="#">Coming Soon</a>
                <a class="dropdown-item" href="#">Coming Soon</a>
              </div>
            </li>
          </ul>
        </div>
      </nav>
</header>

<section id="portfolio">
    <div class="container">
        <div class="row">
          <h1>My portfolio</h1>
            <div class="col-6">
            <h3>
              Academic Background:
            </h3>
            <ul>
              <li>
                  <h6>Bachelor Of Science</h6>
                <ul>
                  <li><p>Int' Islamic University Chittagong</p>
                  </li>
                  <li>Running(7<sup>th</sup> Semester</li>
                </ul>
              </li>
            </ul>
            <h3>Achivements:</h3>
            <ul>
                <li>
                    <h6>Skills For Employment Investment Program</h6>
                  <ul>
                    <li><p>
                        This Project was offered by the Government of Bangladesh.It was about 3 months long project. 
                        I had a great experience . I had also learnt a lot about web design.
                        
                        </p>
                    </li>
                    <li> In SEIP Web Design project ,
                        I achieved Student of the Batch  position. </li>
                  </ul>
                </li>
              </ul>
            

            </div>
            <div class="col-6">
              <h3>Contact:</h3>
            
                <p><a href="https://gitlab.com/hatanvir5/"><i class="fab fa-gitlab"></i> </i>	&nbsp; Gitlab</a></p>
                </li>
                <p><a href="https://www.facebook.com/profile.php?id=100003129122144"><i class="fab fa-facebook"></i> 	&nbsp;Facebook</a></p>
                <p> <a href="https://www.linkedin.com/in/tanvir-ahmed-52aa70192?fbclid=IwAR0RGm8mL_Or0Vr1l0dxuBwTfzVIEMk0O28jQQix3Kw6hlWV2af8OQt1j2Y"><i class="fab fa-linkedin"></i> 	&nbsp;Linkedin</a></p>
          
          
                </div>
        </div>

    </div>
</section>

<script src="js/bootstrap.min.js" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


</body>

</html>
