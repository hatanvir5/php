<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>HTML5</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" type="image/png" href="images/pin-icon.png">
</head>

<body>
      

<header>
    <nav class="navbar navbar-expand-lg ">
        <a class="navbar-brand" href="index.html"><h2>Creative Corners</h2></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"><i class="fas fa-sliders-h"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav  ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                    <a class="nav-link" href="portfolio.html">portfolio</a>
                </li>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="project.html">My Projects</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Others
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Coming Soon</a>
                <a class="dropdown-item" href="#">Coming Soon</a>
                <a class="dropdown-item" href="#">Coming Soon</a>
              </div>
            </li>
          </ul>
        </div>
      </nav>
</header>
</head>
<body>
    <div style="margin: 150px;">
        <p><a href="project/HTML5/typograpy/assignment.html">HTML5 Assignment</a></p>
        
        <p><a href="project/HTML5/typograpy/typography.html">HTML5 Typograpy</a></p>
        <p><a href="project/HTML5/typograpy/boldAndStrong.html">Blog about Strong & Bold</a></p>  
        <p><a href="project/HTML5/typograpy/emAndItalic.html">Blog about Emphasis & Italic</a></p> 
      
        </p>
    </div>
</body>
</html>