<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <div class="container">

    <h1>Feadback Box:</h1>
    
    <form action="feedbackProcessor.php" method="post">

 
    
    <label for="">Name</label>
    <input type="text" name="name" class="form-control">
    <label for="">Email</label>
    <input type="email" name="email" class="form-control">
    

    <label for="">Age</label>
    <input type="text" name="age"  class="form-control">

    <label for="">Address</label>
    <input type="type" name="address"  class="form-control">

    <label for="">Mobile</label>
    <input type="text" name="mobile"  class="form-control">

    <label for="">Comment</label>  
    <textarea name="comment" id="" cols="30" rows="5"  class="form-control"></textarea>

    <button type="submit"  class="btn btn-success">Save</button>
    </form>
    </div>
</body>
</html>