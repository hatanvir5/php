<?php
include_once("../vendor/autoload.php");
include_once("../src/security.php");
use Tanvir\Utility;
use Tanvir\Debugger;
use Tanvir\Sanitizer;
use Tanvir\Validator;

$debugger=new Debugger;

if(!Utility::isPosted()){
    header("location:index.php");
}

$sanitizedData=Sanitizer::sanitize($_POST);
$validatedData=Validator::validate($sanitizedData);
$validatedStringData=serialize($validatedData);
$dataStoreProcess=setcookie('feedbackData', $validatedStringData, time() + (86400 * 30), "/");
if($dataStoreProcess){
    echo "cookie has been  set seccessfully";
}
else{
    echo "cookie has been  set seccessfully";

}

// $debugger->debug($dataStoreProcess);

echo '<br><a href="feedbackView.php">Take a Look</a>';

?>