<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitc5b70bf95910ec9cf507c5153ced9d93
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'Tanvir\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Tanvir\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitc5b70bf95910ec9cf507c5153ced9d93::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitc5b70bf95910ec9cf507c5153ced9d93::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
