create table registrations(
    id int(12) auto_increment not null primary key,
    fullName varchar(50) not null,
    userName varchar(30) unique not null,
    email varchar(30) unique not null,
    phone varchar(20) not null,
    sponsorName varchar(40),
    club varchar(40) not null,
    password varchar(40) not null,
    confirmPassword varchar(40) not null,
    );

    create table login(
    id int(12) auto_increment not null primary key,
    userName varchar(30) unique not null,
    password varchar(40) not null
    )