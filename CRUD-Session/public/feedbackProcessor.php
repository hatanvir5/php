<?php
session_start();
include_once("../vendor/autoload.php");

use Tanvir\Utility;
use Tanvir\Debugger;
use Tanvir\Sanitizer;
use Tanvir\Validator;

// $debugger=new Debugger;


$sanitizedData=Sanitizer::sanitize($_POST);
$validatedData=Validator::validate($sanitizedData);



$data=[];

if(!array_key_exists('feedbackData',$_SESSION)){
    $_SESSION['feedbackData']=[];
}



if(array_key_exists('feedbackData',$_SESSION)){
    $data=$_SESSION['feedbackData'];
}

$data[]=$validatedData;


$_SESSION['feedbackData']=$data;

header('location:feedbackView.php');
    

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>
<br/>
    <a href="feedbackView.php"class="btn btn-success" >Go to View Mode</a>
</body>
</html>