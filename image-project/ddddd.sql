create table patient(
pid int auto_increment PRIMARY key not null,
pname varchar(40) not null,
Address varchar(70),
mobile varchar(15),
DOB date,
gender varchar(10)

)


create table visit(
id int auto_increment PRIMARY key not null,
pid int(11),
did int (11),
visit_date date

FOREIGN KEY(pid) REFERENCES patient(pid),
FOREIGN KEY(did) REFERENCES doctor(did)
)

create table doctor(
did int auto_increment PRIMARY key not null,
dname varchar(40) not null,
speciality varchar(10)
)


create table drug(
drid int auto_increment PRIMARY key not null,
d_name varchar(40) not null,
d_type varchar(10),
manuf_year date,
unit_price double(10,2)
)


create table prescribe(
drid int auto_increment PRIMARY key not null,
pid int(11),
did int(11),
drid int(11),
pdate date,
quantity varchar(20),

FOREIGN KEY(pid) REFERENCES patient(pid),
FOREIGN KEY(did) REFERENCES doctor(did),
FOREIGN KEY(drid) REFERENCES drug(pid)
)

SELECT patient.pid,patient.pname,patient.Address,patient.mobile,patient.gender,patient.DOB,drug.d_name,drug.d_type,drug.manuf_year,drug.unit_price,prescribe.pdate,prescribe.quantity
FROM `prescribe`
JOIN patient  ON patient.pid=prescribe.pid
JOIN drug ON drug.drid=prescribe.drid
WHERE patient.pid=1;


SELECT dname,pname,visit_date
FROM `visit`
JOIN patient  ON patient.pid=visit.pid
JOIN doctor ON doctor.did=visit.did

WHERE visit.visit_date=2020-09-16;





































here 3



CREATE TABLE products(

p_id INT NOT NULL AUTO_INCREMENT,
p_name VARCHAR(50) NOT NULL,
 unit_price INT(8) NOT NULL,
 PRIMARY KEY(p_id)
);

CREATE TABLE customers(
c_id INT NOT NULL AUTO_INCREMENT,
 c_name VARCHAR(50) NOT NULL,
 mobile INT(15) NOT NULL,
 street VARCHAR(50),
 city VARCHAR(50),
 country VARCHAR(50),
 PRIMARY KEY(c_id)
);


CREATE TABLE employees(
emp_id INT NOT NULL AUTO_INCREMENT,
 e_name VARCHAR(50) NOT NULL,
 designation VARCHAR(50),
 commission VARCHAR(50),
 salary INT(8) NOT NULL,
 mobile INT(15) NOT NULL,
 city VARCHAR(50) ,
 country VARCHAR(50),
 PRIMARY KEY (emp_id)
)

CREATE TABLE sales(

    s_id INT NOT NULL AUTO_INCREMENT,
    p_id INT NOT NULL,
    c_id INT NOT NULL,
    emp_id INT NOT NULL,
    date_of_sale VARCHAR(50) NOT NULL,
    time TIME ,
    quantity INT(4) NOT NULL,
    PRIMARY KEY(s_id),
    FOREIGN KEY(c_id) REFERENCES customers(c_id) ,
    FOREIGN KEY(emp_id) REFERENCES employees(emp_id),
    FOREIGN KEY(p_id) REFERENCES products(p_id)
)


answer to the question no.01

SELECT customers.c_id,
	   customers.c_name,
       customers.mobile,
       customers.street,
       customers.city,
       customers.country 
       
FROM `sales` 
JOIN customers 
ON sales.c_id = customers.c_id
WHERE sales.c_id = 2 AND 
	sales.emp_id = 2



    answer to the question no.02

SELECT  employees.e_name    
FROM `sales` 
JOIN employees 
ON sales.emp_id = employees.emp_id 
JOIN products
ON sales.p_id = products.p_id
WHERE sales.emp_id = 2 AND 
	  sales.p_id = 1



    answer to the question no.03


SELECT  customers.c_name
       
FROM `sales` 
JOIN customers 
ON sales.c_id = customers.c_id 
JOIN products
ON sales.p_id = products.p_id
WHERE sales.c_id = 2 AND 
	  sales.p_id = 1