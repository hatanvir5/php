<?php
session_start();

require 'db.php';
require 'header.php';


if(array_key_exists('id',$_GET)){
    $id=$_GET['id'];
}

if(array_key_exists('id',$_SESSION)){
  $id=$_SESSION['id'];
  unset($_SESSION['id']);

}

$sql='SELECT
*
  FROM newstable  
  JOIN reportertable ON newstable.id=reportertable.newsTableId 
  JOIN relatednewstable  ON  newstable.id=relatednewstable.newsTableId
  WHERE newstable.id=:id';
$statement=$connection->prepare($sql);
$statement->execute([':id'=>$id]);
$singleNews=$statement->fetch(PDO::FETCH_OBJ);



$sql3="SELECT commenterName,comment
from comments
JOIN newstable ON newstable.id=comments.newsTableId
where comments.newsTableId='{$id}'
";
$statement=$connection->prepare($sql3);
$statement->execute();
$singleComment=$statement->fetchAll(PDO::FETCH_OBJ);


?>
<div class="container width-300px bg-dark text-light mt-5 p-5"> 
    <h4 class="ml-5">News Portal</h4>
<?php
if(isset( $datainertMsg)){
echo  $datainertMsg;
}
?>
        <div class="card  mb-4 bg-info" style="width:100%;">
    <img class="card-img-top" src='img/<?=$singleNews->newsImage;?>' alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title"><?=$singleNews->newsTitle;?></h5>
      <p class="card-text"><?='<b>Reporter:'.$singleNews->reporterName.'&nbsp; &nbsp; &nbsp;Location:'.$singleNews->location.'</b><br>'.$singleNews->fullNews;?></p>
  
<?php
      foreach($singleComment as $comment){
    ?>
  
    <dl>
    <dt>Name: <?=$comment->commenterName;?></dt>
    <dd>Comment:  <?=$comment->comment;?></dd>
  </dl>
<?php
  }
  // header('Location:view.php');
   
?>



      <a href="createRelatedNews.php?id=<?=$singleNews->id;?>" class="btn btn-primary">Add related news</a>

        <p class="mt-5"><a class="btn btn-primary" href="index.php">Go to homepage</a></p>
 
</div>
<div >
<form method="post" action="commentStore.php"  enctype="multipart/form-data"> 
<div class="form-group col-3">
    <label> Commenter Name:</label>
    <input class="form-control" type="text" name="commenterName">
    </div>
<div class="form-group  col-3">
    <label>Comment:</label><br>
    <textarea name="comment" 
    id=""
    placeholder="comment here"
     cols="40"
      rows="4"></textarea>
    </div>
    <input name="id" type="hidden" value='<?=$id;?>'>
    <button class="btn btn-primary ml-3" type="submit">  Save Comment</button>
</form>
</div>

<?php
require 'footer.php';

?>