<?php
require 'db.php';

$data=$_POST;


if(array_key_exists('id',$data)){
    $id=$data['id'];
}

if(array_key_exists('newsTitle',$data)){
    $newsTitle=$data['newsTitle'];
}

if(array_key_exists('newsDetails',$data)){
    $newsDetails=$data['newsDetails'];
}

if(array_key_exists('relatedNewsTitle',$data)){
    $relatedNewsTitle=$data['relatedNewsTitle'];
}
if(array_key_exists('relatedNewsDetails',$data)){
    $relatedNewsDetails=$data['relatedNewsDetails'];
}
if(array_key_exists('reporterName',$data)){
    $reporterName=$data['reporterName'];
}
if(array_key_exists('location',$data)){
    $location=$data['location'];
}


$sql='UPDATE newsdate set
newsTitle=:newsTitle,
newsImage=:newsImage,
newsDetails=:newsDetails,
relatedNewsTitle=:relatedNewsTitle,
relatedNewsImage=:relatedNewsImage,
relatedNewsDetails=:relatedNewsDetails,
reporterName=:reporterName,
location=:location

 WHERE id=:id ';
$statement=$connection->prepare($sql);
if($statement->execute([

 ':id'=>$id,
':newsTitle'=>$newsTitle,
':newsImage'=>$newsImage,
':newsDetails'=>$newsDetails,
':relatedNewsTitle'=>$relatedNewsTitle,
':relatedNewsImage'=>$relatedNewsImage,
':relatedNewsDetails'=>$relatedNewsDetails,

':reporterName'=>$reporterName,
':location'=>$location

    
    ])){
    echo 'data updated successfully';
}

header("Location:index.php");
 
