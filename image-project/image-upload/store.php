<?php
require 'db.php';

if(strtoupper($_SERVER['REQUEST_METHOD'])=="POST"){
   $data=$_POST;
}

// echo '<pre>';
// // print_r($data);
// print_r($_FILES);
// echo '</pre>';

$reporterName=$data['reporterName'];
$location=$data['location'];


$newsTitle=$data['newsTitle'];
$newsDetails=$data['newsDetails'];
$fullNews=$data['fullNews'];
$newsImage = $_FILES['newsImage']['name'];
$target = "img/".basename($newsImage);
if(move_uploaded_file($_FILES['newsImage']['tmp_name'], $target)) 
{
    $msg = "Image uploaded successfully";
}
else{
    $msg = "Failed to upload image";
}

$relatedNewsTitle=$data['relatedNewsTitle'];

$relatedNewsImage = $_FILES['relatedNewsImage']['name'];
$relatedNewsDetails='';
if(isset($data['relatedNewsDetails'])){
    $relatedNewsDetails=$data['relatedNewsDetails'];

}
if(isset($data['relatedFullNews'])){
    $relatedFullNews=$data['relatedFullNews'];

}
$target1 = "img/".basename($relatedNewsImage);


if (move_uploaded_file($_FILES['relatedNewsImage']['tmp_name'], $target1)) {
    $msg = "Image uploaded successfully";
}else{
    $msg = "Failed to upload image";
}

$sql2='INSERT INTO newstable(newsTitle,newsImage,newsDetails,fullNews) Values(:newsTitle,:newsImage,:newsDetails,:fullNews)';
$statement1=$connection->prepare($sql2);
if($statement1->execute([
    ':newsTitle'=>$newsTitle,
    ':newsImage'=>$newsImage,
    ':newsDetails'=>$newsDetails,
    ':fullNews'=>$fullNews
    ])){
    echo 'data inserted successfully';
}else{
    echo'something wrong';
}
$newsTableId=$connection->lastInsertId();

$sql='INSERT INTO reportertable(reporterName,location,newsTableId) Values(:reporterName,:location,:newsTableId)';
$statement=$connection->prepare($sql);
if($statement->execute([
    ':reporterName'=>$reporterName,
    ':location'=>$location,
    ':newsTableId'=>$newsTableId

    ])){
    echo 'data inserted successfully';
}

$sql1='INSERT INTO relatednewstable(relatedNewsTitle,relatedNewsImage,relatedNewsDetails,newsTableId,relatedFullNews) Values(:relatedNewsTitle,:relatedNewsImage,:relatedNewsDetails,:newsTableId,:relatedFullNews)';
$statement1=$connection->prepare($sql1);
if($statement1->execute([
    ':relatedNewsTitle'=>$relatedNewsTitle,
    ':relatedNewsImage'=>$relatedNewsImage,
    ':relatedNewsDetails'=>$relatedNewsDetails,
    ':newsTableId'=>$newsTableId,
    ':relatedFullNews'=>$relatedFullNews

    ])){
    echo 'data inserted successfully';
}else{
    echo'something wrong';
}


$id=$connection->lastInsertId();


header("Location:index.php"); 

