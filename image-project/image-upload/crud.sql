create DATABASE newsblog;
use newsbloag;
create table newsTable(
    id int(11) auto_increment primary key,
    newsTitle varchar(60) not null,
    newsImage varchar(30) not null,
    newsDetails varchar(250)not null
    )

create table relatedNewsTable(
    id int(11) auto_increment primary key,
    relatedNewsTitle varchar(60) not null,
    relatedNewsImage varchar(30) not null,
    relatedNewsDetails varchar(250)not null,
    newstableId int(11),
    FOREIGN KEY(newstableId) REFERENCES newstable(id)
)

create table reporterTable(
    id int(11) auto_increment primary key,
    reporterName varchar(30) not null,
    location varchar(50) not null,
    newstableId int(11),
    FOREIGN KEY(newstableId) REFERENCES newstable(id)
)

CREATE TABLE newscomment
( id int(11) PRIMARY KEY AUTO_INCREMENT not null,
 comment text,
  newsTableId int(11), 
  relatedNewsTableId int(11),
   FOREIGN KEY (newsTableId) REFERENCES newstable(id) ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY (relatedNewsTableId) REFERENCES relatednewstable(id) ON DELETE CASCADE ON UPDATE CASCADE
  )


  ALTER TABLE Orders
ADD FOREIGN KEY (PersonID) REFERENCES Persons(PersonID);