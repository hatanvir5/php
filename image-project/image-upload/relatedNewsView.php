<?php
session_start();
require 'db.php';
if(array_key_exists('id',$_GET)){
  $id=$_GET['id'];

}

if(array_key_exists('id',$_SESSION)){
  $id=$_SESSION['id'];
  unset($_SESSION['id']);
}


$sql='SELECT
*
  FROM newstable  
  JOIN reportertable ON newstable.id=reportertable.newsTableId 
  JOIN relatednewstable  ON  newstable.id=relatednewstable.newsTableId
  WHERE relatednewstable.newsTableId=:id
  ORDER BY relatednewstable.id DESC';
$statement=$connection->prepare($sql);
$statement->execute([':id'=>$id]);
$news=$statement->fetchAll(PDO::FETCH_OBJ);


?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">


    <title>News</title>
  </head>
  <body class=" container">

<h4 class="text-center pt-3 pb-3">Related News</h4>
<hr>
  <?php
  
   echo' <div  class="row mt-5">';

    foreach($news as $singleNews){
    ?>
    
    <div class="card col-md-4">
    <img class="card-img-top w-100 h-25" src='img/<?=$singleNews->relatedNewsImage;?>' alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title"><?=$singleNews->relatedNewsTitle;?></h5>
    
      <p class="card-text"><?='<b>Reporter:'.$singleNews->reporterName.'&nbsp; &nbsp; &nbsp;Location:'.$singleNews->location.'</b><br>'.$singleNews->relatedNewsDetails;?></p>
      

      <a href="singleRelatedNewsView.php?id=<?=$singleNews->id;?>" class="btn btn-primary">See more</a>

  
      <a href="edit.php?id=<?=$singleNews->id;?>" class="btn btn-primary">Edit</a>
      <form action="delete.php" method="POST">
        <input type="hidden" name="id" value="<?=$singleNews->id;?>">
        <button class="btn btn-primary" type="submit">Delete</button>
      </form>
 
    </div>
  </div>
<?php
  }
?>


</div>
  





















<a class="btn btn-primary mt-3" href="index.php">Homepage</a>







    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </body>
</html>