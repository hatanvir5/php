<?php
require 'db.php';
$id=$_GET['id'];


$sql='SELECT *
  -- newstable.newsImage,newstable.newsTitle,newstable.newsDetails,newstable.fullNews
  FROM newstable  
  JOIN reportertable ON newstable.id=reportertable.newsTableId 
  -- JOIN relatednewstable  ON  newstable.relatedNewsId=relatednewstable.id
  WHERE newstable.id=:id';
$statement=$connection->prepare($sql);
$statement->execute([':id'=>$id]);
$singleNews=$statement->fetch(PDO::FETCH_OBJ);



if(strtoupper($_SERVER['REQUEST_METHOD']=='POST')){
$data=$_POST;
if(array_key_exists('newsTitle',$data)){
  $newsTitle=$data['newsTitle'];
}

if(array_key_exists('newsDetails',$data)){
  $newsDetails=$data['newsDetails'];
}

if(array_key_exists('fullNews',$data)){
  $fullNews=$data['fullNews'];
}

if(array_key_exists('reporterName',$data)){
  $reporterName=$data['reporterName'];
}
if(array_key_exists('location',$data)){
  $location=$data['location'];
}


$editNewsImage = $_FILES['editNewsImage']['name'];
$tmp_dir = $_FILES['editNewsImage']['tmp_name'];
$imgSize = $_FILES['editNewsImage']['size'];

if($editNewsImage)
{
$upload_dir = 'img/'; // upload directory 
$imgExt = strtolower(pathinfo($editNewsImage,PATHINFO_EXTENSION)); // get image extension
$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
$newsPicture = rand(1000,1000000).".".$imgExt;
if(in_array($imgExt, $valid_extensions))
{   
if($imgSize < 5000000)
{
 unlink($upload_dir.$singleNews->newsImage);
 move_uploaded_file($tmp_dir,$upload_dir.$newsPicture);
}
else
{
 $errMSG = "Sorry, your file is too large it should be less then 5MB";
}
}
else
{
$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";  
} 
}
else
{
// if no image selected the old image remain as it is.
$newsPicture = $singleNews->newsImage; // old image from database
}

$sql='UPDATE newstable set
newsTitle=:newsTitle,
newsImage=:newsImage,
newsDetails=:newsDetails,
fullNews=:fullNews
WHERE id=:id ';
$statement=$connection->prepare($sql);
if($statement->execute([

':id'=>$id,
':newsTitle'=>$newsTitle,
':newsImage'=>$newsPicture,
':newsDetails'=>$newsDetails,
':fullNews'=>$fullNews

  
  ])){
  echo 'data updated successfully';
  // header("Location:index.php"); 
}


$sql1='UPDATE reportertable set
reporterName=:reporterName,
location=:location
WHERE reportertable.newsTableId=:id ';
$statement1=$connection->prepare($sql1);
if($statement1->execute([
':id'=>$id,
':reporterName'=>$reporterName,
':location'=>$location
  ])){
  echo 'data updated successfully';
  header("Location:index.php"); 
}

}

?>

<?php
require 'header.php';
?>

<div class="container bg-light col-md-6">
  <h3 class="mt-3 text-dark">News Portal</h3>
<?php
if(isset($errMSG)){
  echo $errMSG;
}
?>
  <form method="post" action="" enctype="multipart/form-data"> 
  <!-- <input type="hidden" name="size" value="1000000"> -->

    <div class="form-group">
    <label> Reporter Name:</label>
    <input class="form-control" type="text" value="<?=$singleNews->reporterName;?>" name="reporterName">
    </div>

    <div class="form-group">
    <label>Location:</label>
    <input class="form-control" value="<?=$singleNews->location;?>" type="text" name="location">
    </div>
    <div class="form-group">
    <label>News Title:</label>
    <input class="form-control" value="<?=$singleNews->newsTitle;?>" type="text" name="newsTitle">
    </div>
    <div class="form-group">
    <label>Select  News Image:</label><br>
<p> <img src="img/<?= $singleNews->newsImage;?>"  height="200px" ></p>

    <p><img src="<?php $singleNews->newsImage;?>"></p>
    <input class="form-control-file" type="file" value="<?php $singleNews->newsImage;?>" name="editNewsImage">
    </div>

    <div class="form-group">
    <label>News in short:</label><br>
    <textarea name="newsDetails" 
    id=""
    placeholder="say something about this image"
     cols="40"
      rows="4"><?=$singleNews->newsDetails;?></textarea>
    </div>
    <div class="form-group">
    <label>Full News:</label><br>
    <textarea name="fullNews" 
    id=""
     cols="40"
      rows="4"><?=$singleNews->fullNews;?></textarea>
    </div>

  
      <button class="btn btn-success mb-5" type="submit">Submit</button>

</form>
</div>
<?php


require 'footer.php';
?>

