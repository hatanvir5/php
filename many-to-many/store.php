<?php 
require 'db.php';

 if(strtoupper($_SERVER['REQUEST_METHOD'])=="POST"){
     $data=$_POST;
 }
 $name=$data['name'];
 $fatherName=$data['fatherName'];
 $motherName=$data['motherName'];
 $birthdate=$data['birthdate'];
 $email=$data['email'];

 $physics='';
if(array_key_exists('physics',$data)){
    $physics=$data['physics'];
}
 $chemistry='';
if(array_key_exists('chemistry',$data)){
    $chemistry=$data['chemistry'];
}
$biology='';
if(array_key_exists('biology',$data)){
    $biology=$data['biology'];
}
$math='';
if(array_key_exists('math',$data)){
    $math=$data['math'];
}


 $sqlProfiles='INSERT into profiles(name,fatherName,motherName,birthdate,email) values(:name,:fatherName,:motherName,:birthdate,:email)';
 $statement=$connection->prepare($sqlProfiles);
 if($statement->execute([
    ':name'=>$name,
    ':fatherName'=>$fatherName,
    ':motherName'=>$motherName,
   ':birthdate'=>$birthdate,
   ':email'=>$email
])){
     echo 'data inserted successfully in profiles';
 }
 else{
     echo 'something wrong';
 }
 $profileId=$connection->lastInsertId();

 $sqlEducation='INSERT into subjects(physics,chemistry,biology,math) values(:physics,:chemistry,:biology,:math)';
 $statementEducation=$connection->prepare($sqlEducation);
 if($statementEducation->execute([
    ':physics'=>$physics,
    ':chemistry'=>$chemistry,
    ':biology'=>$biology,
   ':math'=>$math
])){
     echo 'data inserted successfully in subjects table';
 }
 else{
     echo 'something wrong';
 }
 $subjectId=$connection->lastInsertId();
 $sqlStudents='INSERT into students(profileId,subjectId) values(:profileId,:subjectId)';
 $statementStudents=$connection->prepare($sqlStudents);
 if($statementStudents->execute([
    ':profileId'=>$profileId,
    ':subjectId'=>$subjectId
])){
     echo 'data inserted successfully in students';
 }
 else{
     echo 'something wrong';
 }
header('location:index.php');