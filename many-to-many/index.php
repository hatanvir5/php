<?php
require 'db.php';
$sql='SELECT * FROM students st
join profiles p ON p.profileId=st.profileId
join subjects s ON s.subjectId=st.subjectId
 ';
 $statement=$connection->prepare($sql);
 $statement->execute();
 $students=$statement->fetchAll(PDO::FETCH_OBJ);

 $physicsSql='SELECT p.name FROM students st
 join profiles p ON p.profileId=st.profileId
 join subjects s ON s.subjectId=st.subjectId
 WHERE s.physics=1';
  $statement=$connection->prepare($physicsSql);
  $statement->execute();
  $physics=$statement->fetchAll(PDO::FETCH_OBJ);
 
  $chemistrySql='SELECT p.name FROM students st
  join profiles p ON p.profileId=st.profileId
  join subjects s ON s.subjectId=st.subjectId
  WHERE s.chemistry=1';
   $statement=$connection->prepare($chemistrySql);
   $statement->execute();
   $chemistry=$statement->fetchAll(PDO::FETCH_OBJ);


   $biologySql='SELECT p.name FROM students st
   join profiles p ON p.profileId=st.profileId
   join subjects s ON s.subjectId=st.subjectId
   WHERE s.biology=1';
    $statement=$connection->prepare($biologySql);
    $statement->execute();
    $biology=$statement->fetchAll(PDO::FETCH_OBJ);

    $mathSql='SELECT p.name FROM students st
    join profiles p ON p.profileId=st.profileId
    join subjects s ON s.subjectId=st.subjectId
    WHERE s.math=1';
     $statement=$connection->prepare($mathSql);
     $statement->execute();
     $math=$statement->fetchAll(PDO::FETCH_OBJ);

 ?>




<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Students info</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<style>
	.bs-example{
    	margin: 20px;
    }
</style>
</head>
<body>
<div class="bs-example">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a href="#home" class="nav-link active" data-toggle="tab">Student subject choose</a>
        </li>
        <li class="nav-item">
            <a href="#profile" class="nav-link" data-toggle="tab">Subject Student choose</a>
        </li>
     </ul>
    <div class="tab-content">
        <div class="tab-pane fade show active" id="home">
            <h4 class="mt-2">Student subject choose</h4>
           


            <table  class="table table-striped table-bordered">
            <thead>
                <tr>
              
                <th scope="col">Name</th>
                <th scope="col">Father Name</th>
                <th scope="col">Mother Name</th> 
                <th scope="col">Birthdate</th> 

                <th scope="col">Email</th>
                <th scope="col">Subjects</th>
                <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php
                    foreach($students as $student){
            ?>



                <tr>
                <td><?=$student->name;?></td>
                <td><?=$student->fatherName;?></td>
                <td><?=$student->motherName;?></td>
                <td><?=$student->birthdate;?></td>
                <td><?=$student->email;?></td>


                <td>
                    <ul>
                     <?php if($student->physics==1){?>
                        <li> <input class="form-check-input" type="checkbox" name="physics" checked disabled> Physics</li>

                      <?php  }?>
                      <?php  if( $student->chemistry==1){?>
                        <li> <input class="form-check-input" type="checkbox" name="chemistry" disabled checked >chemistry</li>

                        <?php  }?>
                        <?php   if( $student->biology==1){?>
                            <li><input class="form-check-input" type="checkbox" name="biology"  checked disabled>Biology</li>

                        <?php  }?>
                        <?php   if( $student->math==1){?>
                            <li><input class="form-check-input" type="checkbox" name="math" checked disabled> Math </li>

                        <?php  }?>
                      
                    </ul>
                    
                </td>
                <td>Edit | Delete</td>

                </tr>

            <?php
                    }

            ?>
            </tbody>
            </table>


            <a href="create.php">Add</a>


        </div>
        <div class="tab-pane fade" id="profile">
            <h4 class="mt-2">Subject Student choose</h4>
            
            <table  class="table table-striped table-bordered">
            <thead>
                <tr>
                <th scope="col">Physics</th>
                <th scope="col">Chemistry</th>
                <th scope="col">Biology</th>
                <th scope="col">Math</th>
            
                </tr>
            </thead>
            <tbody>
                <tr>

              

           
                <td>
                <?php
                    foreach($physics as $physicsStudent){?>
                        <li>Name:<?=$physicsStudent->name;?> </li>
                <?php }?>
                
                </td>

                <td>
                <?php
                    foreach($chemistry as $chemistryStudent){?>
                        <li>Name:<?=$chemistryStudent->name;?> </li>
                <?php }?>
                
                </td>

                <td>
                <?php
                    foreach($biology as $biologyStudent){?>
                        <li>Name:<?=$biologyStudent->name;?> </li>
                <?php }?>
                
                </td>

                <td>
                <?php
                    foreach($math as $mathStudent){?>
                        <li>Name:<?=$mathStudent->name;?> </li>
                <?php }?>
                
                </td>




                </tr>
                
            </tbody>
            </table>

        </div>
    
    </div>
</div>
</body>
</html>