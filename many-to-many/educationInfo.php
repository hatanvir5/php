<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Education profile</title>
  </head>
  <body>
<div class="container  col-md-6">
<h1 class="mt-5">Education profile</h1>

<form action="store.php" method="POST">
  <div class="form-group">
    <label for="exampleInputEmail1">JSC GPA</label>
    <input type="text" class="form-control text-center" id="exampleInputEmail1" name="jscGPA" >
  
  </div>
  <div class="form-group">
    <label for="">SSC GPA</label>
    <input type="text" class="form-control text-center" id="exampleInputPassword1" name="sscGPA" >
  </div>
  <div class="form-group">
    <label for="" class="text-left">HSC GPA</label>
    <input type="text" class="form-control text-center" id="exampleInputPassword1" name="hscGPA" >
  </div>
  <div class="form-group">
    <label for="">	Honours CGPA	</label>
    <input type="text" class="form-control text-center" id="exampleInputPassword1" name="honoursCGPA" >
  </div>

  <button type="submit" class="btn btn-primary">save</button>
</form>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>