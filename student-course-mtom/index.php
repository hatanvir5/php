<?php
require 'db.php';

$studentList=' SELECT * from studentsInfo';
$statement=$connection->prepare($studentList);
$statement->execute();
$students=$statement->fetchAll(PDO::FETCH_OBJ);


$courseList=' SELECT * from courseInfo';
$statement=$connection->prepare($courseList);
$statement->execute();
$courses=$statement->fetchAll(PDO::FETCH_OBJ);


if(strtoupper($_SERVER['REQUEST_METHOD'])=='POST'){
           
    $courseList=$_POST['courses'];
    $studentId=$_POST['studentId'];
   
    foreach($courseList as $singleCourse):
        $sql='INSERT into studentscourse(studentId,courseId) values(:studentId,:courseId)';
        $insertCourseList=$connection->prepare($sql);
        $result=$insertCourseList->execute([':studentId'=>$studentId,':courseId'=>$singleCourse]);
        
    endforeach;
    if($result){
        $successMsg= 'data insert successfully in mapping table';
    }




}

 ?>




<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Students info</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<style>
	.bs-example{
    	margin: 20px;
    }
</style>
</head>
<body>
<div class="bs-example">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a href="#home" class="nav-link active" data-toggle="tab">Student subject choose</a>
        </li>
        <li class="nav-item">
            <a href="#profile" class="nav-link" data-toggle="tab">Subject Student choose</a>
        </li>
     </ul>
    <div class="tab-content">
        <div class="tab-pane fade show active" id="home">
            <h4 class="mt-2">Student subject choose</h4>
           


            <table  class="table table-striped table-bordered">
            <thead>
                <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Subjects</th>
                <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
     <?php foreach($students as $student){    
        $studentId=$student->id ;     ?>
<tr>
<td>
        <?=$studentId;?>
    </td>
    <td>
        <?=$student->name;?>
    </td>
    <td>
        <?php
        $selectStudentCourse=' SELECT studentscourse.id,studentscourse.studentId,studentscourse.courseId,studentsinfo.id,studentsinfo.name,
        courseinfo.id,courseinfo.course
                 from studentscourse
                 join studentsinfo ON studentscourse.studentId=studentsinfo.id
                 join courseinfo ON studentscourse.courseId=courseinfo.id
                  Where studentscourse.studentId=:id';
        $statement=$connection->prepare($selectStudentCourse);
        $statement->execute([':id'=>$studentId]);
        $studentCourses=$statement->fetchAll(PDO::FETCH_OBJ);

                 foreach($studentCourses as $studentCourse){
        ?>
                  <li><?=$studentCourse->course;?></li>
                <?php 
                 }
            ?>
    </td>
    <td>
    <a href="addCourse.php?id=<?=$student->id;?>">Add Course</a>  | <a href="editStudent.php">Edit</a> | <a href="deleteStudent.php">Delete</a>
    </td>
</tr>
         

        <?php }?>    



</tbody>
            </table>


            <a href="studentForm.php" class="btn btn-primary">Add New Student</a>


        </div>
        <div class="tab-pane fade" id="profile">
            <h4 class="mt-2">Subject Student choose</h4>
            
            <table  class="table table-striped table-bordered">
            <thead>
            <tr>
              
              <th scope="col">Course</th>
              <th scope="col">Student Name</th>
              <th scope="col">Action</th>
              </tr>
          </thead>
          <tbody>
   <?php foreach($courses as $course):
    $courseId=$course->id;
    ?>          
<tr>
  <td>
      <?=$course->course;?>
  </td>
  <td>
  <?php
        $selectCourseStudent=' SELECT studentscourse.id,studentscourse.studentId,studentscourse.courseId,studentsinfo.id,studentsinfo.name,
        courseinfo.id,courseinfo.course
                 from studentscourse
                 join studentsinfo ON studentscourse.studentId=studentsinfo.id
                 join courseinfo ON studentscourse.courseId=courseinfo.id
                  Where studentscourse.courseId=:id';
        $statement=$connection->prepare($selectCourseStudent);
        $statement->execute([':id'=>$courseId]);
        $studentCourses=$statement->fetchAll(PDO::FETCH_OBJ);

                 foreach($studentCourses as $studentCourse){
        ?>
                  <li><?=$studentCourse->name;?></li>
                <?php 
                 }
            ?>
  </td>
  <td>
<a href="editCourse.php">Edit</a> | <a href="deleteCourse.php">Delete</a>
  </td>
</tr>
       

   <?php endforeach;?>    


                
            </tbody>
            </table>
            <a href="courseForm.php" class="btn btn-primary">Add New Course</a>

        </div>
    
    </div>
</div>
</body>
</html>