<?php
require 'db.php';
$id=$_GET['id'];

$courseList=' SELECT * from courseInfo';
$statement=$connection->prepare($courseList);
$statement->execute();
$courses=$statement->fetchAll(PDO::FETCH_OBJ);



$studentList=' SELECT * from studentsInfo where id=:id';
$statement=$connection->prepare($studentList);
$statement->execute([':id'=>$id]);
$student=$statement->fetch(PDO::FETCH_OBJ);


?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body class="container col-md-6 mt-5">
   <h4>Choose Course </h4>
   <p><b>Student Name:<?=$student->name;?> </b></p>

   <form method="post" action="index.php">
<?php foreach($courses as $course):?>

<div class="form-check ">
<input class="form-check-input" type="checkbox" name="courses[]" value="<?=$course->id;?>" id="defaultCheck1">
<label class="form-check-label" for="defaultCheck1">
  <?=$course->course;?>
</label>
</div>
<?php endforeach;?>
<input type="hidden" name="studentId" value="<?=$id?>">
<button class="btn btn-primary mt-3" type="submit">save</button>
   </form>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </body>
</html>