CREATE TABLE profiles(
profileId int(11) NOT null AUTO_INCREMENT PRIMARY key,
    name varchar(40) not null,
    fatherName varchar(40) not null,
    motherName varchar(40) null,
    birthdate date,
    email varchar(40)
);
CREATE TABLE subjects(
subjectId int(11) NOT null AUTO_INCREMENT PRIMARY key,
   physics varchar(30),
   chemistry varchar(30),
   biology varchar(30),
   math varchar(30)
)

CREATE TABLE students(
id int(11) NOT null AUTO_INCREMENT PRIMARY key,
    profileId int(11),
    subjectId int(11),

     FOREIGN KEY (profileId) REFERENCES profiles(profileId) ON DELETE CASCADE ON UPDATE CASCADE,
     FOREIGN KEY (subjectId) REFERENCES subjects(subjectId)  ON DELETE CASCADE ON UPDATE CASCADE
    
)

create table  customers(
customerId int(11) NOT null AUTO_INCREMENT PRIMARY key,
cName varchar(40) not null,
cAddress varchar(60),
cMobile varchar(15) not null,
cNid varchar(25) not null
);

create table  bankInfo(
bankId int(11) NOT null AUTO_INCREMENT PRIMARY key,
bName varchar(40) not null,
bAccount varchar(60) not null,
bBranch varchar(15) not null,
bAmount varchar(25) not null
);


SELECT p.name FROM students st
join profiles p ON p.profileId=st.profileId
join subjects s ON s.subjectId=st.subjectId
WHERE s.physics=1

create table studentsInfo(
id int(11) NOT null AUTO_INCREMENT PRIMARY key,
name varchar(30)
)


create table courseInfo(
id int(11) NOT null AUTO_INCREMENT PRIMARY key,
course varchar(30)
)

create table studentsCourse(
id int(11) NOT null AUTO_INCREMENT PRIMARY key,
studentId int(11),
courseId int(11),
FOREIGN KEY (studentId) REFERENCES studentsInfo(id) ON DELETE CASCADE,
FOREIGN KEY (courseId) REFERENCES courseInfo(id) ON DELETE CASCADE
)