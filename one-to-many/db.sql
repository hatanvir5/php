CREATE TABLE profiles(
profileId int(11) NOT null AUTO_INCREMENT PRIMARY key,
    name varchar(40) not null,
    fatherName varchar(40) not null,
    motherName varchar(40) null,
    birthdate date
        email varchar(40),
);
CREATE TABLE educations(
educationId int(11) NOT null AUTO_INCREMENT PRIMARY key,
    jscGPA varchar(40) not null,
    sscGPA varchar(40) not null,
    hscGPA varchar(40) null,
    honoursCGPA varchar(10)
)

CREATE TABLE students(
id int(11) NOT null AUTO_INCREMENT PRIMARY key,
profileId int(11) NOT null,
    educationId int(11) not null,

     FOREIGN KEY (profileId) REFERENCES profiles(profileId),
     FOREIGN KEY (educationId) REFERENCES educations(educationId)
    
)