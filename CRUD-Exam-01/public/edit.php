<?php
$products = simplexml_load_file('products.xml');
$validateData=$_POST;
if(isset($validateData['save'])) {

	foreach($products as $product){
		if($product['id']==$validateData['bookId']){
			$product->genre = $validateData['genre'];
           
            $product->title= $validateData['title'];
           
			$product->price = $validateData['price'];
           
           $product->publish_date= $validateData['publishDate'];
            $product->description= $validateData['description'];
        break;
		}
	}
	file_put_contents('products.xml', $products->asXML());
	header('location:index.php');
}

foreach($products as $product){
	if($product['id']==$_GET['bookId']){
        $bookId=$product['id'];
        $title=$product->title;
        $genre=$product->genre;
        $price=$product->price;
        $publishDate=$product->publish_date;
        $description=$product->description;
    break;
	}
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Edit Information</title>
</head>
<body>
    <div class="container">

    <h1>Update Information:</h1>
    
    <form  method="post">
    <label for="">Book Id</label>
    <input type="text" name="bookId" class="form-control" value="<?php echo $bookId;?>" readonly>

    <label for="">Title</label>
    <input type="text" name="title"  class="form-control"  value="<?php echo $title;?>">

    <label for="">Genre</label>
    <input type="text" name="genre" class="form-control"  value="<?php echo $genre;?>">

    <label for="">price </label>
    <input type="text" name="price"  class="form-control"  value="<?php echo $price;?>">

    <label for="">Publish Date </label>
    <input type="date" name="publishDate"  class="form-control"  value="<?php echo $publishDate;?>">

    <label for="">Description </label>  
    <textarea name="description" id="" cols="30" rows="5"  class="form-control"><?php echo $description ;?></textarea>
    <button type="submit" name="save" class="btn btn-primary">Update</button>
   
    </form>
    </div>
</body>
</html>
