<?php
$products = simplexml_load_file('products.xml');



include_once("../vendor/autoload.php");

use Tanvir\Utility;
use Tanvir\Debugger;
use Tanvir\Sanitizer;
use Tanvir\Validator;



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Book List</title>
</head>
<body class="bg-info">
    <h2 class="p-5  text-light text-center">Book List</h2><br>
<table class="table">
          <tr>
       
          <th>Book Id</th>
          <th>Title</th>
          <th>Genre</th>
          <th>Price</th>
          <th>Publish Date</th>
          <th class="text-center">Description</th>
          <th>Action</th>
       
          </tr>
<?php 

if(count($products)<=0){

?>
    <tr><td colspan='8' style="text-align:center;"> No Records Found</td></tr>
<?php
}
else{
?>
          <?php
      
          
         foreach($products->book as $product){
           

          $bookId=$product['id'];
          $title=$product->title;
          $genre=$product->genre;
          $price=$product->price;
          $publishDate=$product->publish_date;
          $description=$product->description;
         
          ?>

           <tr>
            <td><a target="_blank" href="show.php?position=<?php echo $key;?>" class="text-dark"><?php echo $bookId;?></a></td>
            <td><?php echo $title;?></td>
            <td><?php echo $genre;?></td>   
            <td><?php echo $price;?></td>
            <td><?php echo $publishDate;?></td>
            <td class="text-center"><?php echo $description;?></td>
            
            <td> 
            <form action='edit.php' method='get'>
            <input name='bookId' type='hidden' value="<?php echo $bookId;?>">
            <button type='submit' class='btn'>Edit</button>
            </form>
            <form action='destroy.php' method='get'>
            <input name='bookId' type='hidden'value="<?php echo $bookId;?>">
            <button type='submit' class='btn'>Delete</button>
            </form>
      
            </td>
           
            </tr>
<?php
         }



}
?>
        </table>
        <a href="create.php" class="ml-4 btn btn-light">Add</a>
       
        
</body>
</html>