<?php

include_once("../vendor/autoload.php");

use Tanvir\Utility;
use Tanvir\Debugger;
use Tanvir\Sanitizer;
use Tanvir\Validator;

$sanitizedData=Sanitizer::sanitize($_POST);
$validatedData=Validator::validate($sanitizedData);

$catalog = simplexml_load_file('products.xml');

$book = $catalog->addChild('book');
$book->addAttribute('id', $validatedData['bookId']);
$book->addChild('title', $validatedData['title']);
$book->addChild('genre', $validatedData['genre']);
$book->addChild('price', $validatedData['price']);
$book->addChild('publish_date', $validatedData['publishDate']);
$book->addChild('description', $validatedData['description']);

file_put_contents('products.xml', $catalog->asXML());
header('location:index.php');


?>

