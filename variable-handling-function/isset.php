<?php
$a=10;
echo isset($a)."<br>";

if(isset($a)){
    echo "value of variable a is set , the value is $a";
}
else{
    echo "value of variable a is non set, the value is $a";
}


$b=null;
echo isset($b)."<br>";

if(isset($b)){
    echo "value of variable b is set , the value is $b";
}
else{
    echo "value of variable b is non set, the value is null";
}