<?php
$a = "32"; // string
var_dump($a);
settype($a, "integer"); // $a is now integer
var_dump($a);

$b = 32; // integer
var_dump($b);

settype($b, "string"); // $b is now string
var_dump($b);

$c = true; // boolean
var_dump($c);

settype($c, "integer"); // $c is now integer (1)
var_dump($c);

?>