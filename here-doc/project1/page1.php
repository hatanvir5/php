<?php

$header=<<< HEADER



<header class="d-block fixed-top">
    <div class="container">
    <nav class="navbar navbar-expand-lg">

        <a class="navbar-brand" href="index.html"><img src="images/logo.png" class="img-fluid" alt="Al-Shamasy"></a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mx-auto">
                    <li class=" nav-item active">
                        <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="aboutUs.html">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="brand.html">Brand</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="offers.html">Offers</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="ourbranches.html">Our Branches</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="index.html" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Media Center
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="photo.html">Photo Gallary</a>
                            <a class="dropdown-item" href="news.html">News</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ContactUs.html">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"> عربي</a>
                    </li>
                </ul>
            </div>


    </nav>
    </div>
</header>


HEADER;
?>


<?php

$carousel=<<< CAROUSEL

<section id="carousel" >

    <div id="slide" class="carousel slide  ml-2" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="images/Slider-3-1-1560x600.jpg" class="d-block w-100" alt="Slide-image">
            </div>


            <div class="carousel-item">
                <img src="images/Slider-2-1-1560x600.jpg" class="d-block w-100" alt="Slide-image">
            </div>
          


        </div>
        <a class="carousel-control-prev" href="#slide" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#slide" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

</section>
CAROUSEL;


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Al Shamasy For Bags</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" type="image/png" href="images/pin-icon.png">
</head>
<body>


<?php

echo $header;
?>

<?php

echo $carousel;
?>






<section id="about">
    <div class="container">
       <h2 class="text-center pt-5 mb-3">About Us</h2>

       <p class="mb-3">OUR VISION : Our vision is meeting to our responsibility towards our clients and we look forward to be the  resource number(1)in the field of luggage and leather manufactures in the kingdom of Saudi Arabia.</p>



       <p class="mb-3"> OUR MISSION : Providing the distinguished products that justifies our clients requirements of luggage and excellent leather and providing the service beyond  the  clients  all over the kingdom  of Saudi Arabia.</p>


        <div class="mb-3">
            <p>Values :</p>
            <ol>
                <li>Flexibility.</li>
                <li>Creativity.</li>
                <li>Achievement.</li>
                <li>Excellence.</li>
                <li>Cooperation.</li>
            </ol>



            <p class="mb-3"> Objectives :</p>
            <ol>
                <li>- Expanding all over the kingdom and the Arab gulf.</li>
                <li>- Making qualitative serious turning in the field of luggage and leather manufactures  and travel  accessories.</li>
                <li>- Providing high products and services through creativity , excellence and designing.</li>

            </ol>
        </div>

        <div class="card-group">
            <div class="card">
                <img class="card-img-top img-thumbnail" src="images/UnitedColorsOfBenetton-170x57.png" alt="Card image cap">

            </div>
            <div class="card">
                <img class="card-img-top img-thumbnail" src="images/The-Bridge-wayfarer1-170x21.png" alt="Card image cap">

            </div>
            <div class="card">
                <img class="card-img-top img-thumbnail" src="images/5.png" alt="Card image cap">

            </div>
            <div class="card">
                <img class="card-img-top img-thumbnail" src="images/6.png" alt="Card image cap">

            </div>
            <div class="card">
                <img class="card-img-top img-thumbnail" src="images/tommy-hilfiger-170x18.png" alt="Card image cap">

            </div>
            <div class="card">
                <img class="card-img-top img-thumbnail" src="images/Victoria1-170x39.png" alt="Card image cap">

            </div>
        </div>

    </div>
</section>

<section id="latestproduct">
    <h2 class="text-center mb-4 pt-4">Latest product</h2>



    <div class="dropdown text-center mt-4 pb-4">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           More
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="photo.html">Photo Gallary</a>
            <a class="dropdown-item" href="news.html">Update News</a>
            <a class="dropdown-item" href="news.html">Latest News</a>
        </div>
    </div>





</section>
<section id="form">
    <div class="container">
    <form class="text-center">
    <h2>Newsletter Subscription</h2>
        <label for="email">
    <input type="email" id="email" name="email" value="" placeholder="write your email here">
        <button type="submit"><i class="fas fa-location-arrow"></i></button></label>
    </form>
    </div>
</section>

<section id="developer">
    <div class="container">
        <h3 class="text-center mb-3 pt-5">About Developer</h3>
    <div class="row pb-3">

        <div class="col-md-4">
            <img src="images/DSC_0992-2.jpg" class="img-fluid img-thumbnail" alt="Developer-image">
                </div>

        <div class="col-md-8">
            <ul class="custom-radio">
                <li><p><b>Name:</b>   Tanvir Ahmad</p></li>
                <li> <p><b>Study:</b> B.Sc in CSE,International Islamic University Chittagong</p></li>
                <li><p><b>Id:</b> C163078</p></li>
                <li><p><b>Instructor:</b> Mr. Md Mahmudur Rahman</p></li>
                <li><i class="fab fa-facebook-square"></i><a href="https://www.facebook.com/satanvir123" target="_blank">&dbkarow; facebook.com/satanvir123</a> </li>
                <li><i class="fas fa-globe"></i> <a href="https://www.CreativeCorners.online" target="_blank">&dbkarow; CreativeCorners.online</a></li>
                <li><i class="fas fa-envelope-open-text"></i> <a href="https://www.gmail.com/Satanvir5@gmail.com" target="_blank">&dbkarow; Satanvir5@gmail.com</a></li>
            </ul>

        </div>
    </div>
    </div>
</section>
<footer class="pt-4 pb-4">
    <div class="container">
    <div class="row">
        <div class="col-8 col-xs-8">
            <p class="pt-2">&copy; Copyright Al Shamasy For Bags 2019</p>
        </div>
        <div class="col-4 col-xs-4 text-right">
            <ol class="m-0 p-0" >
                <li class="d-inline-block"><a href="www.facebook.com"><i class="fab fa-facebook-square fa-2x"></i></a></li>
                <li class="d-inline-block"><a href="www.youtube.com"><i class="fab fa-youtube fa-2x "></i></a></li>
                <li class="d-inline-block"><a href="www.googleplus.com"><i class="fab fa-google-plus-square fa-2x "></i></a></li>
            </ol>
        </div>
    </div>
    </div>
</footer>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js" crossorigin="anonymous"></script>
<script>$('.carousel').carousel({
    interval: 1500
})</script>
</body>
</html>
