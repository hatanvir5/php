<?php

$number=123;

echo "$number"."<br>";   //heredoc
echo '$number';  //now doc




$heredoc=<<<NUMBER

    <h1>This is Heredoc , here we show  variable as like double qoute: </h1>
    number= $number

NUMBER;

echo $heredoc;




$nowdoc=<<<'NUMBER'

    <h1>This is Nowdoc , here we show  variable as like single qoute: </h1>
    number= $number

NUMBER;

echo $nowdoc;

