<?php
//heredoc
$name="Name";
$password="Password";
$email="Email";
$phone="Phone";
$gender="Gender";


$nowdoc=<<<FORM


<form action="" method="POST">
<div class="form-group">
  <label for="userFullName">$name:</label>
  <input
    type="text"
    placeholder="Enter Your Full Name"

    name="userFullName"
    class="form-control"
  />
</div>

<div class="form-group">
  <label for="userEmail">$email:</label>
  <input
    type="email"
    placeholder="Enter Your Email"

    name="userEmail"
    class="form-control"
  />
</div>
<div class="form-group">
  <label for="userPassword">$password:</label>
  <input
    type="password"
    placeholder="Enter Your password"

    name="userPassword"
    class="form-control"
  />
</div>
<div class="form-group">
  <label for="userPhone">$phone:</label>
  <input
    type="tel"
    placeholder="Enter Your Phone Number"
  
    name="userPhone"
    class="form-control"
  />
</div>

<div class="form-group">
  <p>select your $gender</p>

    <input type="radio" name="gender" value="male" />  <label for=""> Male
  </label>

    <input type="radio" name="gender" value="female" />   <label for=""> Female
  </label>
</div>


  <input
    type="submit"
    value="Sign Up"
    name="registration"
    class="btn btn-success btn-block btn-lg"
  />

</form>


FORM;




?>





<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
      crossorigin="anonymous"
    />
  </head>

<body>
  <div class="container">
  <div class="row">
  <div class="col-8">
        <h2 class="text-success">Registration Form</h2>
      
<?php echo $nowdoc; ?>

      </div>
      <div class="col-4">
      <h3 class="text-primary">
      Login
      </h3>

      <h3 class="text-danger">
   
      </h3>
      <form action="" method="POST">
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" class="form-control" id="exampleInputEmail1" name="login_email" aria-describedby="emailHelp">
              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" name="login_password" class="form-control" >
            </div>
           
            <input type="submit" class="btn btn-success btn-block btn-lg" value="submit" name="login">
          </form>
      </div>
  </div>

</div>
  </body>
</html>
