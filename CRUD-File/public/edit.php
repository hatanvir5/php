<?php
$file='file.txt';

$Position=$_POST['position'];

if(isset($file)){
    $data=unserialize(file_get_contents($file));
}
  $singleInfo=$data[$Position];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Edit Information</title>
</head>
<body>
    <div class="container">

    <h1>Update Information:</h1>
    
    <form action="update.php" method="post">

    <label for="">Book Name</label>
    <input type="text" name="bookName" value="<?php echo $singleInfo['bookName'];?>" class="form-control">

    
    <label for="">Author Name</label>
    <input type="text" name="authorName" value="<?php echo $singleInfo['authorName'];?>" class="form-control">
  
    <label for="">Email</label>
    <input type="email" name="email" value="<?php echo $singleInfo['email'];?>" class="form-control">
    

    <label for="">ISBN</label>
    <input type="text" name="isbn"  value="<?php echo $singleInfo['isbn'];?>" class="form-control">

    <label for="">Review</label>  
    <textarea name="review" cols="30" rows="5" class="form-control"><?php echo $singleInfo['review'];?></textarea>

   <input type="hidden" name="position"  value="<?php echo $Position;?>" class="form-control">
    <button type="submit"  class="btn btn-primary">Update</button>
    </form>
    </div>
</body>
</html>
