<?php
$file='file.txt';

include_once("../vendor/autoload.php");

use Tanvir\Utility;
use Tanvir\Debugger;
use Tanvir\Sanitizer;
use Tanvir\Validator;

$sanitizedData=Sanitizer::sanitize($_POST);
$validatedData=Validator::validate($sanitizedData);



$data=[];


if(isset($file)){
    $data=unserialize(file_get_contents("$file"));

}



$data[]=$validatedData;

$strData=serialize($data);

file_put_contents("$file","$strData");

header('location:index.php');
    

?>

