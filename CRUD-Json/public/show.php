<?php
$file='file.txt';
$positon=$_GET['position'];
$data=unserialize(file_get_contents($file));
$singleData=$data[$positon];

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Book Details</title>
</head>
<body>
<h2>Book Details</h2>
<dl>
  <dt>Book:</dt>
  <dd><?php echo $singleData['bookName'];?></dd>
  <dt>Author:</dt>
  <dd><?php echo $singleData['authorName'];?></dd>
  <dt>Email: </dt>
  <dd><?php echo $singleData['email'];?></dd>
  <dt>ISBN:</dt>
  <dd><?php echo $singleData['isbn'];?></dd>
  <dt>Review:</dt>
  <dd><?php echo $singleData['review'];?></dd>
</dl>
</body>
</html>