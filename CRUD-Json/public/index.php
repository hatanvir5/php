<?php
$file='file.txt';
include_once("../vendor/autoload.php");

use Tanvir\Utility;
use Tanvir\Debugger;
use Tanvir\Sanitizer;
use Tanvir\Validator;

$data=[];
if(isset($file)){
    $data=unserialize(file_get_contents("$file"));

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Book List</title>
</head>
<body class="bg-info">
    <h2 class="p-5  text-light text-center">Book List</h2><br>
<table class="table">
          <tr>
       
          <th>Book Name</th>
          <th>Author Name</th>

          <th>Email</th>
          
          <th>ISBN</th>
          <th>Review</th>
          <th>Action</th>
       
          </tr>
<?php 

if(count($data)<=0){

?>
    <tr><td colspan='8' style="text-align:center;"> No Records Found</td></tr>
<?php
}
else{
?>
          <?php
      
          
         foreach($data as $key=> $singleData){
           

          $bookName=$singleData['bookName'];
          $authorName=$singleData['authorName'];

      

          $email=$singleData['email'];

          $isbn=$singleData['isbn'];
         
         
          $review=$singleData['review'];
          
          ?>

           <tr>
            <td><a target="_blank" href="show.php?position=<?php echo $key;?>" class="text-dark"><?php echo $bookName;?></a></td>
           
            <td><?php echo $authorName;?></a></td>
            <td><?php echo $email;?></td>
            <td><?php echo $isbn;?></td>
           
            <td><?php echo $review;?></td>
            <td> 
            
            <form action='edit.php' method='post'>
            <input name='position' type='hidden' value=<?php echo $key?>>
            <button type='submit' class='btn'>Edit</button>
            </form>
            <form action='destroy.php' method='post'>
            <input name='position' type='hidden'value=<?php echo $key?>>
            <button type='submit' class='btn'>Delete</button>
            </form>
      
            </td>
           
            </tr>
<?php
         }



}
?>

          
        </table>
        <a href="create.php" class="ml-4 btn btn-light">Add</a>
       
        
</body>
</html>