<?php
$catalog = simplexml_load_file('products.xml');



include_once("../vendor/autoload.php");

use Tanvir\Utility;
use Tanvir\Debugger;
use Tanvir\Sanitizer;
use Tanvir\Validator;



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Product List</title>
</head>
<body class="bg-info">
    <h2 class="p-5  text-light text-center">Product List</h2><br>
<!-- <table class="table">
          <tr>
          <th>Catalog Item</th>
          <th>Product Image</th>
          <th>Item Number</th>
          <th>Price</th>
          <th>size </th>
          <th>Color Swatch</th>
          
         
          <th>C.S. image</th>
          <th>Action</th>
       
          </tr>
          </table> -->
<?php 

if(count($catalog)<=0){

?>
    <tr><td colspan='8' style="text-align:center;"> No Records Found</td></tr>
<?php
}
else{
?>
          <?php
      
          
         foreach($catalog as $product):
            
           foreach($product as $catalogItem):


        //   $catalog_item=$product->catalog_item['gender'];
        //   $productImage=$product['product_image'];
        //   $item_number=$product->catalog_item->item_number;
        //   $price=$product->catalog_item->price;
        //   $size=$product->catalog_item->size['description'];

        //   $colorSwatch1=$product->catalog_item->size->color_swatch[0];
        //   $colorSwatch2=$product->catalog_item->size->color_swatch[1];

        //   $colorSwatchImage1=$product->catalog_item->size->color_swatch[0]['image'];
        //   $colorSwatchImage2=$product->catalog_item->size->color_swatch[1]['image'];
           
          ?>
<table class="table">
    <tr>

      <td>
          <dl>
            <dt>Catalog Item</dt>
            <dd><?= $catalogItem['gender'];?></dd>

          </dl>
            
      </td>

      <td>
          <dl>
            <dt>Product Image</dt>
            <dd>  <?= $product['product_image'];?></dd>
          </dl>        
      </td>

      <td>
          <dl>
            <dt>Item Number</dt>
            <dd> <?= $catalogItem->item_number;?></dd>
          </dl>        
      </td>
      <td>
          <dl>
            <dt>Price</dt>
            <dd> <?= $catalogItem->price;?></dd>
          </dl>        
      </td>
            
            <?php foreach($catalogItem->size as $size):?>
            <td>
            <dl>
            <dt>Size</dt>
            <dd><?= $size['description'];?></dd>
            <dt>Color Swatch</dt>
            <dd><?= $size->color_swatch[0] . '<br>'. $size->color_swatch[1];?></dd>
            <dt>Color Swatch Image</dt>
            <dd><?= $size->color_swatch[0]['image'] . '<br>'. $size->color_swatch[1]['image'];?></dd>
            <?php  endforeach;?>
          
            </dl>
            </td>

           

           
            <td> <dl><dt>Action</dt>
            <dd>
            <form action='edit.php' method='post'>
            <input name='itemNumber' type='hidden' value="<?= $catalogItem->item_number;?>">
            <button type='submit' class='btn'>Edit</button>
            </form>
            <form action='destroy.php' method='post'>
            <input name='itemNumber' type='hidden'value="<?= $catalogItem->item_number;?>">
            <button type='submit' class='btn'>Delete</button>
            </form>
            <form action='show.php' method='post'>
            <input name='itemNumber' type='hidden'value="<?= $catalogItem->item_number;?>">
            <button type='submit' class='btn'>show</button>
            </form>
            
            </dd>
            </dl>
            </td>
           
    </tr>
</table>
            <?php   endforeach;?>
           <?php break;  endforeach; ?>
       
<?php
}
?>
        </table>
        <a href="create.php" class="ml-4 btn btn-light">Add</a>
       
        
</body>
</html>