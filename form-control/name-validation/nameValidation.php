<?php

if(strtoupper($_SERVER['REQUEST_METHOD'])=='POST'){
    $userName=null;
     if(array_key_exists('userName',$_POST)){
       
         $userName=$_POST['userName'];
     }
     $errors= errorValidaton($userName);
     if(count($errors)>0){
     displayError($errors);
     echo $userName;
     return true;
     }
     echo $userName;
 
 }
function checkFirstLetter($userName){
    $firstLetter=preg_match('/^[\w]+[\w. ]*/', $userName);
    if($firstLetter){
        return true;
    }
    return false;
}

 function checkNumber($userName){
    $number=preg_match('@[0-9]@', $userName);
    if($number){
        return true;
    }
    return false;
}

function checkSpecialCharacter($userName){
    $specialCharacter=preg_match('@[^\w]@', $userName);
    if($specialCharacter){
        return true;
    }
    return false;
}


function errorValidaton($userName){
    $errors=[];
    if(checkNumber($userName)){
        $errors[]="name must be number free (0-9)!";
    }
    if(checkSpecialCharacter($userName)){
        $errors[]="name must be special character free (@!#)!";
    }
    if(!checkFirstLetter($userName)){
        $errors[]="name start with a letter .";
    }
    return $errors;
}

function displayError($errors){
    echo "<ul>";
    foreach($errors as $error){ 
        echo "<li><b>$error</b></li>"; 
    }
    echo "</ul>";
}