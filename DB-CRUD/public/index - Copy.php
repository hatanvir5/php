<?php
require 'db.php';
$sql='SELECT * FROM students';
$statement=$connection->prepare($sql);
$statement->execute();
$departments=$statement->fetchAll(PDO::FETCH_OBJ);

?>

<!doctype html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>Department</title>
  </head>
  <body class="bg-dark container">

  <table border="3" class="table table-striped table-dark mt-5 ">
  <thead>
    <tr>
     
      <th scope="col">ID</th>
      <th scope="col">Name</th>
      <th scope="col">Phone</th>
      <th scope="col">Email</th>
      <th scope="col">Multiselect</th>
      <th scope="col">Department</th>
      <th scope="col">Birthday</th>
      <th scope="col">Gender</th>
      <th scope="col">Description</th>
      <th scope="col">Files</th>
      <th scope="col">File</th>
      <th scope="col">Action</th>

    </tr>
  </thead>
  <tbody>

  <?php foreach($departments as $department):?>
    <tr>
    
      <td><?= $department->id;?></td>
      <td><?= $department->name;?></td>
      <td><?= $department->phone;?></td>
      <td><?= $department->email;?></td>
      <td><?= $department->multiselect;?></td>
      <td><?= $department->department;?></td>
      <td><?= $department->birthday;?></td>
      <td><?= $department->gender;?></td>
      <td><?= $department->description;?></td>
      <td><?= $department->multipleFile;?></td>
      <td><?= $department->file;?></td>
     
      <td>  
      <div class="form-group"> 

      <form method="post" action="view.php" class="inline-block m-0 p-0">
        <input type="hidden" name="id" value="<?=$department->id?>"> 
        <button type="submit" class="btn btn-success mb-1 pr-4">View</button>

        </form>

      <form method="post" action="edit.php" class="inline-block m-0 p-0">
        <input type="hidden" name="id" value="<?=$department->id?>"> 
        <button type="submit" class="btn btn-success mb-1 pr-4">Edit</button>

        </form>


      <form method="post" action="delete.php" class="inline-block m-0 p-0 width-100px">
        <input type="hidden" name="id" value="<?=$department->id?>"> 
        <button type="submit" class="btn btn-danger">Delete</button>



        <div>
      </form>



      </td>
   
    </tr>
<?php endforeach;?>

  </tbody>


</table>


<a class="btn btn-primary" href="create.php">Add</a>







    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </body>
</html>