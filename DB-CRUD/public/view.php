<?php
require 'db.php';

$data=$_POST;
if(array_key_exists('id',$data)){
    $id=$data['id'];
}
$sql='SELECT * FROM students    WHERE id=:id';
$statement=$connection->prepare($sql);
$statement->execute([':id'=>$id]);
$department=$statement->fetch(PDO::FETCH_OBJ);


require 'header.php';


?>
<div class="container width-300px bg-dark text-light mt-5 p-5"> 
    <h4 class="ml-5">Student Info</h4>
    <ul>
        <li><b>Name:</b><?= $department->name;?></li>
        <li><b>Email:</b><?= $department->email;?></li>
        <li><b>Phone:</b><?= $department->phone;?></li>
        <li><b>Department:</b><?= $department->department;?></li>
        <li><b>Gender:</b><?= $department->gender;?></li>
        <li><b>Birthday:</b><?= $department->birthday;?></li>
        <li><b>File:</b><?= $department->file;?></li>
        <li><b>Multiple File:</b><?= $department->multipleFile;?></li>
        <li><b>Multi select:</b><?= $department->multiselect;?></li>
        <li><b>Description</b><?= $department->description;?></li>
        <p class="mt-5"><a class="btn btn-primary" href="index.php">Go to homepage</a></p>
        
    </ul>
</div>
<?php
require 'footer.php';

?>