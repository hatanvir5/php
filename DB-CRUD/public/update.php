<?php
require 'db.php';

$data=$_POST;


if(array_key_exists('id',$data)){
    $id=$data['id'];
}

if(array_key_exists('name',$data)){
    $name=$data['name'];
}

if(array_key_exists('email',$data)){
    $email=$data['email'];
}

if(array_key_exists('phone',$data)){
    $phone=$data['phone'];
}
if(array_key_exists('gender',$data)){
    $gender=$data['gender'];
}
if(array_key_exists('birthday',$data)){
    $birthday=$data['birthday'];
}
if(array_key_exists('department',$data)){
    $department=$data['department'];
}
if(array_key_exists('description',$data)){
    $description=$data['description'];
}
if(array_key_exists('file',$data)){
    $file=$data['file'];
}
if(array_key_exists('multipleFile',$data)){
    $mulitpleFile=$data['mulitpleFile']; 
$mulitpleFiledata =implode(',',$mulitpleFile);
}
if(array_key_exists('multiselect',$data)){
    $mulitipleSelect=$data['mulitipleSelect'];
    $arraydata = implode(',',$mulitipleSelect);
}
$message='';
// name,email,phone,gender,birthday,,description,file,multipleFile,multiselect
$sql='UPDATE students set
 name=:name ,
 email=:email ,
 phone=:phone ,
 gender=:gender ,
 birthday=:birthday ,
 department=:department ,
 description=:description ,
 file=:file,
 multipleFile=:multipleFile,
 multiselect=:multiselect 
 WHERE id=:id ';
$statement=$connection->prepare($sql);
if($statement->execute([

 ':id'=>$id,
':name'=>$name,
':email'=>$email,
':phone'=>$phone,
':birthday'=>$birthday,
':department'=>$department,
':gender'=>$gender,

':multipleFile'=>$mulitpleFiledata,
':multiselect'=>$arraydata,
':description'=>$description,
':file'=>$file
    
    ])){
    echo 'data updated successfully';
}

header("Location:index.php");
 
