<?php
require 'db.php';

$data=$_POST;
if(array_key_exists('id',$data)){
    $id=$data['id'];
}
$sql='SELECT * FROM students    WHERE id=:id';
$statement=$connection->prepare($sql);
$statement->execute([':id'=>$id]);
$department=$statement->fetch(PDO::FETCH_OBJ);




?>

<?php
require 'header.php';
?>

<div class="container bg-light">
  <h3 class="mt-3 text-dark">Check Html All input types</h3>

  <form method="post" action="update.php"> 
    <div class="form-group">
    <label> Name:</label>
    <input class="form-control" type="text" name="name" value="<?= $department->name;?>">
    </div>

    <div class="form-group">
    <label>Email:</label>
    <input class="form-control" type="email" name="email" value="<?= $department->email;?>">
    </div>
  
    <div class="form-group">
    <label>Phone:</label>
    <input class="form-control" type="tel" name="phone" value="<?= $department->phone;?>">
    </div>

    <div class="form-group">
    <label>Password:</label><br>
    <input class="form-control" type="password"  name="password" value="<?= $department->password;?>">
    </div>

    <div class="form-group">
    <b>       Gender         </b><br>
    <input type="radio" id="male" name="gender"  value="<?= $department->gender;?>" checked>
    <label for="male"><?= $department->gender;?></label><br>

    <input type="radio" id="male" name="gender"  value="male">
      <label for="male">Male</label><br>
      <input type="radio" id="female" name="gender" value="female">
      <label for="female">Female</label><br>
      <input type="radio" id="other" name="gender"   value="other">
      <label for="other">Other</label>
    </div>

    <div class="form-group form-check">
      <b>Allow Department</b><br>
    <input type="checkbox" name="department" value="<?= $department->department;?>"  checked>
    <label ><?= $department->department;?></label><br>

    <input type="checkbox" name="department" value="CSE">
    <label >CSE</label><br>
     
      <input type="checkbox" name="department" value="EEE">
      <label >EEE</label><br>
      <input type="checkbox" name="department" value="ETE">
      <label >ETE</label><br>
    </div>





    <div>
    <label for="birthday">Birthday:</label>
    <input class="form-control" type="date" name="birthday" value="<?= $department->birthday;?>" >
    </div>



  <div class="form-group">
    <label for="file">Select a file:</label>
    <input class="form-control-file" type="file" id="file" name="file" value="<?= $department->file;?>"  >
    </div>

 




    <div class="form-group">
    <label for="files">Select Multiple files:</label>
      <input class="form-control-file" type="file" id="files"  value="<?= $department->multipleFile;?>"    name="mulitpleFile[]" multiple>
    </div> 
  
 <div class="form-group">
    <label for="exampleFormControlSelect2">Example multiple select</label>
    <select multiple class="form-control" name="mulitipleSelect[]" id="exampleFormControlSelect2">
      <option><?= $department->multiselect;?></option>
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div>

  <div  class="form-group">
    <label for="">Description</label>
    <textarea class="form-control" name="description" id="" cols="5" rows="3"><?= $department->description;?></textarea>
  </div>
  <div>
      <input type="hidden" name="id" value="<?=$department->id;?>">
  </div>
        
      <button class="btn btn-success mb-5" type="submit">Submit</button>

</form>
</div>
<?php
require 'footer.php';
?>

