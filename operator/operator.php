<?php

$a=5;
$b=3;

echo $a**$b."<br>"; //here b is the power of a


echo $a%$b;

echo (5 % 3)."<br>";           // prints 2
echo (5 % -3)."<br>";          // prints 2
echo (-5 % 3)."<br>";          // prints -2
echo (-5 % -3)."<br>";         // prints -2

$a=14;
echo $a &1 ; //using and
echo "<br>";
echo  $a % 3; //using modulus

$v=10;
$m=5;


// Inline: $v is value to be divided, $m is the modulus
$remainder = ( $v % $m + $m ) % $m;

// Or as a function:
function modulo( $value, $modulus ){
  echo ( $value % $modulus + $modulus ) % $modulus;
}
modulo(10,3);
// Test:
for( $x = -6; $x <= 6; ++$x )  echo $x, "\t", modulo( $x, 3 ), "\t<br>\n";
/* prints:
-6  0
-5  1
-4  2
-3  0
-2  1
-1  2
0  0
1  1
2  2
3  0
*/ 

echo "<br>";
echo "<br>";


?>
