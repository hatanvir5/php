<?php
include_once("../vendor/autoload.php");
// include_once("../src/security.php");

use Tanvir\Utility;
use Tanvir\Debugger;
use Tanvir\Sanitizer;
use Tanvir\Validator;

$debugger=new Debugger;
$storedData=[];
if(array_key_exists('feedbackData',$_COOKIE)){
  $storedData=unserialize($_COOKIE['feedbackData']);
}



?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Document</title>
</head>
<body class="bg-info">
    <h2 class="p-5  text-light text-center">Participant List</h2><br>


    

<table class="table">
          <tr>
       
          <th>Name</th>
          <th>Email</th>
          <th>Age</th>
          <th>Mobile</th>
          <th>Address</th>
          <th>About</th>
          <th>Action</th>
       
          </tr>
<?php 
if(count($storedData)<=0){



?>

    <tr><td colspan='8' style="text-align:center;"> No Records Found</td></tr>

<?php
}
else{
?>
          <?php
      
          
         foreach($storedData as $key=> $singlePerson){
           

          $name=$singlePerson['name'];
      

          $email=$singlePerson['email'];

          $age=$singlePerson['age'];
          $mobile=$singlePerson['mobile'];
          $address=$singlePerson['address'];
          $about=$singlePerson['about'];
          
          echo 
           "<tr>
            
            <td>$name</td>
            <td>$email</td>
            <td>$age</td>
            <td>$mobile</td>
            <td><address>$address</address></td>
            <td>$about</td>
            <td> 
            <form action='editInfo.php' method='post'>
            <input name='position' type='hidden' value=$key>
            <button type='submit' class='btn'>Edit</button>
            </form>
            <form action='deleteInfo.php' method='post'>
            <input name='position' type='hidden' value=$key>
            <button type='submit' class='btn'>Delete</button>
            </form>
      
            </td>
           
            </tr>";

         }



}
?>

          
        </table>
        <a href="index.php" class="ml-4 btn btn-light">Add</a>
       
        
</body>
</html>