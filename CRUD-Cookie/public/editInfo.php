<?php
$Position=$_POST['position'];
// echo "<pre>";
// print_r($Position);
// echo "</pre>";


if(array_key_exists('feedbackData',$_COOKIE)){
    $storedData=unserialize($_COOKIE['feedbackData']);
  }

  $singleInfo=$storedData[$Position];





?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Edit Information</title>
</head>
<body>
    <div class="container">

    <h1>Update Information:</h1>
    
    <form action="editProcessor.php" method="post">

 
    
    <label for="">Name</label>
    <input type="text" name="name" value="<?php echo $singleInfo['name'];?>" class="form-control">
    <label for="">Email</label>
    <input type="email" name="email" value="<?php echo $singleInfo['email'];?>" class="form-control">
    

    <label for="">Age</label>
    <input type="text" name="age"  value="<?php echo $singleInfo['age'];?>" class="form-control">

    <label for="">Address</label>
    <input type="type" name="address" value="<?php echo $singleInfo['address'];?>"  class="form-control">

    <label for="">Mobile</label>
    <input type="text" name="mobile"  value="<?php echo $singleInfo['mobile'];?>" class="form-control">

    <label for="">About Yourself</label>  
    <textarea name="about" cols="30" rows="5" class="form-control"><?php echo $singleInfo['about'];?></textarea>

   <input type="hidden" name="position"  value="<?php echo $Position;?>" class="form-control">
    <button type="submit"  class="btn btn-primary">Update</button>
    </form>
    </div>
</body>
</html>
