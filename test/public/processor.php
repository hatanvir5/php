<?php

include_once('../vendor/autoload.php');

use Oishee\Guestbook\Guestbook;
use Oishee\Utility\Debugger;
use Oishee\Utility\Sanitizer;
use Oishee\Utility\Validator;
use Oishee\Utility\Utility;


if(!Utility::isPosted()){
    header("location:../index.php");
}
$sanitizedData = Sanitizer::sanitize($_POST);
$validatedData = Validator::validate($sanitizedData);
$strValidatedData = serialize($validatedData);
//Debugger::debug($validatedData);

$result = setcookie('guestbook_data',$strValidatedData, time() + (86400 * 30), "/");
if($result){
    echo 'Data has been saved successfully';
}else{
    echo 'Data has not saved successfully';
}
?>