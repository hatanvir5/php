<?php
ini_set('display_errors','On');
include_once('../vendor/autoload.php');

use Oishee\Guestbook\Guestbook;
use Oishee\Utility\Debugger;

$storedData = [];
if(array_key_exists('guestbook_data', $_COOKIE)){
    $strStoredData = $_COOKIE['guestbook_data'];
    $storedData = unserialize($strStoredData);
    //Debugger::debug($storedData);

}
$guestbook = new Guestbook($storedData);

?>

<!DOCTYPE html>
<html lang = 'en'>

<head>
<meta charset = 'UTF-8'>
<meta name = 'viewport' content = 'width=device-width, initial-scale=1.0'>
<title>Guest Book</title>
</head>

<body>
<h1>Guest Book</h1>
<dl>
<dt>Name :</dt>
<dd>
 <?= !empty($guestbook->name)? $guestbook->name : 'Not Provided'; ?>
</dd>
<dt> Comment : </dt>
<dd>
<?= !empty($guestbook->comment)? $guestbook->comment : 'Not Provided'; ?>
</dd>
</dl>
<a href="guestbook-index.php">Go to Index</a>

<form action="guestbook-index.php" method="post">

<input type="hidden" name="name1" value="<?php echo $guestbook->name; ?>">
<button type="submit">Save</button>
</body>

</html>