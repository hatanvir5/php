<?php

include_once('../vendor/autoload.php');

use Oishee\guestbook\Guestbook;
use Oishee\utility\Debugger;
use Oishee\utility\Sanitizer;
use Oishee\utility\Validator;
use Oishee\utility\Utility;


if(!Utility::isPosted()){
    //header("location:../index.html");
}
$sanitizedData = Sanitizer::sanitize($_POST);
$validatedData = Validator::validate($sanitizedData);
$guestbook = new Guestbook($validatedData);

Debugger::debug($_POST);

?>

<!DOCTYPE html>
<html lang = 'en'>

<head>
<meta charset = 'UTF-8'>
<meta name = 'viewport' content = 'width=device-width, initial-scale=1.0'>
<title>Guest Book</title>
</head>

<body>
<h1>Guest Book</h1>
<dl>
<dt>Name :</dt>
<dd>
 <?= !empty($guestbook->name)? $guestbook->name : 'Not Provided'; ?>
</dd>
<dt> Comment : </dt>
<dd>
<?= !empty($guestbook->comment)? $guestbook->comment : 'Not Provided'; ?>
</dd>
</dl>
<a href="guestbook-list.php">Go to Guest List</a>
</body>

</html>