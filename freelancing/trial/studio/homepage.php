<?php

$connection=mysqli_connect("localhost","root","","studio");
session_start();

if(!isset($_SESSION['email'])){
    header('Location:login.php');

    exit();
}

    $userEmail=$_SESSION['email'];

    $dataQuery=mysqli_query($connection,"SELECT * FROM registrations WHERE email='$userEmail'");
    $dataShow=mysqli_fetch_assoc($dataQuery);
  
    if(isset($_POST['deleteAcc'])){
        $deleteQuery=mysqli_query($connection,"DELETE FROM registrations WHERE email='$userEmail'");
        if($deleteQuery){
            header('Location:logout.php');
        }
    }

    if(isset($_POST['update'])){

        $updateName=$_POST['updateName'];
        $updatePhone=$_POST['updatePhone'];


        $updateQuery=mysqli_query($connection," UPDATE registrations SET name='$updateName',phone='$updatePhone' WHERE email='$userEmail' limit 1");
        if($updateQuery){
            header('Location:homepage.php');
        }

    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
     integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Homepage</title>

</head>
<body class="bg-info">
<div class="row   bg-info m-5">
<div class="col-3"></div>
<div class="col-6 m-5">


<div class="container" style="height: 250px;">
<h1 class="text-center text-warning mb-5 mt-5">Admin Panel</h1>
    <a class="btn btn-dark" href="image-upload/index.php">Upload a post</a>

    <button type="button" class="btn btn-dark" data-toggle="collapse" data-target="#profile">Simple collapsible</button>
  <div id="profile" class="collapse">



<ul>
    <li>ID:<?= $dataShow['id'];?></li>
    <li>Name:<?=$dataShow['name'];?></li>
    <li>Email:<?=$dataShow['email'];?></li>
    <li>Phone:<?=$dataShow['phone'];?></li>
    <li>Password:<?=$dataShow['password'];?></li>
  

</ul>

</div>
    

      <button type="button" class="btn btn-dark" data-toggle="collapse" data-target="#demo">Update</button>
      <div id="demo" class="collapse">
   <form action="" method="POST" class="col-6">
   <div class="form-group">
   <input type="text"  name="updateName" value=" <?= $dataShow['name'];?> " class="form-control">
   </div>
   <div class="form-group">
   <input type="tel" name="updatePhone" value=" <?= $dataShow['phone'];?> " class="form-control" >
   </div>

   <input type="submit"  value="Save" class="btn btn-success mb-3" name="update">
   </form>
  </div>
      <a href="logout.php" class="btn btn-dark">Logout</a>

    <form method="POST">
    <input type="submit" name="deleteAcc" value="Delete account" class="btn btn-danger mt-2 mb-3">
    </form>
      


   
  </tbody>
</table>
 




  



  </div>
</div>

<div class="col-3"></div>
</div>


  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>



