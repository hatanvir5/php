


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
      crossorigin="anonymous"
    />
  </head>

<body>
  <div class="container">
  <div class="row">
  <div class="col-8">
        <h2 class="text-success">Registration Form</h2>
      <h3 class="text-danger">
       <?php
      if(isset($error)){
        echo $error;
      }
      else if(isset($passwordError) || isset($emailError)){
        if(isset($emailError)){
          echo $emailError;
        }
        else{
          echo $passwordError;
        }
        
      }
      
   
       ?>
      </h3>

      <h3 class="text-success">
          <?php
          if(isset($success)){
            echo   $success;
          
          }
         
          ?>
      </h3>

        <form action="" method="POST">
          <div class="form-group">
            <label for="userFullName">Name:</label>
            <input
              type="text"
              placeholder="Enter Your Full Name"
          
              name="userFullName"
              class="form-control"
            />
          </div>

          <div class="form-group">
            <label for="userEmail">Email:</label>
            <input
              type="email"
              placeholder="Enter Your Email"
     
              name="userEmail"
              class="form-control"
            />
          </div>
          <div class="form-group">
            <label for="userPassword">Password:</label>
            <input
              type="password"
              placeholder="Enter Your password"
       
              name="userPassword"
              class="form-control"
            />
          </div>
          <div class="form-group">
            <label for="userPhone">Phone:</label>
            <input
              type="tel"
              placeholder="Enter Your Phone Number"
            
              name="userPhone"
              class="form-control"
            />
          </div>

          <div class="form-group">
            <p>select your gender</p>
          
              <input type="radio" name="gender" value="male" />  <label for=""> Male
            </label>
         
              <input type="radio" name="gender" value="female" />   <label for=""> Female
            </label>
          </div>

     
            <input
              type="submit"
              value="Sign Up"
              name="registration"
              class="btn btn-success btn-block btn-lg"
            />
        
         
        </form>

          <br>

<!--           
<h3>Data </h3>
        <table class="table">
          <tr>
          <th>serial</th>
          <th>Name</th>
          <th>Email</th>
          <th>Password</th>
          <th>phone</th>
          <th>Gender</th>
          </tr>
          <?php
          
          // foreach($show as $single){
          //   $id=$single['serial'];
          //   $name=$single['name'];
          //   $email=$single['email'];
          //   $password=$single['password'];
          //   $phone=$single['phone'];
          //   $gender=$single['gender'];
          // echo 
          //  "<tr>
          // <td>$id</td>
          //   <td>$name</td>
          //   <td>$email</td>
          //   <td>$password</td>
          //   <td>$phone</td>
          //   <td>$gender</td>
          //   </tr>";
        
          // }
          ?>

        </table> -->

      </div>
      <div class="col-4">
      <h3 class="text-primary">
      Login
      </h3>

      <h3 class="text-danger">
        <?php
        if(isset($notMatch)){
          echo $notMatch;
        }
        ?>
      </h3>
      <form action="" method="POST">
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="email" class="form-control" id="exampleInputEmail1" name="login_email" aria-describedby="emailHelp">
              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" name="login_password" class="form-control" >
            </div>
           
            <input type="submit" class="btn btn-success btn-block btn-lg" value="submit" name="login">
          </form>
      </div>
  </div>





</div>

  </body>
</html>
