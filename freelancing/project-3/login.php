<?php  
 session_start();  
 $host = "localhost";  
 $username = "root";  
 $password = "";  
 $database = "ibetcom1_database";  
 $message = "";  

 try  
 {  
      $connect = new PDO("mysql:host=$host; dbname=$database", $username, $password);  
      $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  
      if(isset($_POST["login"]))  
      {  
           if(empty($_POST["userName"]) || empty($_POST["password"]))  
           {  
               echo '<label>All fields are required</label>';  
           }  
           else  
           {  
                $query = "SELECT * FROM registrations WHERE userName = :userName AND password = :password";  
                $statement = $connect->prepare($query);  
                $statement->execute(  
                     array(  
                          'userName'     =>     $_POST["userName"],  
                          'password'     =>     $_POST["password"]  
                     )  
                );  
                $count = $statement->rowCount();  
                if($count > 0)  
                {  
                     $_SESSION["userName"] = $_POST["userName"];  
                     header("location:loginSuccess.php");  
                }  
                else  
                {  
                     echo '<label>Wrong Data</label>';  
                     header("location:index.php");  

                }  
           }  
      }  
 }  
 catch(PDOException $error)  
 {  
     echo $error->getMessage();  
     header("location:index.php");  

 }  
 ?>  