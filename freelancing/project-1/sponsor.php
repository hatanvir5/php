<!DOCTYPE html>
<html lang="en"><head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" id="main-styles-link">
  <link rel="stylesheet" href="css/font-awesome.css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/sidenavbar.css" rel="stylesheet">
  <link href="css/my-style.css" rel="stylesheet">
  <link href="css/responsive.css" rel="stylesheet">
  <link href="css/footer.css" rel="stylesheet">
  <link rel="stylesheet" href="css/zebra_dialog.css" type="text/css">

  <link rel="stylesheet" href="css/custom.css">
  <link rel="favourive icon" href="https://ibet39.com/favicon.png" type="x-icon/png">
  <title>ibet39</title>
</head>
<body>
<style>
 body , #container_buttons {
   background-color: 02474a  !important;
   background-color: #0a2940;
}
.modal-body, .modal-footer {
  background-color: unset !important;
}
.tabMenu li a:hover, .tabMenu li a.active {
    background-color: #0B9B89 !important;
    border: 2px solid #212d5a !important;
    color: #FFF !important;
}
.heading-color {
  background: 005a4c !important;
}
.mh-color {
  background: 3c8cb3 !important;
  background: #027B5B;
}
.panel-heading{
background:005a4c !important;
}
.second-lebal {
    background:166173 !important;
}
.panel-body{
    background: #7E8A94 !important;
}
.buttonrate {
    background: 3c8cb3 !important;
    background: #3a3a3a;
    border:1px solid #7E8A94 !important;
}
.ft-item a.ftmenu {
    background: 02474a  !important;
}
.live-up{
    background: 079dac  !important;
}
.a_demo_four, .b_demo_four{
    background: 0a5b79 !important;
    cursor: pointer;
}
.myButton {;
    background: 3c8cb3 !important;
}
.footer-basic-centered {
    background: 02474a !important ;
    border-top: 1px solid 02474a  !important;
}
.betModalWrap li{
    list-style: none;
}
</style> <div class="top-navbar-wrapper">
  <!--Start Top Navbar-->
  <nav class="navbar navbar-inverse  navbar-fixed-top">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" onclick="openNav()">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">
          <img src="images/logo.png">
        </a>
        <a class="navbar-live" href="live-sports.php" >
          <img src="images/tv.png" style="position: absolute;top: 15px;width: 40px;height: 40px;right: 15px;">
        </a>
        <!-- Site Clock -->
        <p class="text-center siteClock" id="siteClock#w"></p>
        <!-- Site Clock -->
      </div>
              <ul class="nav navbar-nav navbar-right navbar-right-two">
          <li class="join-button"><a class="btn btn-primary btn-sm" href="registration.php" style="text-align: center;">Registration</a></li>
          <li>
            <form method="POST" action="login.php" accept-charset="UTF-8" class="navbar-form navbar-left"><input name="_token" type="hidden">
              <div class="form-group">
                <input type="text" name="user_name" class="form-control" placeholder="user name" required="">
              </div>
              <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password" required="">
              </div>

              <div class="mobile-sign-in-wrapper">
                <button type="submit" class="btn btn-primary login-button login-button-mobile" name="form">Login</button>
                <a href="registration.php" class="btn btn-primary login-button login-button-mobile">Registration</a>
              </div>

            </form>

          </li>

        </ul>
      


      <!-- Responsive Sidebar -->
      <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
        <a href="index.php">Home</a>
        <hr>
              </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="nav-top-links">
        <ul class="nav navbar-nav header-links">
          <li class="active"><a href="#">Sports <span class="sr-only">(current)</span></a></li>
        </ul>
                  <ul class="nav navbar-nav navbar-right navbar-right-one">
            <li class="join-button"><a class="btn btn-primary btn-sm" href="registration.php" style="text-align: center;">Registration</a></li>
            <li>
              <form method="POST" action="login.php" accept-charset="UTF-8" class="navbar-form navbar-left"><input name="_token" type="hidden" value="Hnoe3zB5Ux7RLtkTP61lvoYb57cvSe6dT6dy5x6c">
                <div class="form-group">
                  <input type="text" name="user_name" class="form-control" placeholder="user name" required="">
                </div>
                <div class="form-group">
                  <input type="password" name="password" class="form-control" placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary login-button" name="form">Login</button>
              </form>

            </li>
          </ul>
        
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  <!--End Top Navbar-->
</div><div class="nav-side-menu">
  <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
  <div class="menu-list">
    <ul id="menu-content" class="menu-content collapse out">
      <li><a href="index.php" style="display: block;"><i class="fa fa-home fa-lg"></i> Home </a></li>
      <li><a href="live-sports.php" style="display: block;"><i class="fa fa-home fa-lg"></i> Roulette Live </a></li>
       </ul>
</div>
</div>

<!--End Side Navbar-->

<div class="col-md-10  col-md-offset-2 col-sm-offset-3 col-sm-9 col-lg-10 col-lg-offset-2  main-content-size">
  <div class="row main-content-wrapper">
    <div class="col-md-8 col-lg-9">
      <marquee style="background: 005944 !important;" class="mrq" scrollamount="4" direction="scroll">
        <p style="margin: 2px 0;"><strong>👉&nbsp; All Match Will be live...Withdraw Request Accept Just 15 Minute...Any Helps Contuct Your Club Holder 👈</strong> </p>
      </marquee>
            <div class="site_promoting_ad_panel">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>

            </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      
        <div class="item active">
          <img src="./images/slider/slide-20200821100704.jpg">
        </div>

        <!-- <div class="item">
          <img src="images/slider_2.jpg">
        </div>
        <div class="item">
          <img src="images/join_now_banner.jpg">
        </div> -->
            </div>
  </div>
</div>

      <div class="owl-carousel itemSlider nav nav-tabs animated zoomIn ">
	<div class="SliderItem" id="allgame">
		<div class="sliderImg">
			<a href="#" data-toggle="tab">
				<img src="images/select-all.png" style="">
				<span><span class="all-count"></span></span>
			</a>
		</div>
	</div>
	<div class="SliderItem" id="football">
		<div class="sliderImg">
			<a href="#" data-toggle="tab">
				<img src="images/football.png" style="">
				<span><span class="all-count"></span></span>
			</a>
		</div>
	</div>
	<div class="SliderItem">
		<div class="sliderImg" id="cricket">
			<a href="#" data-toggle="tab">
				<img src="images/cricket.png" style="">
				<span><span class="cricket-count"></span></span>
			</a>
		</div>
	</div>
	<div class="SliderItem">
		<div class="sliderImg">
			<a href="#Basketball" data-toggle="tab" id="basketball">
				<img src="images/basketball.png" style="">
				<span><span class="basketball-count"></span></span>
			</a>
		</div>
	</div>
	<div class="SliderItem">
		<div class="sliderImg">
			<a href="#Badminton" data-toggle="tab" id="badminton">
				<img src="images/badminton.png" style="">
				<span><span class="bmin-count"></span></span>
			</a>
		</div>
	</div>
	<div class="SliderItem">
		<div class="sliderImg">
			<a href="#Tennis" data-toggle="tab" id="tennis">
				<img src="images/tennis.png" style="">
				<span><span class="tnis-count"></span></span>
			</a>
		</div>
	</div>

	<div class="SliderItem">
		<div class="sliderImg">
			<a href="#TableTennis" data-toggle="tab" id="TableTennis">
				<img src="images/table tennis.png" style="">
				<span><span class="hockey-count"></span></span>
			</a>
		</div>
	</div>
</div>
      		<div class=" panel2 TableTennis" role="tab" id="TableTennis">
			<div class="panel-heading first-lebal "  style="cursor: pointer;">
				<div  class="colupsPanelHeader panel-title" data-toggle="collapse" href="#one2928" aria-expanded="true" aria-controls="#one2928">
					<div class="colupsPanelHeaderImg">
													<img src="./images/table tennis.png" width="30px;">&nbsp;
												</div>
					<div class="colupsPanelHeaderContent">
						<h5>
							lvashkin A <img src="./images/vs.png"> Yurchenko K						</h5>
						<p>
							Table Tennis Setka Cup<span> <img src="./images/calander.png"> 23-08-2020 <img src="./images/clock.png">  12-00-PM</span><br>
							 <span style="color:#fff ;padding: 0; ">Live Score :-- [  0 : 0  ]</span>
						</p>
					</div>                                                   
				</div>
			</div>
			<div id="one2928" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
				<div class="panel-body" style="padding: 0px;">
					<div class=" panel2">
														<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29281" aria-expanded="true" aria-controls="#two29281">
									To Win The Match<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29281" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														lvashkin A 													</span>
													2.50												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Yurchenko K													</span>
													1.35												</div>
											</div>
											 
								</div>
							</div>
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29282" aria-expanded="true" aria-controls="#two29282">
									1st Set Win<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29282" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														lvashkin A 													</span>
													2.25												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Yurchenko K													</span>
													1.37												</div>
											</div>
											 
								</div>
							</div>
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29283" aria-expanded="true" aria-controls="#two29283">
									Full Time Total Point<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29283" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Odd													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Even													</span>
													1.85												</div>
											</div>
											 
								</div>
							</div>
							
												</div>
				</div>
			</div>
		</div>
			<div class=" panel2 Football" role="tab" id="Football">
			<div class="panel-heading first-lebal "  style="cursor: pointer;">
				<div  class="colupsPanelHeader panel-title" data-toggle="collapse" href="#one2929" aria-expanded="true" aria-controls="#one2929">
					<div class="colupsPanelHeaderImg">
													<img src="./images/football.png" width="30px;">&nbsp;
												</div>
					<div class="colupsPanelHeaderContent">
						<h5>
							NyanCat FC <img src="./images/vs.png"> Pele Warriors						</h5>
						<p>
							Football Liga Pro 12 Min Play<span> <img src="./images/calander.png"> 23-08-2020 <img src="./images/clock.png">  12-30-PM</span><br>
							 <span style="color:#fff ;padding: 0; ">Live Score :-- [  0 : 0  ]</span>
						</p>
					</div>                                                   
				</div>
			</div>
			<div id="one2929" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
				<div class="panel-body" style="padding: 0px;">
					<div class=" panel2">
														<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29291" aria-expanded="true" aria-controls="#two29291">
									Half Time Result<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29291" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														NyanCat FC													</span>
													2.30												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Pele Warriors													</span>
													2.20												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Draw													</span>
													1.80												</div>
											</div>
											 
								</div>
							</div>
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29292" aria-expanded="true" aria-controls="#two29292">
									Full Time Result<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29292" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														NyanCat FC													</span>
													2.20												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Pele Warriors													</span>
													2.10												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Draw													</span>
													2.50												</div>
											</div>
											 
								</div>
							</div>
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29293" aria-expanded="true" aria-controls="#two29293">
									Team Of 1st Goaol<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29293" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														NyanCat FC													</span>
													1.80												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Pele Warriors													</span>
													1.80												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29294" aria-expanded="true" aria-controls="#two29294">
									Total Match Goal<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29294" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														1 Goal													</span>
													4.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2 Goal													</span>
													2.50												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														3 Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														4 Goal													</span>
													2.80												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														5 or 5+ Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29295" aria-expanded="true" aria-controls="#two29295">
									Total Match Goal Under/Over<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29295" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2.5 Under													</span>
													2.30												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2.5 Over													</span>
													1.40												</div>
											</div>
											 
								</div>
							</div>
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29296" aria-expanded="true" aria-controls="#two29296">
									Full Time Total Goal<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29296" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Odd													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Even													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
							
												</div>
				</div>
			</div>
		</div>
			<div class=" panel2 Cricket" role="tab" id="Cricket">
			<div class="panel-heading first-lebal "  style="cursor: pointer;">
				<div  class="colupsPanelHeader panel-title" data-toggle="collapse" href="#one2923" aria-expanded="true" aria-controls="#one2923">
					<div class="colupsPanelHeaderImg">
													<img src="./images/cricket.png" width="30px;">&nbsp;
												</div>
					<div class="colupsPanelHeaderContent">
						<h5>
							Baggy Blues CC <img src="./images/vs.png">  Cobra CC						</h5>
						<p>
							ECS Hungary, 2020<span> <img src="./images/calander.png"> 23-08-2020 <img src="./images/clock.png">  01-00-PM</span><br>
							 <span style="color:#fff ;padding: 0; ">--</span>
						</p>
					</div>                                                   
				</div>
			</div>
			<div id="one2923" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
				<div class="panel-body" style="padding: 0px;">
					<div class=" panel2">
														<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29231" aria-expanded="true" aria-controls="#two29231">
									To Win The Toss<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29231" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Baggy Blues CC													</span>
													1.90												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Cobra CC													</span>
													1.90												</div>
											</div>
											 
								</div>
							</div>
							
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29233" aria-expanded="true" aria-controls="#two29233">
									1st ball (1st innigs)<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29233" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Dot ball													</span>
													1.42												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														1 Run													</span>
													2.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2 Runs													</span>
													4.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														3 Runs													</span>
													10.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														4 Runs													</span>
													4.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														6 Runs													</span>
													6.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Wide													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Wicket													</span>
													10.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Ball													</span>
													13.00												</div>
											</div>
											 
								</div>
							</div>
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29234" aria-expanded="true" aria-controls="#two29234">
									1st Over Runs (1st innigs)<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29234" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														0-5													</span>
													2.10												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														6-9													</span>
													1.70												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														10-13													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														14 or 14+													</span>
													3.20												</div>
											</div>
											 
								</div>
							</div>
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29235" aria-expanded="true" aria-controls="#two29235">
									Fall Of 1st Wicket By Run<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29235" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														17.5 Under													</span>
													1.50												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														17.5 Over													</span>
													2.10												</div>
											</div>
											 
								</div>
							</div>
							
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29237" aria-expanded="true" aria-controls="#two29237">
									1st wicket (1st innigs)<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29237" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Cautch Out													</span>
													1.40												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Bowled Out													</span>
													2.70												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														LBW													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Others													</span>
													5.00												</div>
											</div>
											 
								</div>
							</div>
							
							
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two292310" aria-expanded="true" aria-controls="#two292310">
									1st innings total run<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two292310" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Odd													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Even													</span>
													1.85												</div>
											</div>
											 
								</div>
							</div>
							
							
							
							
												</div>
				</div>
			</div>
		</div>
			<div class=" panel2 Football" role="tab" id="Football">
			<div class="panel-heading first-lebal "  style="cursor: pointer;">
				<div  class="colupsPanelHeader panel-title" data-toggle="collapse" href="#one2930" aria-expanded="true" aria-controls="#one2930">
					<div class="colupsPanelHeaderImg">
													<img src="./images/football.png" width="30px;">&nbsp;
												</div>
					<div class="colupsPanelHeaderContent">
						<h5>
							NyanCat FC <img src="./images/vs.png"> Shemyakin Onze						</h5>
						<p>
							Football Liga Pro 12 Min Play<span> <img src="./images/calander.png"> 23-08-2020 <img src="./images/clock.png">  01-20-PM</span><br>
							 <span style="color:#fff ;padding: 0; ">--</span>
						</p>
					</div>                                                   
				</div>
			</div>
			<div id="one2930" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
				<div class="panel-body" style="padding: 0px;">
					<div class=" panel2">
														<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29301" aria-expanded="true" aria-controls="#two29301">
									Half Time Result<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29301" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														NyanCat FC													</span>
													1.65												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Shemyakin Onze													</span>
													2.55												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Draw													</span>
													2.00												</div>
											</div>
											 
								</div>
							</div>
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29302" aria-expanded="true" aria-controls="#two29302">
									Full Time Result<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29302" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														NyanCat FC													</span>
													1.55												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Shemyakin Onze													</span>
													2.50												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Draw													</span>
													3.00												</div>
											</div>
											 
								</div>
							</div>
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29303" aria-expanded="true" aria-controls="#two29303">
									Team Of 1st Goaol<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29303" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														NyanCat FC													</span>
													1.50												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Shemyakin Onze													</span>
													1.90												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29304" aria-expanded="true" aria-controls="#two29304">
									Total Match Goal<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29304" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														1 Goal													</span>
													4.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2 Goal													</span>
													2.50												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														3 Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														4 Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														5 or 5+ Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29305" aria-expanded="true" aria-controls="#two29305">
									Total Match Goal Under/Over<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29305" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2.5 Under													</span>
													2.50												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2.5 Over													</span>
													1.30												</div>
											</div>
											 
								</div>
							</div>
							
															<div class="panel-heading second-lebal" style="">
								<h4 style="cursor: pointer;" class="panel-title" role="button" data-toggle="collapse"  href="#two29306" aria-expanded="true" aria-controls="#two29306">
									Full Time Total Goal<span style=""> <img src="./images/live.gif" class="live-gif"> </span>
								</h4>
							</div>
							<div id="two29306" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Odd													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Even													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
							
												</div>
				</div>
			</div>
		</div>
	    </div>
    <div class="col-md-4 col-lg-3 " >
      <div class="upcoming-matches rightside-content-wrapper" style="margin-bottom: 5px;">

        <div class="news_update_panel common_block_panel">
          <h2>Upcomming Matches</h2>
          <div class="news_container_block" id="scrollable">
            		<div class=" panel2 Football" role="tab">
			<div class="panel-heading first-lebal "  style="cursor: pointer;">
				<div  class="colupsPanelHeader panel-title" data-toggle="collapse" href="#one2931" aria-expanded="true" aria-controls="#one2931">
					<div class="">
						<div class="published_date clearfix">
							<strong>20</strong> <span>-2</span>
						</div>
						<div class="extra_content_wrap">
							<a href="#">
								<h5>Football Liga Pro 12 Min Play</h5>
								<p>Shemyakin Onze vs Ezid11 Esports...{23-08-2020}|</p>
								<p>Time - 02-10-PM</p>
							</a>
						</div>
					</div>                                                  
				</div>
			</div>
			<div id="one2931" class="panel-collapse collapse" role="tabpanel" aria-labelledby="">
				<div class="panel-body" style="padding: 0px;">
					<div class=" panel2">
													<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29311" aria-expanded="true" aria-controls="#two29311">
									Half Time Result								</h4>
							</div>
							<div id="two29311" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Shemyakin Onze													</span>
													2.40												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Ezid11 Esports													</span>
													1.90												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Draw													</span>
													1.80												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29312" aria-expanded="true" aria-controls="#two29312">
									Full Time Result								</h4>
							</div>
							<div id="two29312" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Shemyakin Onze													</span>
													2.30												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Ezid11 Esports													</span>
													1.80												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Draw													</span>
													2.50												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29313" aria-expanded="true" aria-controls="#two29313">
									Team Of 1st Goaol								</h4>
							</div>
							<div id="two29313" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Shemyakin Onze													</span>
													1.90												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Ezid11 Esports													</span>
													1.60												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29314" aria-expanded="true" aria-controls="#two29314">
									Total Match Goal								</h4>
							</div>
							<div id="two29314" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														1 Goal													</span>
													4.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2 Goal													</span>
													2.50												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														3 Goal													</span>
													2.50												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														4 Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														5 or 5+ Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29315" aria-expanded="true" aria-controls="#two29315">
									Total Match Goal Under/Over								</h4>
							</div>
							<div id="two29315" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2.5 Under													</span>
													2.50												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2.5 Over													</span>
													1.30												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29316" aria-expanded="true" aria-controls="#two29316">
									Full Time Total Goal								</h4>
							</div>
							<div id="two29316" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Odd													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Even													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
												</div>
				</div>
			</div>
		</div>
			<div class=" panel2 Football" role="tab">
			<div class="panel-heading first-lebal "  style="cursor: pointer;">
				<div  class="colupsPanelHeader panel-title" data-toggle="collapse" href="#one2932" aria-expanded="true" aria-controls="#one2932">
					<div class="">
						<div class="published_date clearfix">
							<strong>20</strong> <span>-2</span>
						</div>
						<div class="extra_content_wrap">
							<a href="#">
								<h5>Football Liga Pro 12 Min Play</h5>
								<p>Dages CF vs NyanCat FC...{23-08-2020}|</p>
								<p>Time - 02-35-PM</p>
							</a>
						</div>
					</div>                                                  
				</div>
			</div>
			<div id="one2932" class="panel-collapse collapse" role="tabpanel" aria-labelledby="">
				<div class="panel-body" style="padding: 0px;">
					<div class=" panel2">
													<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29321" aria-expanded="true" aria-controls="#two29321">
									Half Time Result								</h4>
							</div>
							<div id="two29321" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Dages CF													</span>
													2.35												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														NyanCat FC													</span>
													2.10												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Draw													</span>
													1.80												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29322" aria-expanded="true" aria-controls="#two29322">
									Full Time Result								</h4>
							</div>
							<div id="two29322" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Dages CF													</span>
													1.30												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														NyanCat FC													</span>
													1.90												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Draw													</span>
													2.50												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29323" aria-expanded="true" aria-controls="#two29323">
									Team Of 1st Goaol								</h4>
							</div>
							<div id="two29323" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Dages CF													</span>
													1.80												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														NyanCat FC													</span>
													1.70												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29324" aria-expanded="true" aria-controls="#two29324">
									Total Match Goal								</h4>
							</div>
							<div id="two29324" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														1 Goal													</span>
													5.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2 Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														3 Goal													</span>
													2.70												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														4 Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														5 or 5+ Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29325" aria-expanded="true" aria-controls="#two29325">
									Total Match Goal Under/Over								</h4>
							</div>
							<div id="two29325" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2.5 Under													</span>
													2.50												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2.5 Over													</span>
													1.30												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29326" aria-expanded="true" aria-controls="#two29326">
									Full Time Total Goal								</h4>
							</div>
							<div id="two29326" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Odd													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Even													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
												</div>
				</div>
			</div>
		</div>
			<div class=" panel2 Cricket" role="tab">
			<div class="panel-heading first-lebal "  style="cursor: pointer;">
				<div  class="colupsPanelHeader panel-title" data-toggle="collapse" href="#one2924" aria-expanded="true" aria-controls="#one2924">
					<div class="">
						<div class="published_date clearfix">
							<strong>20</strong> <span>-2</span>
						</div>
						<div class="extra_content_wrap">
							<a href="#">
								<h5>ECS Hungary, 2020</h5>
								<p>Dunabogdany CC vs  Royal Tigers Cricke...{23-08-2020}|</p>
								<p>Time - 03-00-PM</p>
							</a>
						</div>
					</div>                                                  
				</div>
			</div>
			<div id="one2924" class="panel-collapse collapse" role="tabpanel" aria-labelledby="">
				<div class="panel-body" style="padding: 0px;">
					<div class=" panel2">
													<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29241" aria-expanded="true" aria-controls="#two29241">
									To Win The Toss								</h4>
							</div>
							<div id="two29241" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Dunabogdany CC													</span>
													1.90												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Royal Tigers Cricke													</span>
													1.90												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29242" aria-expanded="true" aria-controls="#two29242">
									1st ball (1st innigs)								</h4>
							</div>
							<div id="two29242" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Dot ball													</span>
													1.42												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														1 Run													</span>
													2.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2 Runs													</span>
													4.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														3 Runs													</span>
													10.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														4 Runs													</span>
													4.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														6 Runs													</span>
													6.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Wide													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Wicket													</span>
													10.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Ball													</span>
													13.00												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29243" aria-expanded="true" aria-controls="#two29243">
									1st Over Runs (1st innigs)								</h4>
							</div>
							<div id="two29243" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														0-5													</span>
													2.10												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														6-9													</span>
													1.70												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														10-13													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														14 or 14+													</span>
													3.20												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29244" aria-expanded="true" aria-controls="#two29244">
									Fall Of 1st Wicket By Run								</h4>
							</div>
							<div id="two29244" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														17.5 Under													</span>
													1.50												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														17.5 Over													</span>
													2.10												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29245" aria-expanded="true" aria-controls="#two29245">
									1st wicket (1st innigs)								</h4>
							</div>
							<div id="two29245" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Cautch Out													</span>
													1.40												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Bowled Out													</span>
													2.70												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														LBW													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Others													</span>
													5.00												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29246" aria-expanded="true" aria-controls="#two29246">
									1st innings total run								</h4>
							</div>
							<div id="two29246" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Odd													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Even													</span>
													1.85												</div>
											</div>
											 
								</div>
							</div>
												</div>
				</div>
			</div>
		</div>
			<div class=" panel2 Football" role="tab">
			<div class="panel-heading first-lebal "  style="cursor: pointer;">
				<div  class="colupsPanelHeader panel-title" data-toggle="collapse" href="#one2933" aria-expanded="true" aria-controls="#one2933">
					<div class="">
						<div class="published_date clearfix">
							<strong>20</strong> <span>-2</span>
						</div>
						<div class="extra_content_wrap">
							<a href="#">
								<h5>Football Liga Pro 12 Min Play</h5>
								<p>Ezid11 Esports vs NyanCat FC...{23-08-2020}|</p>
								<p>Time - 03-25-PM</p>
							</a>
						</div>
					</div>                                                  
				</div>
			</div>
			<div id="one2933" class="panel-collapse collapse" role="tabpanel" aria-labelledby="">
				<div class="panel-body" style="padding: 0px;">
					<div class=" panel2">
													<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29331" aria-expanded="true" aria-controls="#two29331">
									Half Time Result								</h4>
							</div>
							<div id="two29331" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Ezid11 Esports 													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														NyanCat FC													</span>
													2.40												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Draw													</span>
													1.80												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29332" aria-expanded="true" aria-controls="#two29332">
									Full Time Result								</h4>
							</div>
							<div id="two29332" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Ezid11 Esports													</span>
													1.75												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														NyanCat FC													</span>
													2.35												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Draw													</span>
													2.50												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29333" aria-expanded="true" aria-controls="#two29333">
									Team Of 1st Goaol								</h4>
							</div>
							<div id="two29333" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Ezid11 Esports													</span>
													1.60												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														NyanCat FC													</span>
													1.90												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													5.00												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29334" aria-expanded="true" aria-controls="#two29334">
									Total Match Goal								</h4>
							</div>
							<div id="two29334" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														1 Goal													</span>
													4.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2 Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														3 Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														4 Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														5 or 5+ Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													5.00												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29335" aria-expanded="true" aria-controls="#two29335">
									Total Match Goal Under/Over								</h4>
							</div>
							<div id="two29335" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2.5 Under													</span>
													2.40												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2.5 Over													</span>
													1.35												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29336" aria-expanded="true" aria-controls="#two29336">
									Full Time Total Goal								</h4>
							</div>
							<div id="two29336" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Odd													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Even													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
												</div>
				</div>
			</div>
		</div>
			<div class=" panel2 Football" role="tab">
			<div class="panel-heading first-lebal "  style="cursor: pointer;">
				<div  class="colupsPanelHeader panel-title" data-toggle="collapse" href="#one2934" aria-expanded="true" aria-controls="#one2934">
					<div class="">
						<div class="published_date clearfix">
							<strong>20</strong> <span>-2</span>
						</div>
						<div class="extra_content_wrap">
							<a href="#">
								<h5>Football Liga Pro 12 Min Play</h5>
								<p>Pele Warriors vs Ezid11 Esports...{23-08-2020}|</p>
								<p>Time - 03-50-PM</p>
							</a>
						</div>
					</div>                                                  
				</div>
			</div>
			<div id="one2934" class="panel-collapse collapse" role="tabpanel" aria-labelledby="">
				<div class="panel-body" style="padding: 0px;">
					<div class=" panel2">
													<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29341" aria-expanded="true" aria-controls="#two29341">
									Half Time Result								</h4>
							</div>
							<div id="two29341" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Pele Warriors													</span>
													2.10												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Ezid11 Esports													</span>
													2.30												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Draw													</span>
													1.80												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29342" aria-expanded="true" aria-controls="#two29342">
									Full Time Result								</h4>
							</div>
							<div id="two29342" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Pele Warriors													</span>
													2.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Ezid11 Esports													</span>
													2.20												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Draw													</span>
													2.50												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29343" aria-expanded="true" aria-controls="#two29343">
									Team Of 1st Goaol								</h4>
							</div>
							<div id="two29343" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Pele Warriors													</span>
													1.70												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Ezid11 Esports													</span>
													1.80												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29344" aria-expanded="true" aria-controls="#two29344">
									Total Match Goal								</h4>
							</div>
							<div id="two29344" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														1 Goal													</span>
													5.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2 Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														3 Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														4 Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														5 or 5+ Goal													</span>
													3.00												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29345" aria-expanded="true" aria-controls="#two29345">
									Total Match Goal Under/Over								</h4>
							</div>
							<div id="two29345" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2.5 Under													</span>
													2.50												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														2.5 Over													</span>
													1.30												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29346" aria-expanded="true" aria-controls="#two29346">
									Full Time Total Goal								</h4>
							</div>
							<div id="two29346" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Odd													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Even													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														No Goal													</span>
													7.00												</div>
											</div>
											 
								</div>
							</div>
												</div>
				</div>
			</div>
		</div>
			<div class=" panel2 TableTennis" role="tab">
			<div class="panel-heading first-lebal "  style="cursor: pointer;">
				<div  class="colupsPanelHeader panel-title" data-toggle="collapse" href="#one2936" aria-expanded="true" aria-controls="#one2936">
					<div class="">
						<div class="published_date clearfix">
							<strong>20</strong> <span>-2</span>
						</div>
						<div class="extra_content_wrap">
							<a href="#">
								<h5>Table Tennis TT Cup</h5>
								<p>Yaremchuk D vs Didukh V...{23-08-2020}|</p>
								<p>Time - 04-40-PM</p>
							</a>
						</div>
					</div>                                                  
				</div>
			</div>
			<div id="one2936" class="panel-collapse collapse" role="tabpanel" aria-labelledby="">
				<div class="panel-body" style="padding: 0px;">
					<div class=" panel2">
													<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29361" aria-expanded="true" aria-controls="#two29361">
									To Win The Match								</h4>
							</div>
							<div id="two29361" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Yaremchuk D													</span>
													1.50												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Didukh V													</span>
													2.10												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29362" aria-expanded="true" aria-controls="#two29362">
									1st Set Win								</h4>
							</div>
							<div id="two29362" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Yaremchuk D													</span>
													1.45												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Didukh V													</span>
													1.95												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29363" aria-expanded="true" aria-controls="#two29363">
									Full Time Total Point								</h4>
							</div>
							<div id="two29363" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Odd													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Even													</span>
													1.85												</div>
											</div>
											 
								</div>
							</div>
												</div>
				</div>
			</div>
		</div>
			<div class=" panel2 TableTennis" role="tab">
			<div class="panel-heading first-lebal "  style="cursor: pointer;">
				<div  class="colupsPanelHeader panel-title" data-toggle="collapse" href="#one2937" aria-expanded="true" aria-controls="#one2937">
					<div class="">
						<div class="published_date clearfix">
							<strong>20</strong> <span>-2</span>
						</div>
						<div class="extra_content_wrap">
							<a href="#">
								<h5>Table Tennis liga pro</h5>
								<p>Fedorov V vs Vahnin G...{23-08-2020}|</p>
								<p>Time - 05-15-PM</p>
							</a>
						</div>
					</div>                                                  
				</div>
			</div>
			<div id="one2937" class="panel-collapse collapse" role="tabpanel" aria-labelledby="">
				<div class="panel-body" style="padding: 0px;">
					<div class=" panel2">
													<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29371" aria-expanded="true" aria-controls="#two29371">
									To Win The Match								</h4>
							</div>
							<div id="two29371" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Vahnin G													</span>
													1.50												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Fedorov V													</span>
													2.10												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29372" aria-expanded="true" aria-controls="#two29372">
									1st Set Win								</h4>
							</div>
							<div id="two29372" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Fedorov V													</span>
													1.90												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Vahnin G													</span>
													1.55												</div>
											</div>
											 
								</div>
							</div>
														<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two29373" aria-expanded="true" aria-controls="#two29373">
									Full Time Total Point								</h4>
							</div>
							<div id="two29373" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Odd													</span>
													1.85												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Even													</span>
													1.85												</div>
											</div>
											 
								</div>
							</div>
												</div>
				</div>
			</div>
		</div>
			<div class=" panel2 Cricket" role="tab">
			<div class="panel-heading first-lebal "  style="cursor: pointer;">
				<div  class="colupsPanelHeader panel-title" data-toggle="collapse" href="#one2729" aria-expanded="true" aria-controls="#one2729">
					<div class="">
						<div class="published_date clearfix">
							<strong>20</strong> <span>-2</span>
						</div>
						<div class="extra_content_wrap">
							<a href="#">
								<h5>Caribbean Premier League</h5>
								<p>Guyana Amazon Warriors vs St Lucia Zouks...{24-08-2020}|</p>
								<p>Time - 12-15-AM</p>
							</a>
						</div>
					</div>                                                  
				</div>
			</div>
			<div id="one2729" class="panel-collapse collapse" role="tabpanel" aria-labelledby="">
				<div class="panel-body" style="padding: 0px;">
					<div class=" panel2">
													<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two27291" aria-expanded="true" aria-controls="#two27291">
									To Win The Toss								</h4>
							</div>
							<div id="two27291" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														St Lucia Zouks													</span>
													1.90												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														Guyana Amazon													</span>
													1.90												</div>
											</div>
											 
								</div>
							</div>
												</div>
				</div>
			</div>
		</div>
			<div class=" panel2 Cricket" role="tab">
			<div class="panel-heading first-lebal "  style="cursor: pointer;">
				<div  class="colupsPanelHeader panel-title" data-toggle="collapse" href="#one2730" aria-expanded="true" aria-controls="#one2730">
					<div class="">
						<div class="published_date clearfix">
							<strong>20</strong> <span>-2</span>
						</div>
						<div class="extra_content_wrap">
							<a href="#">
								<h5>Caribbean Premier League</h5>
								<p>St Kitts & Nevis Patriots vs Barbados Tridents...{25-08-2020}|</p>
								<p>Time - 08-00-PM</p>
							</a>
						</div>
					</div>                                                  
				</div>
			</div>
			<div id="one2730" class="panel-collapse collapse" role="tabpanel" aria-labelledby="">
				<div class="panel-body" style="padding: 0px;">
					<div class=" panel2">
													<div class="panel-heading second-lebal">
								<h4 style="cursor: pointer" class="panel-title" role="button" data-toggle="collapse"  href="#two27301" aria-expanded="true" aria-controls="#two27301">
									To Win The Toss								</h4>
							</div>
							<div id="two27301" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="">
								<div class="panel-body" style="padding: 0px;background: #3a3a3a; background:#7E8A94 !important;">
																				<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														St Kitts & Nevis													</span>
													1.90												</div>
											</div>
																						<div class="betAsia_participant sports_event_perticipant_3rd mA btn btn-default btn-sm data-show "data-toggle='modal'  data-target="#SignIn">
												<div class="bet_modal" style="color: #000;font-weight: bold;">
													<span class="event_perticipant_name" >
														 Barbados Tridents													</span>
													1.90												</div>
											</div>
											 
								</div>
							</div>
												</div>
				</div>
			</div>
		</div>
	          </div>
        </div>
      </div>
      <div class="upcoming-matches" style="border-top: 30px solid #0A2940;">

        <div class="news_update_panel common_block_panel">
          <h2>Roulette Game</h2>
          <div class="news_container_block" id="scrollable">
                      </div>
        </div>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="login-notice" role="dialog">
  <div class="modal-dialog  " >
    <div class="modal-content m-content">
      <div class="modal-header m-head mh-color" style="  background: #027B5B;">
        <button type="button" class="close" data-dismiss="modal" style="color: #ffff00">&times;</button>
        <h4 class="modal-title"  style="color: #fff">   &nbsp; Sign In To ibet39</h4>
      </div>
      <div class="modal-body" style="padding: 2% !important">
        <form action="" method="POST" style="padding: 0;box-shadow: none">
          <div class="signup-form">
            <div  id="formData">
              <div id="errorSignIn" class="alert alert-danger errorSignIn" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>  <strong>  Opps !!</strong> Please Login frist !!
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="profile1" role="dialog">
  <div class="modal-dialog  " >
    <div class="modal-content m-content">
      <div class="modal-header m-head mh-color" style="  background: #027B5B;">
        <button type="button" class="close" data-dismiss="modal" style="color: #ffff00">&times;</button>
        <h4 class="modal-title"  style="color: #fff">   &nbsp; Profile In ibet39</h4>
      </div>
      <div class="modal-body" style="padding: 2% !important">
        <form action="" method="POST" style="padding: 0;box-shadow: none">
          <div class="signup-form">
            <div  id="formData">
                            <div id="errorSignIn" class="alert alert-success" role="alert">
                <strong>Name: </strong><br><hr>
                <strong>User Name: </strong><br><hr>
                <strong>Email: </strong><br><hr>
                <strong>Mobile: </strong><br><hr>
                <strong>Club: 
                                    
                </strong><br><hr>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="SignIn" role="dialog">
  <div class="modal-dialog  " >
    <div class="modal-content m-content">
      <div class="modal-header m-head mh-color" style="  background: #027B5B;">
        <button type="button" class="close" data-dismiss="modal" style="color: #ffff00">&times;</button>
        <h4 class="modal-title"  style="color: #fff">   &nbsp; Sign In To ibet39</h4>
      </div>
      <div class="modal-body" style="padding: 2% !important">
        <form action="login.php" method="POST" style="padding: 0;box-shadow: none">
          <div class="signup-form">
            <div  id="formData">
              <div class="form-group">
                <label>USER ID<span style="color: red">*</span></label>
                <input type="text" class="form-control" name="user_name" 
                value="" id="userIdOfuser" placeholder="user Id" required>
                <span id="userIdError" style="color: #C84038;font-family: initial;"></span>
              </div>
              <div class="form-group">
                <label> PASSWORD  <span style="color: red">*</span></label>
                <input type="password" class="form-control" name="password" value="" id="passwordOfuser" placeholder="password" required="required"  pattern=".{6,}"   title="6 characters minimum">
                <span>Password at least 6 characters.</span>
              </div>
              <div class="form-group">
                <button type="submit" id="userSignInForm"  name="form"  class="btn btn-success btn-lg btn-block mh-color" style="background: #027B5B">Registration</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="change-club" role="dialog">
  <div class="modal-dialog  " >
    <div class="modal-content m-content">
      <div class="modal-header m-head mh-color" style="  background: #027B5B;">
        <button type="button" class="close" data-dismiss="modal" style="color: #ffff00">&times;</button>
        <h4 class="modal-title"  style="color: #fff">   &nbsp; Change Club in ibet39</h4>
      </div>
      <div class="modal-body" style="padding: 2% !important">
        <form action="" method="POST" style="padding: 0;box-shadow: none">
          <div class="signup-form">
            <div  id="formData">
              <div class="form-group">
                <label>Select Club<span style="color: red">*</span></label>
                <select class="form-control" name="club">
                  <option value="">Select Club</option>
                                      <option value="1" >ibet39 Official</option>
                                        <option value="2" >Bet Bazar Club</option>
                                        <option value="3" >Lions Club</option>
                                        <option value="4" >Dubai Club</option>
                                        <option value="5" >Sports Club</option>
                                        <option value="7" >Boss Club</option>
                                        <option value="11" >Golden Club</option>
                                        <option value="12" >Rajshahi Club</option>
                                        <option value="13" >Khulna Club</option>
                                        <option value="16" >Tiger Boys Club</option>
                                        <option value="17" >BetbuzZ Club</option>
                                        <option value="18" >Box Club</option>
                                        <option value="19" >IPL T20 Club</option>
                                        <option value="20" >Lucky Club</option>
                                        <option value="21" >BETHERO CLUB</option>
                                        <option value="33" >Gopalgonj Club</option>
                                        <option value="45" >Popular Club</option>
                                        <option value="49" >Rich Club</option>
                                        <option value="50" >Narayanganj Club</option>
                                        <option value="51" >DEGREE Club</option>
                                        <option value="52" >Bogura King Club</option>
                                        <option value="53" >Tamim28 Club</option>
                                        <option value="54" >Discover Club</option>
                                        <option value="55" >Duronto Club</option>
                                    </select>
              </div>
              <div class="form-group">
                <label> PASSWORD  <span style="color: red">*</span></label>
                <input type="password" class="form-control" name="password" value="" id="passwordOfuser" placeholder="password" required="required"  pattern=".{6,}"   title="6 characters minimum">
                <span>Password at least 6 characters.</span>
              </div>
              <div class="form-group">
                <button type="submit" id=""  name="form6"  class="btn btn-success btn-lg btn-block mh-color" style="background: #027B5B">Submit</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- sign up -->
<div id="SignUp" class="modal fade" role="dialog" >
  <div class="modal-dialog  " >
    <!-- Modal content-->
    <div class="modal-content m-content">
      <div class="modal-header m-head mh-color" style="  background: #027B5B;">
        <button type="button" class="close" data-dismiss="modal" style="color: #ffffff">&times;</button>
        <h4 class="modal-title"  style="color: #fff"> &nbsp; Wellcome To ibet39 </h4>
      </div>
      <div class="modal-body" style="padding: 2% !important">
        <form method="POST" action="" style="padding: 0;box-shadow: none">
          <div class="signup-form">
            <div  id="formData">
              <div id="error" class="alert alert-danger error" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>  <strong>  Opps !! </strong><span id="signuperrorText"></span>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-6">
                    <label> FULL NAME <span style="color: red">*</span></label>
                    <input type="text" class="form-control" name="full_name" id="name" placeholder="Name" required>
                  </div>
                  <div class="col-xs-6">
                    <label>USER ID <span style="color: red">*</span></label>
                    <input type="text" class="form-control" name="username" 
                    value="" id="userId" placeholder="user Id" required>
                    <span id="userIdError" style="color: #C84038;font-family: initial;"></span>
                  </div>
                </div>          
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-6">
                    <label>MOBILE NUMBER <span style="color: red">*</span></label>
                    <input type="text" class="form-control" name="number" id="mobileNumber" placeholder="mobileNumber" required>
                    <span id="mobileError" style="color: #C84038;font-family: initial;"></span>
                  </div>
                  <div class="col-xs-6">
                    <label>EMAIL <span style="color: red">*</span></label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="email" required="required">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-6">
                    <label>Select Club <span style="color: red">*</span></label>
                    <select class="form-control" name="club">
                      <option value="">Select Club</option>
                                          </select>
                  </div>
                  <div class="col-xs-6">
                    <label>Sponsor</label>
                    <input type="text" class="form-control" name="sponsor_name" id="sponsor" placeholder="Your Sponsor" >
                  </div>
                </div>
              </div> 
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-6">
                    <label>PASSWORD <span style="color: red">*</span></label>
                    <input type="password" class="form-control" name="password" value="" id="password" placeholder="password" required="required"  pattern=".{6,}"   title="6 characters minimum">
                    <span>Password at least 6 characters.</span>
                  </div>
                  <div class="col-xs-6">
                    <label>CONFIRM PASSWORD <span style="color: red">*</span></label>
                    <input type="password" class="form-control" name="confirm_password" id="confirmPassword" placeholder="confirm Password" required="required"  pattern=".{6,}"   title="6 characters minimum">
                    <span id="passwordError" style="color: #C84038;font-family: initial;"></span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <button type="submit" id="userSignUp"  name="registration"  class="btn btn-success btn-lg btn-block mh-color" style="background: #027B5B">REGISTER NOW</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="bet-sel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Bet Sell Confirmation</h4>
      </div>
      <div class="modal-body" style="background:url('./images/reg-bg.jpg')no-repeat bottom center !important;">
        <p>Are you sure want to sell this bet ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="cancel-withrow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Cancel Withrow Confirmation</h4>
      </div>
      <div class="modal-body" style="background:url('./images/reg-bg.jpg')no-repeat bottom center !important;">
        <p>Are you sure want to cancel this withrow ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        <a class="btn btn-danger btn-ok">Yes</a>
      </div>
    </div>
  </div>
</div>

<div id="clubwidthdraw" class="modal fade" role="dialog" >
  <div class="modal-dialog  ">
    <div class="modal-content">
      <div class="modal-header m-head mh-color" style="  color:#fff !important;">
        <button onClick="window.location.reload();" type="button" class="close" data-dismiss="modal" style="color: #ffffff">×</button>
        <h4 class="modal-title" style="color: #D2D2D2" id="clubwidthdraw"> <img style="width: 85px;height: 32px;margin-left: 10px;" src="images/logo.png"> &nbsp;Request a withdraw</h4>
      </div>
      <form action="" method="post">
        <div class="modal-body" style="padding: 2% !important">
          <div role="form" class="register-form">
           <div id="status_data1">
           </div>
           <hr class="colorgraph">
           <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <label style="text-align: left;width: 100%;">Method<span style="color:#DD4F43;">*</span></label>
                <select class="form-control" name="withdraw_method" id="withdraw_method">
                  <option value="">Select Method</option>
                  <option value="Instant Transfer">Instant Transfer</option> 
                  <option value="TrustPay">TrustPay</option> 
                  <option value="BKASH   (বাংলাদেশ)">BKASH   (বাংলাদেশ)</option>
                  <option value="NAGAD (বাংলাদেশ)">NAGAD (বাংলাদেশ)</option>
                  <option value="Neteller">Neteller</option> 
                  <option value="Banco Do Brasil">Banco Do Brasil</option> 
                  <option value="Naranja">Naranja</option> 
                  <option value="Santander Rio">Santander Rio</option> 
                  <option value="HiperCard">HiperCard</option> 
                  <option value="Teleingreso">Teleingreso</option> 
                  <option value="CMR Falabella">CMR Falabella</option>
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <label style="text-align: left;width: 100%;">Type <span style="color:#DD4F43;">*</span></label>
                <select placeholder="Account Type" class="form-control" id="withdraw_account_type" name="account_type">
                  <option value="">Account Type</option>
                  <option value="Personal">Personal</option>
                  <option value="Agent">Agent</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <label style="text-align: left;width: 100%;">Amount <span style="color:#DD4F43;">*</span></label>
                <input type="text" class="form-control bg-light-gray" id="req-withdraw-amount"
                name="withdraw_amount"  placeholder="Amount" tabindex="1">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
              <label style="text-align: left;width: 100%;"> To <span style="color:#DD4F43;">*</span></label>
              <div class="form-group">
                <input type="text" id="req-withdraw-to"
                name="withdraw_to" class="form-control input-lg" placeholder=" To " tabindex="1">
              </div>
            </div>
          </div>
          <hr class="colorgraph">
          <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6"><a  style="color:#fff;" onclick="cwithdrawPost();" value="" class="btn btn-block btn-lg mh-color" tabindex="7"> Submit </a></div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
<div id="club-change-password" class="modal fade" role="dialog" style="display: none;" aria-hidden="true">
  <div class="modal-dialog  ">

    <div class="modal-content">
      <div class="modal-header m-head mh-color">
        <button type="button" onClick="window.location.reload();" class="close" data-dismiss="modal" style="color: #ffffff">×</button>
        <h4 class="modal-title" style="color: #D2D2D2"> <img style="width: 85px;height: 32px;margin-left: 10px;" src="images/logo.png"> &nbsp; Change Password</h4>
      </div>
      <form action="" method="post">
        <div class="modal-body" style="padding: 2% !important">
          <div class="">
            <div role="form" class="register-form">


              <hr class="colorgraph">
              <div class="form-group">
                <label style="text-align: left;width: 100%;">Current Password <span style="color:#DD4F43;">*</span></label>
                <input type="password" name="current_password" id="user-current-password" class="form-control input-lg" placeholder="Current Password " tabindex="3" required="">
              </div>
              <div class="form-group">
                <label style="text-align: left;width: 100%;">New Password <span style="color:#DD4F43;">*</span></label>
                <input type="password" name="new_password" id="user-new-password" class="form-control input-lg" placeholder="New Password" tabindex="3" minlength="6" required="">
              </div>
              <div class="form-group">
                <label style="text-align: left;width: 100%;">Confirm Password <span style="color:#DD4F43;">*</span></label>
                <input type="password" name="confirm_password" id="user-cnf-password" class="form-control input-lg" placeholder="Confirm Password" tabindex="3" required="" minlength="6">
              </div>
              <hr class="colorgraph">
              <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6"><button style="background: 3c8cb3 !important;color:#fff !important;" type="submit" name="formclub" class="btn btn-block btn-lg mh-color" tabindex="7" > Submit </button></div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
    <div class="fm-FooterModule ">

        <div class="fm-FooterModule_Content "><img src="images/logo.png" class="fm-FooterModule_Logo ">
      <!--     <div class="fm-SitesMenu ">
            <div class="fm-SitesMenu_Link ">Betting News</div>
            <div class="fm-SitesMenu_Link ">Affiliates</div>
            <div class="fm-SitesMenu_Link ">Careers</div>
          </div>
        </div>
       
         <div class="fm-PartnerLogoContainer ">
          <div class="">
            <div class="col-sm-6">
              <img src="images/copyright-logo-1.png" class="fm-PartnerLogoContainer_Icon fm-PartnerLogoContainer_IconLink ">
              <img src="images/copyright-logo-2.png" class="fm-PartnerLogoContainer_Icon ">
            </div>
            <div class="col-sm-6">
              <img src="images/copyright-logo-4.png" class="fm-PartnerLogoContainer_Icon ">
              <img src="images/copyright-logo-5.png" class="fm-PartnerLogoContainer_Icon fm-PartnerLogoContainer_IconLink ">
            </div>
          </div>
        </div>
         <div class="fm-Menu_Container ">
             <div class="fm-Menu_Major ">
                 <div class="fm-Menu_Menu fm-Menu_MenuLeft ">
                    <div class="fm-Menu_Title fm-Menu_TitleHelp ">Help</div>
                    <div class="fm-Menu_Link ">Deposits</div>
                    <div class="fm-Menu_Link ">Rules</div>
                    <div class="fm-Menu_Link ">Withdrawals</div>
                    <div class="fm-Menu_Link ">Privacy Policy</div>
                    <div class="fm-Menu_Link ">Technical Issues</div>
                    <div class="fm-Menu_Link ">Complaints Procedure</div>
                    <div class="fm-Menu_Link " style="">Terms and Conditions</div>
                    <div class="fm-Menu_Link ">ibet39 FAQ</div>
                </div>
              
                </div> 
     
            <div class="fm-Menu_Minor ">
                <div class="fm-Menu_Menu fm-Menu_MenuRight ">
                    <div class="fm-Menu_Title fm-Menu_TitleScores ">Scores &amp; Results</div>
                    <div class="fm-Menu_Link ">Live Scores</div>
                    <div class="fm-Menu_Link ">Results</div>
                    <div class="fm-Menu_Title fm-Menu_TitlePromo ">Promotions</div>
                    <div class="fm-Menu_Link ">Open Account Offer</div>
                    <div class="fm-Menu_Link ">Current Offers</div>
                    <div class="fm-Menu_Link ">Deposit Offers</div>
                 </div>
             </div>
         </div>
    -->
        <div class="fm-FooterModule_License " style="">
          ibet39 is a reliable bookmaker that strives to create a long-term association with each customer. We guarantee a personal approach with your best interests in mind, easy payment methods and, what is most important, 100% payouts on all winning bets! May luck never leave you!
        </div>
        <div class="fm-PartnerLogoContainer footer_payment_logo" style="margin-top:10px;">
            <img src="" class="fm-PartnerLogoContainer_Icon fm-PartnerLogoContainer_IconLink ">
            <img src="images/bkash logo.png" class="fm-PartnerLogoContainer_Icon fm-PartnerLogoContainer_IconLink img-thumbnail">
            <img src="images/nagad logo.png" class="fm-PartnerLogoContainer_Icon fm-PartnerLogoContainer_IconLink  img-thumbnail">
            <img src="images/rocket logo.png" class="fm-PartnerLogoContainer_Icon  img-thumbnail">
           
        </div>
        <div class="fm-FooterModule_License " style="">
          We don't allow anyone under 18 years to bet on this site and also site administrator is not liable for any kind of issues created by user.
        </div>
        <div class="fm-PartnerLogoContainer footer_payment_logo" style="margin-top:10px;">
            <a id="downloadApps" href="#">
              <img src="images/google_play_logo.png" class="fm-PartnerLogoContainer_Icon fm-PartnerLogoContainer_IconLink  img-thumbnail">
            </a>
        </div>
       
        <div class="fm-FooterModule_Copyright ">Copyright © 2020 ibet39.com. All rights reserved.</div>
             <!--    <div class="fm-FooterModule_SocialMedia ">
                     <div class="fm-FooterModule_Twitter ">Follow</div>
                     <div class="fm-FooterModule_Facebook ">Like</div>
                 </div>
             -->   
    </div><script src="js/jquery.min_1.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
        function openNav() {
          document.getElementById("mySidenav").style.width = "250px";
        }

        function closeNav() {
          document.getElementById("mySidenav").style.width = "0";
        }
      </script>
<script>

  $('#allgame').on('click',function () {
    $('.Football').show();
    $('.Cricket').show();
    $('.Basketball').show();
    $('.Badminton').show();
    $('.Tennis').show();
    $('.vball').show();
    $('.Handball').show();
    $('.Hockey').show();
    $('.Virtual Game').show();
    $('.TableTennis').show();
  });
  $('#football').on('click',function () {
    $('.Football').show();
    $('.Cricket').hide();
    $('.Basketball').hide();
    $('.Badminton').hide();
    $('.Tennis').hide();
    $('.vball').hide();
    $('.Handball').hide();
    $('.Hockey').hide();
    $('.TableTennis').hide();
    $('.Virtual Game').hide();
  });
  $('#cricket').on('click',function () {
    $('.Football').hide();
    $('.Cricket').show();
    $('.Basketball').hide();
    $('.TableTennis').hide();
    $('.Badminton').hide();
    $('.Tennis').hide();
    $('.vball').hide();
    $('.Handball').hide();
    $('.Hockey').hide();
    $('.Virtual Game').hide();
  });
  $('#basketball').on('click',function () {
    $('.Football').hide();
    $('.Cricket').hide();
    $('.Basketball').show();
    $('.Badminton').hide();
    $('.Tennis').hide();
    $('.vball').hide();
    $('.Handball').hide();
    $('.Hockey').hide();
    $('.TableTennis').hide();
    $('.Virtual Game').hide();
  });
  $('#badminton').on('click',function () {
    $('.Football').hide();
    $('.Cricket').hide();
    $('.Badminton').show();
    $('.Tennis').hide();
    $('.vball').hide();
    $('.Basketball').hide();
    $('.Handball').hide();
    $('.Hockey').hide();
    $('.TableTennis').hide();
    $('.Virtual Game').hide();
  });
  $('#tennis').on('click',function () {
    $('.Football').hide();
    $('.Cricket').hide();
    $('.Badminton').hide();
    $('.Tennis').show();
    $('.vball').hide();
    $('.Basketball').hide();
    $('.Handball').hide();
    $('.Hockey').hide();
    $('.TableTennis').hide();
    $('.Virtual Game').hide();
  });
  $('#vball').on('click',function () {
    $('.Football').hide();
    $('.Cricket').hide();
    $('.Badminton').hide();
    $('.Tennis').hide();
    $('.vball').show();
    $('.Basketball').hide();
    $('.Handball').hide();
    $('.Hockey').hide();
    $('.TableTennis').hide();
    $('.Virtual Game').hide();
  });
  $('#handball').on('click',function () {
    $('.Football').hide();
    $('.Cricket').hide();
    $('.Badminton').hide();
    $('.Tennis').hide();
    $('.vball').hide();
    $('.Basketball').hide();
    $('.Handball').show();
    $('.Hockey').hide();
    $('.TableTennis').hide();
    $('.Virtual Game').hide();
  });
  $('#hockey').on('click',function () {
    $('.Football').hide();
    $('.Cricket').hide();
    $('.Badminton').hide();
    $('.Tennis').hide();
    $('.vball').hide();
    $('.Basketball').hide();
    $('.Handball').hide();
    $('.Hockey').show();
    $('.TableTennis').show();
    $('.Virtual Game').hide();
  });
  $('#virtual').on('click',function () {
    $('.Football').hide();
    $('.Cricket').hide();
    $('.Badminton').hide();
    $('.Tennis').hide();
    $('.vball').hide();
    $('.Basketball').hide();
    $('.Handball').hide();
    $('.Hockey').hide();
    $('.TableTennis').hide();
    $('.Virtual Game').show();
  });
  $('#TableTennis').on('click',function () {
    $('.Football').hide();
    $('.Cricket').hide();
    $('.Badminton').hide();
    $('.Tennis').hide();
    $('.vball').hide();
    $('.Basketball').hide();
    $('.Handball').hide();
    $('.Hockey').hide();
    $('.TableTennis').show();
    $('.Virtual Game').hide();
  });
</script>
<script>

  $(document).ready(function(){ $('.itemSlider').owlCarousel({ margin: 0, loop: true, autoplay: true, autoplayTimeout: 4000,  smartSpeed: 800, dots:false, navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'], responsive: { 0: { items: 4 }, 450: { items: 4 }, 768: { items: 5 }, 1000: { items: 6 } } }); });
</script>

<script>
  $(document).ready(function(){ $('.game-slider').owlCarousel({ margin: 0, loop: true, autoplay: true, autoplayTimeout: 3000,  dots:false, responsive: { 0: { items: 1 }, 450: { items: 2 }, 768: { items: 3 },1000: { items: 4 } } }); });
</script>
<script>
  function showTime(){
    var date = new Date();
        var h = date.getHours(); // 0 - 23
        var m = date.getMinutes(); // 0 - 59
        var s = date.getSeconds(); // 0 - 59
        var session = "AM";

        if(h == 0){
          h = 12;
        }

        if(h % 12 == 0){
            // h = h - 12;
            session = "PM";
          }else if (h > 12) {
            h = h - 12;
            session = "PM";
          }

          h = (h < 10) ? "0" + h : h;
          m = (m < 10) ? "0" + m : m;
          s = (s < 10) ? "0" + s : s;

          var time = h + ":" + m + ":" + s + " " + session;
          document.getElementById("siteClock").innerText = time;
          document.getElementById("siteClock").textContent = time;

          setTimeout(showTime, 1000);

        }
        showTime();
        $('input,textarea').attr('autocomplete', 'off');
      </script>

      <script>
        $('.number').on('keypress', function(e) {
          if (e.which == 32)
            return false;
        });
        function valueChange(el) {
          $('#betLabel').text(visual_number_format($(el).val()));
          $('#modalPossibleAmountLabel').text(visual_number_format($('#modalBetRate').val()* $(el).val()));
        }

        $('.amountBtn').click(function () {
          $('.amountBtn').removeClass('btn-active');
          $(this).addClass('btn-active');
          var amount = $(this).text();
          $('#bet').val(amount).trigger('change')
        })
      </script>
      <script>

        $("#bet").on("change keypress input", function() {
          var rate=$("#asshole").text();
          $('#stake').text(this.value);
          var as=document.getElementById("stake").value;
          document.getElementById("possible").style.display="none";
          document.getElementById("poss").style.visibility="visible";
          var as=($('#stake').text()*rate).toFixed(2);
          $('#poss').text(as);
        })
        ;

      </script>
      <script>
        function bet(bet_id) {
          $.ajax({
            url : "bet.php",
            type : "post",
            dataType:"text",
            data : {
             stake_id: bet_id
           },
           success : function (a){
            var respData1 = JSON.parse(a);


            if(respData1.fucker === 'Active'){

              $('#error_data').html(respData1.error);
              $('#first_data').html(respData1.first);
              $('#second_data').html(respData1.second);
              $('#possible').html(respData1.possible);
              $('#asshole').html(respData1.possible1);
              $('#button_data').html(respData1.button);
              $('#betRequestModal').modal('show');
              $('#possible').trigger('change');
              $('#rate').trigger('change');
              $('#betRequestModal').modal('show');
            }
            else{
              location.reload();
            }
          }
        });
        } 
      </script>
      <script>

        function betPost(bet_id) {

          var amount=$("#stake").text();
          $.ajax({
            url : "bet-post.php",
            type : "post",
            dataType:"text",
            data : {
             stake_id: bet_id,
             amount: amount
           },
           success : function (a){
            var respData = JSON.parse(a);

            if(respData.fucker === 'Active'){
              $('#error_data').html(respData.error);
              $('#first_data').html(respData.first);
              $('#second_data').html(respData.second);
              $('#possible').html(respData.possible);
              $('#asshole').html(respData.possible1);
              $('#button_data').html(respData.button);
              $('#betRequestModal').modal('show');
              $('#possible').trigger('change');
              $('#rate').trigger('change');
              $('#betRequestModal').modal('show');
            }
            else{
              location.reload();
            }
          }
        });

        } 

        $('#button_data').click(function(event) {
         document.getElementById("button_data").disabled = true; 
       });
     </script>
     <script>
      function check() {
        document.getElementById("req-deposit-to").value=document.getElementById("sel1").value;
      } 
      function depositPost() {
        var amount=document.getElementById("req-deposit-amount").value;
        var sellist1=document.getElementById("sel1").value;
        var from=document.getElementById("req-deposit-from").value;
        var to=document.getElementById("req-deposit-to").value;
        var transection_number=document.getElementById("req-deposit-transaction_number").value;
        $.ajax({
          url : "deposit.php",
          type : "post",
          dataType:"text",
          data : {
           transection_number: transection_number,
           sellist1: sellist1,
           from: from,
           to: to,
           amount: amount
         },
         success : function (a){
          var respData2 = JSON.parse(a);
          $('#status_data').html(respData2.statues);
          $('#requestDepositModal').modal('show');
        }
      });
      } 
    </script>
    <script>

      function withdrawPost() {

        var withdraw_amount=document.getElementById("req-withdraw-amount").value;
        var withdraw_method=document.getElementById("withdraw_method").value;
        var withdraw_to=document.getElementById("req-withdraw-to").value;
        var account_type=document.getElementById("withdraw_account_type").value;
        $.ajax({
          url : "withdraw.php",
          type : "post",
          dataType:"text",
          data : {
           withdraw_method: withdraw_method,
           account_type: account_type,
           withdraw_to: withdraw_to,
           withdraw_amount: withdraw_amount
         },
         success : function (a){
          var respData3 = JSON.parse(a);
          $('#status_data1').html(respData3.wstatues);
          $('#buttondata').html(respData3.buttondata);
          $('#requestWithdrawModal').modal('show');
        }
      });
      } 
      $('#buttondata').click(function(event) {
        $('#buttondata').addClass('hidden');
        $('#button_data_loading').removeClass('hidden');
      });
    </script>
    <script>
      $('#bet-sel').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
      });
      $('#cancel-withrow').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
      });
    </script>
    <script>

      function u2ubtPost() {

        var password=document.getElementById("cpassword").value;
        var amount=document.getElementById("amount").value;
        var username=document.getElementById("username").value;
        $.ajax({
          url : "u2ubtpost.php",
          type : "post",
          dataType:"text",
          data : {
           password: password,
           amount: amount,
           username: username
         },
         success : function (a){
          var respData3 = JSON.parse(a);
          $('#wstatues').html(respData3.wstatues);
          $('#wstatues1').html(respData3.wstatues1);
          $('#balancetrnsfr').modal('show');
        }
      });

      } 
    </script>
    <script>
      $('.list-item').click(function () {
        $('.list-item').removeClass('active');
        $(this).addClass('active');
        $('.bhoechie-tab').addClass('hide');
        $($(this).attr('href')).removeClass('hide');
      });

      $('#bet-sel').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
      });
    </script>





    <script type="text/javascript">
      setTimeout(function(){
        document.getElementById('aap').className = 'waa';
      }, 5000);
      jQuery(document).ready(function($){
        var addedclass="";
        var flipResult="";
        var result="";
        var amount="";
        var stake="";
        var run=0;
        $('#btn-1').click(function(event) {
          if(run==0)
          {

           stake="1";
           $('#btn-1').addClass('active');
           $('#btn-2').removeClass('active');
           $('#btn-3').removeClass('active');
           $('#btn-4').removeClass('active');
           $('#btn-5').removeClass('active');
           $('#btn-6').removeClass('active');
         }
       });

        $('#btn-2').click(function(event) {
          if(run==0)
          {

           stake="2";
           $('#btn-1').removeClass('active');
           $('#btn-2').addClass('active');
           $('#btn-3').removeClass('active');
           $('#btn-4').removeClass('active');
           $('#btn-5').removeClass('active');
           $('#btn-6').removeClass('active');
         }
         else
         {

         }
       });

        $('#btn-3').click(function(event) {
          if(run==0)
          {

           stake="3";
           $('#btn-1').removeClass('active');
           $('#btn-2').removeClass('active');
           $('#btn-3').addClass('active');
           $('#btn-4').removeClass('active');
           $('#btn-5').removeClass('active');
           $('#btn-6').removeClass('active');
         }
         else
         {

         }
       });
        $('#btn-4').click(function(event) {
          if(run==0)
          {

           stake="4";
           $('#btn-1').removeClass('active');
           $('#btn-2').removeClass('active');
           $('#btn-3').removeClass('active');
           $('#btn-4').addClass('active');
           $('#btn-5').removeClass('active');
           $('#btn-6').removeClass('active');
         }
         else
         {

         }
       });
        $('#btn-5').click(function(event) {
          if(run==0)
          {

           stake="5";
           $('#btn-1').removeClass('active');
           $('#btn-2').removeClass('active');
           $('#btn-3').removeClass('active');
           $('#btn-4').removeClass('active');
           $('#btn-5').addClass('active');
           $('#btn-6').removeClass('active');
         }
         else
         {

         }
       });
        $('#btn-6').click(function(event) {
          if(run==0)
          {

           stake="6";
           $('#btn-1').removeClass('active');
           $('#btn-2').removeClass('active');
           $('#btn-3').removeClass('active');
           $('#btn-4').removeClass('active');
           $('#btn-5').removeClass('active');
           $('#btn-6').addClass('active');
         }
         else
         {

         }
       });
        $('#play1').on('click', function(){
          alert('Refresh your browser');
          location.reload();
        });
        $('#play').on('click', function(){
          run=0;

          $('#btn-tail').css('background', '#123');
          $('#btn-head').css('background', '#123');
          var amount=$('#ludu_amount').val();
          if(stake=="" )
          {
            alert('choose your point (1-6)');
            run=0;
          }
          else
          {
            if(amount<=0)
            {
              alert('Enter Stake amount');
              run=0;
            }
            else
            {
              $.ajax({
                method: "POST",
                url:'ludupost.php',
                data : {
                 stake: stake,
                 amount: amount
               },
               success : function (a){
                var respData = JSON.parse(a);
                $('#coin').html(respData.wstatues);
                $('#result').html(respData.b);
                $('#notice').html(respData.c);
                $('#posiblewin').html(respData.d);
                $('#error').html(respData.error);
              }
            }); 
            }
          }
        });
                $('#ludu_amount').keyup(function(event) {
          var rate=3.5;
          var amount=$('#ludu_amount').val();
          var returna=amount*rate;
          var returna=returna.toFixed(1); 
          $('#pwin').html(returna);
        });
      });


    </script>

    <script type="text/javascript">
      setTimeout(function(){
        document.getElementById('aap').className = 'waa';
      }, 5000);
      jQuery(document).ready(function($){
        var addedclass="";
        var flipResult="";
        var result="";
        var coin_amount="";
        var coin_stake="";
        var run=0;
        $('#btn-head').click(function(event) {
          if(run==0)
          {

           coin_stake="1";
           $('#btn-head').addClass('active');
           $('#btn-till').removeClass('active');
         }
       });

        $('#btn-till').click(function(event) {
          if(run==0)
          {

           coin_stake="2";
           $('#btn-head').removeClass('active');
           $('#btn-till').addClass('active');
         }
         else
         {

         }
       });

        $('#cplay').on('click', function(){
          run=0;
          var coin_amount=$('#coin_amount').val();
          if(coin_stake=="" )
          {
            alert('choose your point ( Head or Till )');
            run=0;
          }
          else
          {
            if(coin_amount<=0)
            {
              alert('Enter Stake amount');
              run=0;
            }
            else
            {
              $.ajax({
                method: "POST",
                url:'coinpost.php',
                data : {
                 coin_stake: coin_stake,
                 coin_amount: coin_amount
               },
               success : function (a){
                var respData = JSON.parse(a);
                $('#coin').html(respData.wstatues);
                $('#result').html(respData.b);
                $('#notice').html(respData.c);
                $('#posiblewin').html(respData.posiblewin);
                $('#error').html(respData.error);
              }
            }); 
            }
          }
        });

                $('#coin_amount').keyup(function(event) {
      // var rate=2.2;
      var rate=1.8;
      var amount=$('#coin_amount').val();
      var returna=amount*rate;
      var returna=returna.toFixed(1); 
      $('#coin_pwin').html(returna);
    });
      });


    </script>
    <script type="text/javascript">
      setTimeout(function(){
        document.getElementById('aap').className = 'waa';
      }, 5000);
      jQuery(document).ready(function($){
        var coin_amount="";
        $('#playludutouch').on('click', function(){
          $.ajax({
            method: "POST",
            url:'ludutouchpost.php',
            success : function (a){
              var respData = JSON.parse(a);
              $('#coin').html(respData.wstatues);
              $('#result').html(respData.b);
              $('#notice').html(respData.c);
              $('#posiblewin').html(respData.posiblewin);
              $('#error').html(respData.error);
            }
          }); 
        });

      });


    </script>
<script>
  function showTime(){
    var date = new Date();
        var h = date.getHours(); // 0 - 23
        var m = date.getMinutes(); // 0 - 59
        var s = date.getSeconds(); // 0 - 59
        var session = "AM";

        if(h == 0){
          h = 12;
        }

        if(h % 12 == 0){
            // h = h - 12;
            session = "PM";
          }else if (h > 12) {
            h = h - 12;
            session = "PM";
          }

          h = (h < 10) ? "0" + h : h;
          m = (m < 10) ? "0" + m : m;
          s = (s < 10) ? "0" + s : s;

          var time = h + ":" + m + ":" + s + " " + session;
          document.getElementById("siteClock").innerText = time;
          document.getElementById("siteClock").textContent = time;

          setTimeout(showTime, 1000);

        }
        showTime();


      </script>
      <script>
        function openNav() {
          document.getElementById("mySidenav").style.width = "250px";
        }

        function closeNav() {
          document.getElementById("mySidenav").style.width = "0";
        }
      </script>
    </body>
    </html>