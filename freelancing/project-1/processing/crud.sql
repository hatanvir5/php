create database crud;
use crud;
create table departments(
    id int(11) auto_increment primary key,
    departmentName varchar(30) not null,
    name varchar(30) not null,
    email varchar(30) not null unique,
    phone varchar(15) not null ,
    gender varchar(9),
    department varchar(7),
    birthday date,
    joiningDateTime dateTime,
    file binary,
    bdaymonth varchar,
    quantity number(6),
    mulitpleFile json,
    range number,
    mulitipleSelect varchar,
    description varchar(150);
)

