<?php
require 'db.php';

$data=$_POST;
if(array_key_exists('departmentName',$data)){
    $departmentName=$data['departmentName'];
}
if(array_key_exists('id',$data)){
    $id=$data['id'];
}
$message='';

$sql='UPDATE departments set departmentName=:departmentName WHERE id=:id ';
$statement=$connection->prepare($sql);
if($statement->execute([':departmentName'=>$departmentName,':id'=>$id])){
    echo 'data updated successfully';
}

header("Location:index.php");
