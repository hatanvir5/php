<?php
require 'header.php';
?>

<div class="container bg-light">
  <h3 class="mt-3 text-dark">Check Html All input types</h3>

  <form method="post" action="store.php"> 
    <div class="form-group">
    <label>Department Name:</label>
    <input class="form-control" type="text" name="departmentName">
    </div>

    <div class="form-group">
    <label>Email:</label>
    <input class="form-control" type="email" name="email">
    </div>

    <div class="form-group">
    <label>Phone:</label>
    <input class="form-control" type="tel" name="phone">
    </div>

    <div class="form-group">
    <label>Password:</label><br>
    <input class="form-control" type="password"  name="password">
    </div>

    <div class="form-group">
    <b>       Gender         </b><br>
    <input type="radio" id="male" name="gender"  value="male">
      <label for="male">Male</label><br>
      <input type="radio" id="female" name="gender" value="female">
      <label for="female">Female</label><br>
      <input type="radio" id="other" name="gender"   value="other">
      <label for="other">Other</label>
    </div>

    <div class="form-group form-check">
      <b>Allow Department</b><br>
    <input type="checkbox" name="department" value="CSE">
      <label >CSE</label><br>
      <input type="checkbox" name="department" value="EEE">
      <label >EEE</label><br>
      <input type="checkbox" name="department" value="ETE">
      <label >ETE</label><br>
    </div>





    <div>
    <label for="birthday">Birthday:</label>
    <input class="form-control" type="date" name="birthday">
    </div>

    <div class="form-group">
    <label for="">Joining Date and Time:</label>
    <input class="form-control" type="datetime-local" name="joiningDateTime">
    </div>

    <div class="form-group">
    <label for="file">Select a file:</label>
    <input class="form-control-file" type="file" id="file" name="file">
    </div>

    <div class="form-group">
    <label for="bdaymonth">Birthday(month):</label>
    <input class="form-control" type="month"  name="bdaymonth">
    </div>

    <div class="form-group">
    <label for="quantity">Quantity (between 1 and 5):</label>
    <input class="form-control" type="number" id="quantity" name="quantity" min="1" max="5">
    </div>

    <div class="form-group">
    <label for="fname">Read Only No Edit:</label><br>
    <input class="form-control" type="text" name="readOnly" value="NO Edit" readonly><br>
    </div>

    <div class="form-group">
    <label for="fname">Disabled:</label><br>
      <input class="form-control" type="text" name="disabled" value="Disabled" disabled><br>
    </div>

    <div class="form-group">
    <label for="pin">PIN(Size Define):</label><br>
      <input class="form-control" type="text" id="pin" name="pin" size="4">
    </div>

    <div class="form-group">
    <label for="files">Select Multiple files:</label>
      <input class="form-control-file" type="file" id="files" name="mulitpleFile" multiple>
    </div>

    <div class="form-group">
    <label for="formControlRange">Example Range input</label>
    <input type="range" name="range" class="form-control-range" id="formControlRange">
  </div>

  <div class="form-group">
    <label for="exampleFormControlSelect2">Example multiple select</label>
    <select multiple class="form-control" name="mulitipleSelect" id="exampleFormControlSelect2">
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div>

  <div  class="form-group">
    <label for="">Description</label>
    <textarea class="form-control" name="description" id="" cols="5" rows="3"></textarea>
  </div>
        
      <button class="btn btn-success mb-5" type="submit">Submit</button>

</form>
</div>
<?php
require 'footer.php';
?>