<?php require_once("header.php")?>
<!-- Custom css start just use only contact form -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<!-- Custom  css end just use only contact form -->

<div class="container-contact100 py-3 bg-primary">
	
 
		<div class="wrap-contact100 my-5">
			<div class="job100-form-title" style="padding: 50px;">
			<h2>Wellcome my Online Studio BD</h2>
			<div class="text-center mt-4"> <h4>আবেদনের র্নির্দেশিকা</h4></div>
		  <div class="text-danger mt-4"> 	<p>১. প্রথমে আমাদের ওয়েবসাইট সিভি পূরণ করতে হবে। সিভি পূরণ না করে থাকলে এখানে ‍<a class="text-primary" href="cvform.php">ক্লিক করুন </a> </p>
		  <p>২. চাকুরীর সারকুলার ভালো ভাবে পড়ুন যে আপনার যোগ্যতা, বয়স, আপনি কোন জেলার প্রার্থী আবেদন করতে পারবে কি না ।</p>
		  <p>৩. সিভি তে আপনার মোবাইল নং ও ই-মেইল আবেদন ফরমে দিন।</p>
		  <p>৪.আবেদন ফরম পূরণ করার সঙ্গে সঙ্গে বিকাশ/রকেট/শিওরক্যাশ এর মাধ্যমে আবেদন ফি জমা দিন। </p>
		  </div>

			</div>
		
			<form class="contact100-form validate-form">
				<div class="wrap-input100 validate-input" data-validate="Name is required">
					<span class="label-input100">নাম:<span class="red">*</span> </span>
					<input class="input100" type="text" name="name" placeholder="আপনার নাম দিন">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
					<span class="label-input100">ই-মেইল: <span class="red">*</span></span>
					<input class="input100" type="text" name="email" placeholder="আপনার ইমেইল দিন">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate="Phone is required">
					<span class="label-input100">মোবাইল নং: <span class="red">*</span></span>
					<input class="input100" type="text" name="phone" placeholder="আপনার মোবাইল নং দিন">
					<span class="focus-input100"></span>
				</div>
				<div class="wrap-input100 validate-input" data-validate="instution is required">
					<span class="label-input100">প্রতিষ্ঠানের নাম:<span class="red">*</span></span>
					<input class="input100" type="text" name="phone" placeholder="">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate="Phone is required">
					<span class="label-input100">পদের নাম:<span class="red">*</span></span>
					<input class="input100" type="text" name="phone" placeholder="">
					<span class="focus-input100"></span>
				</div>

				<div class="container-contact100-form-btn">
					<button class="contact100-form-btn">
						<span>
							Submit
							<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
						</span>
					</button>
				</div>
			</form>
		</div>
	</div>


<?php require_once("footer.php")?>