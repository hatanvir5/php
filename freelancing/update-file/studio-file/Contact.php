<?php require_once("header.php")?>
<!-- Custom css start just use only contact form -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<!-- Custom  css end just use only contact form -->

<div class="container-contact100 py-3 bg-primary">
	
 
		<div class="wrap-contact100">
			<div class="contact100-form-title" style="background-image: url(images/contactimage.png);">
				 
			</div>
			<div class="ml-auto py-4"> <h2>Contact Us </h2></div>
			<div class="text-center"> <p> নিশ্চিন্তে আপনার মতামত জানান ।</p></div>

			<form class="contact100-form validate-form">
				<div class="wrap-input100 validate-input" data-validate="Name is required">
					<span class="label-input100">Full Name:<span class="red">*</span></span>
					<input class="input100" type="text" name="name" placeholder="Enter full name">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
					<span class="label-input100">Email: <span class="red">*</span></span>
					<input class="input100" type="text" name="email" placeholder="Enter email addess">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate="Phone is required">
					<span class="label-input100">Phone: <span class="red">*</span></span>
					<input class="input100" type="text" name="phone" placeholder="Enter phone number">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input" data-validate = "Message is required">
					<span class="label-input100">Message:</span>
					<textarea class="input100" name="message" placeholder="Your Comment..."></textarea>
					<span class="focus-input100"></span>
				</div>

				<div class="container-contact100-form-btn">
					<button class="contact100-form-btn">
						<span>
							Submit
							<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
						</span>
					</button>
				</div>
			</form>
		</div>
	</div>


<?php require_once("footer.php")?>