<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Welcome to studio website</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../css/aos.css" media="all" />	
  <link rel="shortcut icon" type="image/x-icon" href="img/mlogo.png">
 
  <script src="../js/jquery.min.js"></script>
	<script src="../js/bjqs-1.3.min.js"></script>	
	<link rel="stylesheet" href="../css/bjqs.css">
  <link rel="stylesheet" type="text/css" href="../css/style.css" media="all" />
 



</head>
<body>

<div class="container bg-primary text-light">
    <div class="row">
        <div class="col-lg-12 ">
            <div class="tab-content py-4">
                <div class="tab-pane active" id="profile">
                    <div class="row bg-primary">
                        <div class="col-md-12">
                          <!-- profile info start here -->
                          <div class="container"> 
                        <div class="row">  
                           <div class="col-md-2"> 
                            <a class="btn btn-danger" href="dashbored.php">Dashbored</a>       
                        </div>
                            <div class="col-md-4">         
                                 <img style="width:50%" src="../images/me.jpg" class="img-fluid mx-auto img-circle d-block" alt="avatar">
                                 
                            </div>
                            <div class="col-md-6 mt-2">
                            <h3>  Name : <strong>Md. Numan Parvej</strong> </h3>
                            <p class="lead">  Date of Birth : <strong> 20/April/1999</strong> </p>
                            <p class="lead">Father Name : <strong>Shahar Uddin </strong> </p>
                            <p class="lead">Mother Name : <strong> Mst Rumela Akter</strong> </p>

                            </div>
                          
                        </div>
                        </div>
                    <div class=" mt-3"> 
                    <div class="row "> 
                        <div class="col-md-2"> </div>
                      <div class="col-md-4"> 
                         <h5 class="eng"> Gender : <strong> Male</strong> </h5>
                       </div>
                      <div class="col-md-4"> 
                          <h5 class="eng"> Religion : <strong> Islam</strong></h5>
                       </div>
                        <div class="col-md-2">  </div>
                      </div>
                    </div>

                    <div class="mt-3"> 
                    <div class="row "> 
                        <div class="col-md-2"> </div>
                      <div class="col-md-4"> 
                         <h5 class="eng"> Place of brith : <strong> Banglabazar</strong> </h5>
                       </div>
                      <div class="col-md-4"> 
                          <h5 class="eng"> Blood Group : <strong> O+</strong></h5>
                       </div>
                        <div class="col-md-2">  </div>
                      </div>
                    </div>

                    <div class="mt-3"> 
                    <div class="row "> 
                        <div class="col-md-2"> </div>
                      <div class="col-md-4"> 
                         <h5 class="eng"> National ID : <strong> 9013386000321</strong> </h5>
                       </div>
                      <div class="col-md-4"> 
                          <h5 class="eng"> Passport ID : <strong>9013386000321</strong></h5>
                       </div>
                        <div class="col-md-2">  </div>
                      </div>
                    </div>

                    <div class="mt-3"> 
                    <div class="row "> 
                        <div class="col-md-2"> </div>
                      <div class="col-md-4"> 
                         <h5 class="eng"> Birth Registration: <strong> 9013386000321</strong> </h5>
                       </div>
                      <div class="col-md-3"> 
                          <h5 class="eng"> Marital Staus : <strong>Married</strong></h5>
                       </div>
                        <div class="col-md-3"> <h5 class="eng"> Spouse Name:  <strong>Empty</strong></h5> </div>
                      </div>
                    </div>

                    <div class="mt-3"> 
                    <div class="row "> 
                        <div class="col-md-2"> </div>
                      <div class="col-md-4"> 
                         <h5 class="eng">  Quota:<strong>  Non Quota</strong> </h5>
                       </div>
                      <div class="col-md-4"> 
                          <h5 class="eng"> Email : <strong>mdnomanparvej@gmail.com</strong></h5>
                       </div>
                        <div class="col-md-2">  </div>
                      </div>
                    </div>

                    <div class="mt-3"> 
                    <div class="row "> 
                        <div class="col-md-2"> </div>
                      <div class="col-md-4"> 
                         <h5 class="eng">Mobile Number: <strong>  0170001000</strong> </h5>
                       </div>
                      <div class="col-md-4"> 
                          <h5 class="eng">   <span> </span></h5>
                       </div>
                        <div class="col-md-2">  </div>
                      </div>
                    </div>
<!-- Address box start here  -->
                <div class="container text-light"> 
                    <div class="row "> 
                        <div class="col-md-2"> </div>
                        <div class="col-md-4 text-center"> 
                            <h2 class="lead text-light"> Mailing/Present Address</h2>
                            <h5> Care of:<strong> Shahar Uddin</strong></h5>
                            <h5 class="eng">Village/Town/ Road/House/Flat: <br> <strong> 32 banglabazar</strong> </h5> 
                            <h5 class="eng"> District : <strong> Sunamganj</strong> </h5>
                            <h5 class="eng"> P.S/Upazila : <strong> Dourabazar</strong></h5>
                            <h5 class="eng"> Post Office : <strong> Banglabazar</strong> </h5>
                            <h5 class="eng"> Post Code : <strong> 307001</strong> </h5>
                            
                    </div>
                    <!-- Left Side address bar -->
                        <div class="col-md-4 text-center">
                        <h2 class="lead text-light"> Parmanent Address</h2>
                        <h5> Care of:<strong> Shahar Uddin</strong></h5>
                            <h5 class="eng">Village/Town/Road/House/Flat: <br> <strong> 32 banglabazar</strong> </h5> 
                            <h5 class="eng"> District : <strong> Sunamganj</strong> </h5>
                            <h5 class="eng"> P.S/Upazila : <strong> Dourabazar</strong></h5>
                            <h5 class="eng"> Post Office : <strong> Banglabazar</strong> </h5>
                            <h5 class="eng"> Post Code : <strong> 307001</strong> </h5>

                    </div>
                        <div class="col-md-2"> </div>
                    </div>
                </div>

<!-- jsc exam start -->
                <div class="container mt-4"> 
                    <div class="row "> 
                        <div class="col-md-12"> 
                            <h2 class="lead text-light"> JSC or Equalvalent Level </h2>
                        </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3"> <h5> Examination: <strong> JSC</strong></h5></div>
                        <div class="col-md-3"> <h5> Board: <strong> Sylhet</strong></h5></div>
                        <div class="col-md-3"> <h5> Roll No: <strong> 326550</strong></h5></div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3 mt-3"> <h5> Group/Subject: <strong> Genaral</strong></h5></div>
                        <div class="col-md-3 mt-3"> <h5> Passing Year: <strong> 2014</strong></h5></div>
                        <div class="col-md-4 mt-3"> <h5> Result : <strong>GPA (Out of 5)  <span> 5.00</span></strong></h5></div>
                    </div>
                </div>
<!-- end jsc exam -->
<!-- ssc exam start -->
<div class="container mt-4"> 
                    <div class="row "> 
                        <div class="col-md-12"> 
                            <h2 class="lead text-light"> SSC or Equalvalent Level </h2>
                        </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3"> <h5> Examination: <strong> SSC</strong></h5></div>
                        <div class="col-md-3"> <h5> Board: <strong> Sylhet</strong></h5></div>
                        <div class="col-md-3"> <h5> Roll No: <strong> 326550</strong></h5></div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3 mt-3"> <h5> Group/Subject: <strong> Genaral</strong></h5></div>
                        <div class="col-md-3 mt-3"> <h5> Passing Year: <strong> 2014</strong></h5></div>
                        <div class="col-md-4 mt-3"> <h5> Result : <strong>GPA (Out of 5)  <span> 5.00</span></strong></h5></div>
                    </div>
                </div>
<!-- end ssc exam -->
<!-- HSC exam start -->
<div class="container mt-4"> 
                    <div class="row "> 
                        <div class="col-md-12"> 
                            <h2 class="lead text-light"> HSC or Equalvalent Level </h2>
                        </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3"> <h5> Examination: <strong> HSC</strong></h5></div>
                        <div class="col-md-3"> <h5> Board: <strong> Sylhet</strong></h5></div>
                        <div class="col-md-3"> <h5> Roll No: <strong> 326550</strong></h5></div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3 mt-3"> <h5> Group/Subject: <strong> Genaral</strong></h5></div>
                        <div class="col-md-3 mt-3"> <h5> Passing Year: <strong> 2014</strong></h5></div>
                        <div class="col-md-4 mt-3"> <h5> Result : <strong>GPA (Out of 5)  <span> 5.00</span></strong></h5></div>
                    </div>
                </div>
<!-- end HSC exam -->
<!-- dgree exam start -->
<div class="container mt-4"> 
                    <div class="row "> 
                        <div class="col-md-12"> 
                            <h2 class="lead text-light"> Degree/Honars or Equalvalent Level </h2>
                        </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3"> <h5> Examination: <strong> HSC</strong></h5></div>
                        <div class="col-md-3"> <h5> Subject/Degree: <strong> Civil</strong></h5></div>
                        <div class="col-md-3"> <h5>Course Duration:<strong> 05 Years</strong></h5></div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3 mt-3"> <h5>University/Institute: <strong> Dhaka University</strong></h5></div>
                        <div class="col-md-3 mt-3"> <h5> Passing Year: <strong> 2014</strong></h5></div>
                        <div class="col-md-4 mt-3"> <h5> Result : <strong>GPA (Out of 5)  <span> 5.00</span></strong></h5></div>
                    </div>
                </div>
<!-- end dgree exam -->
<!-- Master exam start -->
<div class="container mt-4"> 
                    <div class="row "> 
                        <div class="col-md-12"> 
                            <h2 class="lead text-light"> Master Equalvalent Level</h2>
                        </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3"> <h5> Examination: <strong> Master</strong></h5></div>
                        <div class="col-md-3"> <h5> Subject/Degree: <strong> Civil</strong></h5></div>
                        <div class="col-md-3"> <h5>Course Duration:<strong> 05 Years</strong></h5></div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-2"> </div>
                        <div class="col-md-3 mt-3"> <h5>University/Institute: <strong> Dhaka University</strong></h5></div>
                        <div class="col-md-3 mt-3"> <h5> Passing Year: <strong> 2014</strong></h5></div>
                        <div class="col-md-4 mt-3"> <h5> Result : <strong>GPA (Out of 5)  <span> 5.00</span></strong></h5></div>
                    </div>
                </div>
<!-- end Master exam -->
<!-- Professional   start -->
<div class="container mt-4"> 
                    <div class="row "> 
                        <div class="col-md-12"> 
                            <h2 class="lead text-light"> Professional Experience</h2>
                        </div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-5"> <h5> Designation/Post name: <strong> Assistence Teacher</strong></h5></div>
                        <div class="col-md-5"> <h5>Organization Name<strong> name of Organization</strong></h5></div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-5"> <h5>Service Start Date:<strong> 05 May 2015 </strong></h5></div>
                        <div class="col-md-5 "> <h5>Service End Date: <strong> 11 Jun 2019</strong></h5></div>
                        <div class="col-md-1"> </div>
                    </div>
                </div>
<!-- end Professional   -->

                <div class="container mt-3"> 
                    <div class="row"> 
                        <div class="col-md-1"> </div>
                        <div class="col-md-6"> <h5> Responsibilities : <strong> Name Of Responsibilities</strong></h5></div>
                        <div class="col-md-5"> <h5> Departmental Candidate Status: <strong> Name</strong></h5></div>
                    </div>
                </div>

                <div class="container mt-3"> 
                    <div class="row"> 
                        <div class="col-md-1"> </div>
                        <div class="col-md-3"> <h5>Weight : <strong>50KG</strong></h5></div>
                        <div class="col-md-4"> <h5> Height (Feet --inc): <strong> 5 Feet 4 inc</strong></h5></div>
                        <div class="col-md-3"> <h5> Chest Size <strong> 32 inc</strong></h5> </div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-1"> </div>
                        <div class="col-md-6 mt-3"> <h5> Computer Training institute services :<strong> Name of service</strong></h5></div>
                        <div class="col-md-5 mt-3"> <h5> Typeing speed : <span> Bangla: <strong> 25</strong> <span> English : <strong> 30</strong></span></span></h5></div>
                        <div class="col-md-8"></div>
                        <div class="col-md-4 mt-5">         
                                 <img style="width:50%" src="https://upload.wikimedia.org/wikipedia/commons/a/ae/Dave_Bautista_signature.svg" class=" mx-auto img-circle d-block" alt="avatar">
                                 
                            </div>
                    </div>
                </div>
                
                <div class="container mt-5"> 
                    <div class="row"> 
                        <div class="col-md-12 text-center">
                           <a class="btn btn-warning p-3" href="dashbored.php">Complete</a> 
                           </div>
                    </div>
                </div>
         
                        </div> 
                    </div>
                    <!--/row------------------>
                </div>
              
            </div>
        </div>
    </div>
</div>
</div>
	<script src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/acor.js"></script>
  <script src="../js/bootstrap.min.js"></script>

</body>
</html>