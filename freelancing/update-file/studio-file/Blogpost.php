<?php require_once("header.php")?>

<div class="p-5 bg-light"> 
<div class="container"> 
   
<ul class="list-unstyled">
  <li class="media">
    <img class="w-25 mr-3" src="images/banner03.jpg" alt="Generic placeholder image">
    <div class="media-body">
      <h3 class="mt-0 mb-1">List-based media object</h3>
      Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus. <a class="text-success" href="#">Continue Reading.. </a>
    </div>
  </li>
  <li class="media my-4">
    <img class="w-25 mr-3"src="images/banner03.jpg" alt="Generic placeholder image">
    <div class="media-body">
      <h3 class="mt-0 mb-1">List-based media object</h3>
      Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus. <a class="text-success" href="#">Continue Reading.. </a>
    </div>
  </li>
  <li class="media">
    <img class="w-25 mr-3"src="images/banner03.jpg" alt="Generic placeholder image">
    <div class="media-body">
      <h3 class="mt-0 mb-1">List-based media object</h3>
      Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus. <a class="text-success" href="#">Continue Reading.. </a>
    </div>
  </li>
</ul>

<ul class="pagination">
  <li class="page-item"><a class="page-link" href="#">Previous</a></li>
  <li class="page-item"><a class="page-link" href="#">1</a></li>
  <li class="page-item"><a class="page-link" href="#">2</a></li>
  <li class="page-item"><a class="page-link" href="#">3</a></li>
  <li class="page-item"><a class="page-link" href="#">Next</a></li>
</ul> 
</div>
</div>

<?php require_once("footer.php")?>