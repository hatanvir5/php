<?php require_once("header.php")?>

<!------------Slider start here----------------->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="images/banner02.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="images/banner02.jpg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="images/banner03.jpg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<!-------------Slider END ---------->

<div class="container">
	<div class="row">
	<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" onclick="this.parentNode.parentNode.removeChild(this.parentNode);" class="close" data-dismiss="alert"><span aria-hidden="true">×</span
  ><span class="sr-only">Close</span>
</button>
  <strong>
	  <i class="fa fa-warning"></i> নোটিশ !
	</strong>
	 <marquee><p style="font-size: 18px">Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor!</p></marquee>
</div>
	</div>
</div>


	
	   
<!-------Notice END--->


<!------Post start------>
<div class="container bg-primary pb-5"> 
	<div class="row"> 
		<div class="col-md-4 mt-5"> 
			
			<div class="card" style="width: 100%;">
  				<img class="card-img-top" src="images/banner02.jpg" alt="Card image cap">
		   	 <div class="card-body">
					<div class="card-title"><strong class="ban"> বাংলায় নমুনা লেখা </strong></div>
    				<p class="ban card-text">অর্থহীন লেখা যার মাঝে আছে অনেক কিছু। হ্যাঁ, এই লেখার মাঝেই আছে অনেক কিছু। যদি তুমি মনে করো, এটা তোমার কাজে লাগবে, তাহলে তা লাগবে কাজে। নিজের ভাষায় লেখা দেখতে অভ্যস্ত হও। মনে রাখবে লেখা অর্থহীন হয়, ?</p>
 				 </div>
			</div>
		</div>

		<div class="col-md-4 mt-5"> 
			
			<div class="card" style="width: 100%;">
  				<img class="card-img-top" src="images/banner01.jpg" alt="Card image cap">
		   	 <div class="card-body">
					<div class="card-title"><h3 class="ban"> বাংলায় নমুনা লেখা </h3></div>
    				<p class="ban card-text">অর্থহীন লেখা যার মাঝে আছে অনেক কিছু। হ্যাঁ, এই লেখার মাঝেই আছে অনেক কিছু। যদি তুমি মনে করো, এটা তোমার কাজে লাগবে, তাহলে তা লাগবে কাজে। নিজের ভাষায় লেখা দেখতে অভ্যস্ত হও। মনে রাখবে লেখা অর্থহীন হয়,  ?</p>
 				 </div>
			</div>
		</div>

		<div class="col-md-4 mt-5"> 
			
			<div class="card" style="width: 100%;">
  				<img class="card-img-top" src="images/banner03.jpg" alt="Card image cap">
		   	 <div class="card-body">
					<div class="card-title"><strong class="ban"> বাংলায় নমুনা লেখা </strong></div>
    				<p class="card-text ban">অর্থহীন লেখা যার মাঝে আছে অনেক কিছু। হ্যাঁ, এই লেখার মাঝেই আছে অনেক কিছু। যদি তুমি মনে করো, এটা তোমার কাজে লাগবে, তাহলে তা লাগবে কাজে। নিজের ভাষায় লেখা দেখতে অভ্যস্ত হও। মনে রাখবে লেখা অর্থহীন হয়,  </p>
 				 </div>
			</div>
		</div>
	</div>
</div>
<!-----Service start here--->
<section> 
	<div class="container bg-primary"> 
		<div class="row"> 
	    	<div class="col-md-12 text-light text-center"> <h3 class="ban"> আমাদের সেবা সমূহ</h3></div>
		</div>
		<div class="row"> 
			<div class="col-md-4 mt-5"> 
				<div class="card p-4"> 
					<div class="card-title text-center"> <h3 class="ban"> আমাদের সেবা সমূহ</h3> </div>
					<div class="row"> 
					<div class="col-4 ban">  ইমেজ/ আইকন হবে</div>
					<div class="ban col-8"> 
						<ul> 
						<li> আমাদের সেবা সমূহ</li>
						<li> আমাদের সেবা সমূহ</li>
						<li> আমাদের সেবা সমূহ</li>
						</ul>
					</div>
				</div>
				 </div>
			</div>

			<div class="col-md-4 mt-5"> 
				<div class="card p-4"> 
					<div class="card-title text-center"> <h4> আমাদের সেবা সমূহ</h4> </div>
					<div class="row"> 
					<div class="col-4">  ইমেজ/ আইকন হবে</div>
					<div class="col-8"> 
						<ul> 
						<li> আমাদের সেবা সমূহ</li>
						<li> আমাদের সেবা সমূহ</li>
						<li> আমাদের সেবা সমূহ</li>
						</ul>
					</div>
					</div>
				 </div>
			</div>

			<div class="col-md-4 mt-5"> 
				<div class="card p-4"> 
					<div class="card-title text-center"> <h4> আমাদের সেবা সমূহ</h4> </div>
					<div class="row"> 
					<div class="col-4">  ইমেজ/ আইকন হবে  </div>
					<div class="col-8"> 
						<ul> 
						<li> আমাদের সেবা সমূহ</li>
						<li> আমাদের সেবা সমূহ</li>
						<li> আমাদের সেবা সমূহ</li>
						</ul>
					</div>
					</div>

				 </div>
			</div>
		</div>
	</div>
</section>	


<!-----Service start here--->
<section> 
	<div class="container pt-5 bg bg-primary"> 
		<div class="row"> 
	    	<div class="col-md-12 text-light text-center"> 
				<h3> আপনি কেন আমাদের সেবা নিবেন </h3>
			</div>
		</div>
		<div class="row"> 
			<div class="col-md-4 mt-5"> 
				<div class="card p-4"> 
					<div class="card-title text-center"> <h4> আমাদের সেবা সমূহ</h4> </div>
					<div class="row"> 
					<div class="col-4">  ইমেজ/ আইকন হবে</div>
					<div class="col-8"> 
						<ul> 
						<li> আমাদের সেবা সমূহ</li>
						<li> আমাদের সেবা সমূহ</li>
						<li> আমাদের সেবা সমূহ</li>
						</ul>
					</div>
					</div>
				 </div>
			</div>
			<div class="col-md-4 mt-5"> 
				<div class="card p-4"> 
					<div class="card-title text-center"> <h4> আমাদের সেবা সমূহ</h4> </div>
					<div class="row"> 
					<div class="col-4">  ইমেজ/ আইকন হবে</div>
					<div class="col-8"> 
						<ul> 
						<li> আমাদের সেবা সমূহ</li>
						<li> আমাদের সেবা সমূহ</li>
						<li> আমাদের সেবা সমূহ</li>
						</ul>
					</div>
					</div>
				 </div>
			</div>
			<div class="col-md-4 mt-5"> 
				<div class="card p-4"> 
					<div class="card-title text-center"> <h4> আমাদের সেবা সমূহ</h4> </div>
					<div class="row"> 
					<div class="col-4">  ইমেজ/ আইকন হবে  </div>
					<div class="col-8"> 
						<ul> 
						<li> আমাদের সেবা সমূহ</li>
						<li> আমাদের সেবা সমূহ</li>
						<li> আমাদের সেবা সমূহ</li> 
						</ul>
					</div>
					</div>
				 </div>
			</div>
		</div>
	</div>
</section>	

<!-- Testomonial part start -->
<div class="container bg-primary pt-5"> 
	<div class="row"> 
		<div class="col-md-12"> 
			
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Carousel indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol> <!-- Wrapper for carousel items -->
    <div class="carousel-inner">
        <div class="item carousel-item active">
            <div class="img-box"><img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1558758981/male-2634974_640.jpg" alt=""></div>
            <p class="testimonial">Phasellus vitae suscipit justo. Mauris pharetra feugiat ante id lacinia. Etiam faucibus mauris id tempor egestas. Duis luctus turpis at accumsan tincidunt. Phasellus risus risus, volutpat vel tellus ac, tincidunt fringilla massa. Etiam hendrerit dolor eget rutrum.</p>
            <p class="overview"><b>Paula Wilsons</b>Seo Analyst </p>
            <div class="star-rating"> </div>
        </div>
        <div class="item carousel-item">
            <div class="img-box"><img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1558758980/people-875617_1280.jpg" alt=""></div>
            <p class="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Vestibulum idac nisl bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
            <p class="overview"><b>Paula Wilson</b>Media Analyst </p>
            <div class="star-rating"> </div>
        </div>
        <div class="item carousel-item">
            <div class="img-box"><img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1558758980/portrait-1287413_640.jpg" alt=""></div>
            <p class="testimonial">Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget nisi a mi suscipit tincidunt. Utmtc tempus dictum risus. Pellentesque viverra sagittis quam at mattis. Suspendisse potenti. Aliquam sit amet gravida nibh, facilisis gravida odio. Phasellus auctor velit.</p>
            <p class="overview"><b>Antonio Moreno</b>Web Developer</p>
            <div class="star-rating"> </div>
        </div>
    </div> <!-- Carousel controls --> <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev"> <i class="fa fa-angle-left"></i> </a> <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next"> <i class="fa fa-angle-right"></i> </a>
</div>
		</div>
	</div>
</div>
<!----------------Video Part start Here----------------->
<div class="video_part p-5 bg-primary"> 
	<div class="container"> 
		<div class="row"> 
				<div class="col-md-7 col-sm-12 text-light"> 
					<div class="video_title "> 
						<h3 class="text-center p-3"> ভিডিও টাইটেল হবে </h3>
				</div>
				<div class="video_discription"> 
					<p class="lead"> 	অর্থহীন লেখা যার মাঝে আছে অনেক কিছু। হ্যাঁ, এই লেখার মাঝেই আছে অনেক কিছু। যদি তুমি মনে করো, এটা তোমার কাজে লাগবে, তাহলে তা লাগবে কাজে। নিজের ভাষায় লেখা দেখতে অভ্যস্ত হও। মনে রাখবে লেখা অর্থহীন হয়,  ?			</p>
			</div>
				</div>
				<div class="col-md-5 col-sm-12"> 
				<iframe width="100%" height="315"
src="https://www.youtube.com/embed/tgbNymZ7vqY">
</iframe> 
			</div>
		</div>
	</div>
</div>

<?php require_once("footer.php")?>