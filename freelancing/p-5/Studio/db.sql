create TABLE registrations(
    id int(11) not null auto_increment primary key,
    name varchar(50) not null,
    phone varchar(50) not null,
    email varchar(50) not null,
    password varchar(50) not null

);
create TABLE users(
    id int(11) not null auto_increment primary key,
    email varchar(50) not null,
    password varchar(50) not null,
    registrationId int(11) not null,
    FOREIGN KEY (registrationId) REFERENCES registrations(id)
);


CREATE TABLE registrations(
id int(11) not null auto_increment primary key,
applicantName VARCHAR(40),
fatherName VARCHAR(40),
motherName VARCHAR(40),

birthDate VARCHAR(5),
birthMonth VARCHAR(15),
birthYear VARCHAR(8),
gender VARCHAR(10),

religion VARCHAR(15),
birthPlace VARCHAR(30),
bloodGroup VARCHAR(15),
nid VARCHAR(30),
passport VARCHAR(30),
birthCirtificate VARCHAR(30),
maritalStatus VARCHAR(20),
wifeName VARCHAR(40),
email VARCHAR(40),

number VARCHAR(15),
confirmNumber VARCHAR(15),
 
presentGurdianName VARCHAR(40),
PresentVillage VARCHAR(40),
presentDistrict VARCHAR(20),
presentUpazila VARCHAR(20),
presenPostOffice VARCHAR(20),
presentPostCode  VARCHAR(20),

homePostalCHeck TINYINT,

parmanentGurdianName VARCHAR(40),
parmanentDistrict VARCHAR(20),
parmanentUpazila VARCHAR(20),
parmanentPostOffice VARCHAR(20),
parmanentPostCode VARCHAR(20),

jscExam VARCHAR(30),
jscBoard VARCHAR(20),
jscRoll VARCHAR(20),
jscSubject VARCHAR(20),
jscResult VARCHAR(20),
jscGPA float(4,3),
jscYear VARCHAR(10),

sscExam VARCHAR(30),
sscBoard VARCHAR(20),
sscRoll VARCHAR(20),
sscGroup VARCHAR(40),
sscResult VARCHAR(20),
sscGPA float(4,3),
sscYear VARCHAR(10),

hscExam VARCHAR(30),
hscBoard VARCHAR(20),
hscRoll VARCHAR(20),
hscSubject VARCHAR(40),
hscResult VARCHAR(20),
hscGPA float(4,3),
hscYear VARCHAR(10),

degreeExam VARCHAR(30),
degreeUniversity VARCHAR(70),
degreeCourseDuration VARCHAR(10),
degreeResult VARCHAR(20),
degreeCGPA  float(4,3),
degreePassingYear VARCHAR(10),

masterExam VARCHAR(30),
masterUniversity VARCHAR(70),
masterCourseDuration VARCHAR(10),
masterSubject VARCHAR(60),
masterResult VARCHAR(40),
masterCGPA float(4,3),
masterPassingYear VARCHAR(10),

postName VARCHAR(40),
organizationName VARCHAR(70),
jobStartTime VARCHAR(40),
jobEndTime VARCHAR(40),
jobResponsibility VARCHAR(140),
departmentalCandidateStatus VARCHAR(100),


weight VARCHAR(10),
height VARCHAR(10),
chestSize VARCHAR(10),
computerTrainingServices VARCHAR(100),

banglaTypingSpeed VARCHAR(10),
englishTypingSpeed VARCHAR(10),


applicantPhoto VARCHAR(40),
applicantSignature VARCHAR(40),

password VARCHAR(40),
confirmPassword VARCHAR(40)

        )
