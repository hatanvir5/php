<?php
require 'db.php';

$data=$_POST;
if(array_key_exists('id',$data)){
    $id=$data['id'];
}

$sql='SELECT * FROM users JOIN `registrations` ON `users`.`registrationId` = `registrations`.`id`  WHERE users.id=:id';
$statement=$connection->prepare($sql);
$statement->execute([':id'=>$id]);
$user=$statement->fetch(PDO::FETCH_OBJ);



?>
<div class="container width-300px bg-dark text-light mt-5 p-5"> 
    <h4 class="ml-5">Student Info</h4>
    <ul>
        <li><b>Name:</b><?= $user->name;?></li>
        <li><b>Email:</b><?= $user->email;?></li>
        <li><b>Phone:</b><?= $user->phone;?></li>
    
        <li><b>Gender:</b><?= $user->gender;?></li>
        <li><b>Password:</b><?= $user->password;?></li>
  
        <p class="mt-5"><a class="btn btn-primary" href="list.php">Go to homepage</a></p>
        
    </ul>
</div>
<?php


?>