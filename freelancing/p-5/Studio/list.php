<?php
require 'db.php';

        $sql2='SELECT users.id,users.email,users.password,registrations.name,registrations.gender,registrations.phone FROM `users`  JOIN `registrations` ON `users`.`registrationId` = `registrations`.`id`' ;
        $statement3=$connection->prepare($sql2);
        $statement3->execute();
        $users=$statement3->fetchAll(PDO::FETCH_OBJ);
        ?>



<!doctype html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>Records</title>
  </head>
  <body class="bg-dark container">

        <table border="3" class="table table-striped table-dark mt-5 ">
        <thead>
          <tr>
           
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Phone</th>
            <th scope="col">Email</th>
            <th scope="col">Gender</th>
            <th scope="col">Password</th>
     
            <th scope="col">Action</th>
      
          </tr>
        </thead>
        <tbody>
      
        <?php foreach($users as $user):?>
          <tr>
          
            <td><?= $user->id;?></td>
            <td><?= $user->name;?></td>
            <td><?= $user->phone;?></td>
            <td><?= $user->email;?></td>
            <td><?= $user->gender;?></td>
            <td><?= $user->password;?></td>
 
           
            <td>  
            <div class="form-group"> 
      
            <form method="post" action="view.php" class="inline-block m-0 p-0">
              <input type="hidden" name="id" value="<?=$user->id?>"> 
              <button type="submit"  name="submit" class="btn btn-success mb-1 pr-4">View</button>
      
              </form>
      
            <form method="post" action="edit.php" class="inline-block m-0 p-0">
              <input type="hidden" name="id" value="<?=$user->id?>"> 
              <button type="submit"  name="submit" class="btn btn-success mb-1 pr-4">Edit</button>
      
              </form>
      
      
            <form method="post" action="delete.php" class="inline-block m-0 p-0 width-100px">
              <input type="hidden" name="id" value="<?=$user->id?>"> 
              <button type="submit" name="submit" class="btn btn-danger">Delete</button>
      
      
      
              <div>
            </form>
      
      
      
            </td>
         
          </tr>
      <?php endforeach;?>
      
        </tbody>
      
      
      </table>
      
<a class="btn btn-success mb-4" href="cvform.php">Add Record</a>

