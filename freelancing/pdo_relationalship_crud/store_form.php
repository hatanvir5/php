
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
    

<div class="row  mt-5 offset-2">
<div class="col-md-8">
<form action="store_processing.php" method="POST" enctype="multipart/form-data">

  <div class="form-group">
    <label for="name" class="col-sm-2 col-form-label">User Name</label>
    <div class="col-sm-8">
    <input type="text" class="form-control" name="username" id="name" aria-describedby="emailHelp" placeholder="Enter name">

    </div>

  <div class="form-group">
    <label for="fullname" class="col-sm-2 col-form-label"> fullname</label>
    <div class="col-sm-8">
    <input type="text" class="form-control" name="fullname" id="fullname" aria-describedby="emailHelp" placeholder="Enter fullname">

    </div>


  <div class="form-group">
    <label for="status" class="col-sm-2 col-form-label"> status</label>
    <div class="col-sm-8">
    <input type="text" class="form-control" name="status_message" id="status" aria-describedby="emailHelp" placeholder="Enter fullname">

    </div>



  <div class="form-group">
    <label for="password" class="col-sm-2 col-form-label">password</label>
    <div class="col-sm-8">
    <input type="password" class="form-control" name="password" id="password" aria-describedby="emailHelp" placeholder="Enter password">
    </div>
    
  </div>

  <div class="form-group">
    <label for="confirm_password" class="col-sm-2 col-form-label">confirm password</label>
    <div class="col-sm-8">
    <input type="password" class="form-control" name="confirm_password" id="confirm_password" aria-describedby="emailHelp" placeholder="Enter password">
    </div>
    
  </div>

  <div class="form-group">
    <label for="file" class="col-sm-2 col-form-label">Image</label>
    <div class="col-sm-8">
    <input type="file" class="form-control" name="image" id="file" aria-describedby="emailHelp" placeholder="">
    </div>
    
  </div>
<br />
  <button type="submit" name="submit" class="btn btn-primary">Submit</button>
</form>
</div>

</div>
</body>
</html>