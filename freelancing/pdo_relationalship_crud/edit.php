<?php

include_once('config.php');

$id = $_GET['user_id'];


$select_query = "SELECT * FROM profiles
                 JOIN users 
                 ON profiles.user_id = users.id
                  WHERE user_id = '{$id}'";

$data = $db_connect->query($select_query);
$user = $data->fetch();


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
    

<div class="row offset-4 mt-5">
<form action="edit_processing.php" method="POST" enctype="multipart/form-data">


    <input type="hidden" value="<?=$user['user_id'] ?> " name="user_id" />


    <div class="form-group">
    <label for="name" class=" col-form-label">User Name</label>
    <input type="text" class="form-control" name="username" id="username" aria-describedby="emailHelp" placeholder="Enter username" value="<?= $user['username'] ?>">
    </div>


    <div class="form-group">
    <label for="fullname" class=" col-form-label"> Full Name</label>
    <input type="text" class="form-control" name="fullname" id="fullname" aria-describedby="emailHelp" placeholder="Enter fullname" value="<?= $user['fullname'] ?>">
    </div>



    <div class="form-group">
    <label for="status_message" class=" col-form-label"> Status</label>
    <input type="text" class="form-control" name="status_message" id="status_message" aria-describedby="emailHelp" placeholder="Enter status" value="<?= $user['status_message'] ?>">
    </div>


    <div class="form-group">
    <label for="password" class=" col-form-label"> password</label>
    <input type="text" class="form-control" name="password" id="password" aria-describedby="emailHelp" placeholder="Enter password" value="<?= $user['password'] ?>">
    </div>




    <div class="form-group">
    <label for="confirm_password" class=" col-form-label"> confirm_password</label>
    <input type="text" class="form-control" name="confirm_password" id="confirm_password" aria-describedby="emailHelp" placeholder="Enter confirm_password" value="<?= $user['password'] ?>">
    </div>



    <div class="form-group">
    <label for="file" class=" col-form-label"> Image</label>
    
    <input type="file" class="form-control" name="new_image" id="file" aria-describedby="emailHelp" >
    <input type="hidden" value="<?= $user['image'];?>"  name="old_image">
    <img src="<?= $user['image'] ?>" width="100" height="100"> 
  
  </div>




  <button type="submit" name="submit" class="btn btn-primary">Submit</button>
</form>
</div>
</body>
</html>