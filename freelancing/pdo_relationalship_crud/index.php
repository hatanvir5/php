<?php


include_once('config.php');

$select_sql = 'SELECT * FROM profiles 
                JOIN users 
                ON users.id = profiles.user_id';

$statement = $db_connect->query($select_sql);


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    
<div class="div col-md-8 offset-2 mt-5">
    <a href="store_form.php">Add Record</a>
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">User Name</th>
      <th scope="col">Full Name</th>
      <th scope="col">Status</th>
      <th scope="col">Password</th>
      <th scope="col">Picture</th>
      
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>

<?php while($row = $statement->fetch(PDO::FETCH_ASSOC)){ ?>
   
    <tr>
      <th scope="row"><?= $row['user_id'] ?></th>
      <td><?= $row['username']?></td>
      <td><?= $row['fullname']?></td>
      <td class="<?= ($row['status_message']==1)?'text-success':'text-danger'?>"><?= ($row['status_message']==1)?'Active':'Inactive'?></td>
      <td><?php echo $row['password']?></td>
      <td>
        
      <img src="<?= $row['image'] ?>" width="100px" height="100px">
      
      </td>
      <td>
          <button><a href="edit.php?user_id=<?= $row['user_id'] ?>">edit</a></button>

          <form action="delete.php" method="POST">
              <input type="hidden" name="delete" value="<?= $row['id'] ?>">
      
          <input type="submit" value="delete" name='submit' class="text-primary">

          </form>
      </td>

    </tr>

<?php }?>

  </tbody>
</table>
</div>





</body>
</html>