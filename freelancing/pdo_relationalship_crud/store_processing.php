<?php

include_once('config.php');




if(isset($_POST['submit'])){

    $username = $_POST['username'];
    $fullname = $_POST['fullname'];
    $status_message = $_POST['status_message'];


    $password = null;
    if($_POST['password'] == $_POST['confirm_password']){
       $password = $_POST['password'];
    
    }

    $sql_users_insert = "INSERT INTO users(username,password)
    VALUES(:username,:password)";
    $users_query = $db_connect->prepare($sql_users_insert);
    $users_query->bindparam(':username', $username);
    $users_query->bindparam(':password', $password);
   
    $users_query->execute();



    
    $file_name = strtolower($_FILES['image']['name']);
    $file_dir = 'image/'.$file_name;
    move_uploaded_file($_FILES['image']['tmp_name'],$file_dir);




    $sql_profiles_insert = "INSERT INTO profiles(user_id,fullname,status_message,image)
    VALUES(LAST_INSERT_ID(),:fullname,:status_message,:image)";
    $profile_query = $db_connect->prepare($sql_profiles_insert);
    $profile_query->bindparam(':fullname',$fullname);
    $profile_query->bindparam(':image',$file_dir);
    $profile_query->bindparam(':status_message',$status_message);
    $profile_query->execute();

     
      

    header('location:index.php');
}else{
    echo 'failed';
}


?>