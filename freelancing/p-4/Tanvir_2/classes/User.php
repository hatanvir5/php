<?php
$filepath = realpath(dirname(__FILE__));
include_once ($filepath.'/../lib/Session.php');
Session::checkLogin();
include_once ($filepath.'/../lib/Database.php');
include_once ($filepath.'/../Helper/Format.php');
?>

<?php

class User
{
    private $db;
    private $fm;


    public function __construct()
    {

        $this->db = new Database();
        $this->fm = new  Format();

    }

    public function UserRegistration($data)
    {
        $full_name = mysqli_real_escape_string($this->db->link, $data['full_name']);
        $username = mysqli_real_escape_string($this->db->link, $data['username']);
        $email = mysqli_real_escape_string($this->db->link, $data['email']);
        $number = mysqli_real_escape_string($this->db->link, $data['number']);
        $sponsor_name = mysqli_real_escape_string($this->db->link, $data['sponsor_name']);
        $club = mysqli_real_escape_string($this->db->link, $data['club']);
        $password = mysqli_real_escape_string($this->db->link, $data['password']);
        $password_confirm = mysqli_real_escape_string($this->db->link, $data['password_confirm']);


//        if ($full_name == "" || $username == "" || $email == "" || $number == "" || $sponsor_name == "" || $club == '' ||  $password == "") {
//            $msg = "<span class ='error'> Fields Must Not be Empty! </span>";
//            return $msg;
//        }

        $mailquery = "SELECT * FROM tbl_user WHERE email = '$email' LIMIT 1";
        $mailchk = $this->db->select($mailquery);
        if ($mailchk != false) {
            $msg = "<span class ='error'>This Email Already Exist! </span>";
            return $msg;
        } else {
            $query = "INSERT INTO tbl_user(full_name,username,email,number,sponsor_name,club,password,password_confirm) 
VALUES ('$full_name','$username','$email','$number','$sponsor_name','$club','$password','$password_confirm')";
            $inserted_rows = $this->db->insert($query);
            if ($inserted_rows) {
                $msg = "<span class='success'>Your Data Inserted Successfully. </span>";
                return $msg;
            } else {
                $msg = "<span class='error'>Your Data Not Inserted !</span>";
                return $msg;

            }
        }
    }

    public function adminLogin($username,$password){

        $username = $this->fm->validation($username);
        $password = $this->fm->validation($password);


//        if(empty($username) || empty($password)){
//            $loginmsg = "Username or Password must not be Empty !";
//            return $loginmsg;
//        }
//        else{


            $query = "SELECT * FROM tbl_user WHERE username ='$username' AND
             password = '$password'";


            $result =  $this->db->select($query);

            if($result != false){
                $value = $result->fetch_assoc();
                Session::set("adminLogin",true);
                Session::set("adminId",$value['id']);
                header("location:login.php");
            }
            else{
                $loginmsg = "Username or Password not match !";
                return $loginmsg;
            }
        }

//    }


//    public function userLogin($data)
//    {
//        $username = mysqli_real_escape_string($this->db->link, $data['username']);
//        $password = mysqli_real_escape_string($this->db->link, $data['password']);
//
//        if (empty($username) || empty($password)) {
//            $msg = "<span class='error'>Fields must not be empty !</span>";
//            return $msg;
//        }
//        $query = "SELECT * FROM tbl_user WHERE username ='$username' AND password ='$password'";
//        $result = $this->db->select($query);
//        if ($result != false) {
//            $value = $result->fetch_assoc();
//            Session::set("userlogin", true);
//            Session::set("id", $value['id']);
//            Session::set("username", $value['username']);
//            header("Location:index.php");
//        } else {
//            $msg = "<span class='error'>Email or Password Not match  !</span>";
//            return $msg;
//        }
//    }

    public function getCustomerData($id)
    {
        $query = "SELECT * FROM tbl_customer WHERE id = '$id'";
        $result = $this->db->select($query);
        return $result;
    }

    public function customerUpdate($data, $cmrId)
    {
        $name = mysqli_real_escape_string($this->db->link, $data['name']);
        $address = mysqli_real_escape_string($this->db->link, $data['address']);
        $city = mysqli_real_escape_string($this->db->link, $data['city']);
        $country = mysqli_real_escape_string($this->db->link, $data['country']);
        $zip = mysqli_real_escape_string($this->db->link, $data['zip']);
        $phone = mysqli_real_escape_string($this->db->link, $data['phone']);
        $email = mysqli_real_escape_string($this->db->link, $data['email']);
        if ($name == "" || $address == "" || $city == "" || $country == "" || $zip == "" || $phone == "" || $email == "") {
            $msg = "<span class ='error'> Fields Must Not be Empty! </span>";
            return $msg;
        } else {

            $query = "INSERT INTO tbl_customer(name,address,city,country,zip,phone,email) 
VALUES ('$name','$address','$city','$country','$zip','$phone','$email')";

            $query = "UPDATE  tbl_customer
             SET 
                   name   = '$name',
                  address = '$address',
                     city = '$city',
                  country = '$country',
                      zip = '$zip',
                    phone = '$phone',
                    email = '$email'
                 WHERE id = '$cmrId'";

            $updated_row = $this->db->update($query);

            if ($updated_row) {
                $msg = "<span class ='success'> Customer Data  Updated Successfully.</span>";
                return $msg;

            } else {
                $msg = "<span class ='error'> Category Data Not  Updated. </span>";
                return $msg;
            }
        }

    }


}

?>